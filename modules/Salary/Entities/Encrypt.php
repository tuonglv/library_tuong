<?php
/**
 * Created by NgocNH.
 * Date: 8/16/16
 * Time: 2:49 PM
 */

namespace Modules\Salary\Entities;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Encryption\Encrypter;

class Encrypt
{

    public static function encrypt($string, $key)
    {
        $newEncrypter = new Encrypter($key, config('app.cipher'));

        return $newEncrypter->encrypt($string);
    }


    public static function decrypt($string, $key)
    {
        try {
            $newEncrypter = new Encrypter($key, config('app.cipher'));

            return $newEncrypter->decrypt($string);
        } catch (\Exception $e) {
            return false;
        }
    }
}