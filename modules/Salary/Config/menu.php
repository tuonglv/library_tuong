<?php

return [
    'salaries' => [
        'route'       => 'salary.mail.index',
        'permissions' => [ 'salary.mail.index', 'salary.config.index' ],
        'active'      => 'salary/*',
        'class'       => '',
        'icon'        => 'fa fa-users',
        'name'        => 'role',
        'text'        => 'salary::menu.salaries',
        'order'       => 5,
        'subs'        => [
            'mail' => [
                'route'       => 'salary.mail.index',
                'permissions' => [ 'salary.mail.index' ],
                'active'      => 'salary/mail',
                'class'       => '',
                'icon'        => '',
                'name'        => 'mail',
                'text'        => 'salary::menu.mail',
                'order'       => 1
            ],
            'config' => [
                'route'       => 'salary.config.index',
                'permissions' => [ 'salary.config.index' ],
                'active'      => 'salary/config/index',
                'class'       => '',
                'icon'        => '',
                'name'        => 'salary.config',
                'text'        => 'salary::menu.config',
                'order'       => 2
            ]
        ]
    ]
];