<?php
/**
 * Created by NgocNH.
 * Date: 8/16/16
 * Time: 9:27 AM
 */
?>

<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-content">
                <div class="tab_left">
                    <ul class="nav nav-tabs nav-red">
                        <li class="active">
                            <a href="#general" data-toggle="tab">{!! trans('salary::config.tab_general') !!}</a>
                        </li>
                        <li>
                            <a href="#mail_template"
                               data-toggle="tab">{!! trans('salary::config.tab_mail_template') !!}</a>
                        </li>
                        <li>
                            <a href="#file_template"
                               data-toggle="tab">{!! trans('salary::config.tab_file_template') !!}</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class='active tab-pane fade in' id='general'>
                            {!! Form::open(['class' => 'form-horizontal']) !!}
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="secret_key">{!! trans('salary::config.general.secret_key') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="secret_key"/>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class='tab-pane fade' id='mail_template'>
                            {!! Form::open(['url' => route('salary.config.save'), 'class' => 'form-horizontal']) !!}
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="secret_key">{!! trans('salary::config.mail_template.secret_key') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="secret_key" name="secret_key"
                                           placeholder="{!! trans('salary::config.mail_template.secret_key_placeholder') !!}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="mail_name">{!! trans('salary::config.mail_template.mail_name') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="mail_name" name="salary[mail_name]"
                                           value="{!! isset($config['mail_name']) ? $config['mail_name'] : (old('meta')['mail_name'] ?: '') !!}"
                                           placeholder="{!! trans('salary::config.mail_template.mail_name_placeholder') !!}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="mail_address">{!! trans('salary::config.mail_template.mail_address') !!}</label>

                                <div class="col-sm-8">
                                    <input type="email" class="form-control" id="mail_address"
                                           name="salary[mail_address]"
                                           value="{!! isset($config['mail_address']) ? $config['mail_address'] : (old('meta')['mail_address'] ?: '') !!}"
                                           placeholder="{!! trans('salary::config.mail_template.mail_address_placeholder') !!}"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="mail_password">{!! trans('salary::config.mail_template.mail_password') !!}</label>

                                <div class="col-sm-8">
                                    <input type="password" class="form-control" id="mail_password"
                                           name="salary[mail_password]"
                                           value="{!! isset($config['mail_password']) ? $config['mail_password'] : (old('meta')['mail_password'] ?: '') !!}"
                                           placeholder="{!! trans('salary::config.mail_template.mail_password_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label"
                                       for="mail_body">{!! trans('salary::config.mail_template.mail_body') !!}</label>

                                <div class="col-sm-12">
                                    <textarea class="form-control input-transparent editor"
                                              id="mail_body"
                                              name="salary[mail_body]" rows="3"
                                    >{!! old("meta.mail_body") ?: (isset($config['mail_body']) ? $config['mail_body'] : '') !!}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label"
                                       for="mail_footer">{!! trans('salary::config.mail_template.mail_footer') !!}</label>

                                <div class="col-sm-12">
                                    <textarea class="form-control input-transparent editor"
                                              id="mail_footer"
                                              name="salary[mail_footer]" rows="3"
                                    >{!! old("meta.mail_footer") ?: (isset($config['mail_footer']) ? $config['mail_footer'] : '') !!}</textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <button type="submit"
                                                class="btn btn-success">{!! trans('core::general.save') !!}</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="tab-pane fade" id="file_template">
                            {!! Form::open(['url' => route('salary.config.save'), 'class' => 'form-horizontal']) !!}
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="ten_nhan_vien">{!! trans('salary::config.file_template.ten_nhan_vien') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="ten_nhan_vien" name="salary_file[ten_nhan_vien]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.ten_nhan_vien_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="ngay_cong">{!! trans('salary::config.file_template.ngay_cong') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="ngay_cong" name="salary_file[ngay_cong]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : ''!!}"
                                           placeholder="{!! trans('salary::config.file_template.ngay_cong_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="ngay_thu_viec">{!! trans('salary::config.file_template.ngay_thu_viec') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="ngay_thu_viec" name="salary_file[ngay_thu_viec]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : ''!!}"
                                           placeholder="{!! trans('salary::config.file_template.ngay_thu_viec') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="luong_chinh_thuc">{!! trans('salary::config.file_template.luong_chinh_thuc') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="luong_chinh_thuc" name="salary_file[luong_chinh_thuc]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.luong_chinh_thuc') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="luong_thu_viec">{!! trans('salary::config.file_template.luong_thu_viec') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="luong_thu_viec" name="salary_file[luong_thu_viec]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.luong_thu_viec_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="tong_luong">{!! trans('salary::config.file_template.tong_luong') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tong_luong" name="salary_file[tong_luong]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.tong_luong_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="an_trua">{!! trans('salary::config.file_template.an_trua') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="an_trua" name="salary_file[an_trua]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : ''!!}"
                                           placeholder="{!! trans('salary::config.file_template.an_trua_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="phu_cap_1">{!! trans('salary::config.file_template.phu_cap_1') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="phu_cap_1" name="salary_file[phu_cap_1]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.phu_cap_1_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="phu_cap_2">{!! trans('salary::config.file_template.phu_cap_2') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="phu_cap_2" name="salary_file[phu_cap_2]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : ''!!}"
                                           placeholder="{!! trans('salary::config.file_template.phu_cap_2_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="phu_cap_3">{!! trans('salary::config.file_template.phu_cap_3') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="phu_cap_3" name="salary_file[phu_cap_3]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : ''!!}"
                                           placeholder="{!! trans('salary::config.file_template.phu_cap_3_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="phu_cap_4">{!! trans('salary::config.file_template.phu_cap_4') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="phu_cap_4" name="salary_file[phu_cap_4]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.phu_cap_4_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="luong">{!! trans('salary::config.file_template.luong') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="luong" name="salary_file[luong]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.luong_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="bao_hiem">{!! trans('salary::config.file_template.bao_hiem') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="bao_hiem" name="salary_file[bao_hiem]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong']  : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.bao_hiem_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="thue_tncn">{!! trans('salary::config.file_template.thue_tncn') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="thue_tncn" name="salary_file[thue_tncn]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.thue_tncn_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="luong_nhan">{!! trans('salary::config.file_template.luong_nhan') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="luong_nhan" name="salary_file[luong_nhan]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.luong_nhan_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="luong_ck">{!! trans('salary::config.file_template.luong_ck') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="luong_ck" name="salary_file[luong_ck]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.luong_ck_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="luong_tien_mat">{!! trans('salary::config.file_template.luong_tien_mat') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="luong_tien_mat" name="salary_file[luong_tien_mat]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.luong_tien_mat_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"
                                       for="tong_ngay_cong">{!! trans('salary::config.file_template.tong_ngay_cong') !!}</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tong_ngay_cong" name="salary_file[tong_ngay_cong]"
                                           value="{!! isset($config_file['tong_ngay_cong']) ? $config_file['tong_ngay_cong'] : '' !!}"
                                           placeholder="{!! trans('salary::config.file_template.tong_ngay_cong_placeholder') !!}"/>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <button type="submit"
                                                class="btn btn-success">{!! trans('core::general.save') !!}</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
