<div class="row">
    {!! Form::open(['class' => 'form-horizontal', 'url' => route('salary.mail.send'), 'enctype' => 'multipart/form-data']) !!}
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-header">
                <h4 class="panel-title">
                    <i class="fa fa-pencil"></i> {!! trans('salary::salary.title') !!}
                </h4>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"
                                   for="secret_key">{!! trans('salary::salary.secret_key') !!}</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="secret_key" name="secret_key"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"
                                   for="mail_subject">{!! trans('salary::salary.mail_subject') !!}</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mail_subject" name="mail_subject"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"
                                   for="mail_subject">{!! trans('salary::salary.file') !!}</label>

                            <div class="col-sm-8">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">{!! trans('core::general.choose') !!}</span>
                                        <span class="fileinput-exists">{!! trans('core::general.change') !!}</span>
                                        <input type="file" name="salary_file">
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists"
                                       data-dismiss="fileinput">{!! trans('core::general.remove') !!}</a>
                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <button type="submit"
                                                class="btn btn-success">{!! trans('core::general.save') !!}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>