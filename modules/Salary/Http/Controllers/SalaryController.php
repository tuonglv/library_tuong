<?php namespace Modules\Salary\Http\Controllers;

use League\Fractal\Manager;
use Modules\Core\Http\Controllers\Backend\AdminController;

class SalaryController extends AdminController
{

    public function __construct(Manager $manager)
    {
        parent::__construct($manager);
    }


    public function getIndex()
    {
        return $this->theme->of('salary::salary')->render();
    }


    public function postSave()
    {

    }
}