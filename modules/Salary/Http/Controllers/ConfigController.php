<?php
/**
 * Created by NgocNH.
 * Date: 8/16/16
 * Time: 9:08 AM
 */

namespace Modules\Salary\Http\Controllers;

use League\Fractal\Manager;
use Modules\Core\Http\Controllers\Backend\AdminController;
use Modules\Core\Repositories\ConfigRepository;
use Modules\Salary\Entities\Encrypt;
use Modules\Salary\Http\Requests\SalaryConfigIndexRequest;
use Modules\Salary\Http\Requests\SalaryConfigSaveRequest;

class ConfigController extends AdminController
{

    public function __construct(Manager $manager, ConfigRepository $config)
    {
        parent::__construct($manager);
        $this->config = $config;
    }


    public function getIndex(SalaryConfigIndexRequest $request)
    {
        $data = [
            'config' => []
        ];

        if ($configs = $this->config->getByGroup('salary')) {
            foreach ($configs as $config) {
                $data['config'][$config->key] = $config->value;
            }
        }

        return $this->theme->of('salary::config', $data)->render();
    }


    public function postSave(SalaryConfigSaveRequest $request)
    {
        $attributes = [ ];

        foreach ($request->all() as $group => $configs) {
            if (is_array($configs)) {
                foreach ($configs as $key => $value) {
                    if ($key == 'mail_password') {
                        $value = Encrypt::encrypt($value, $request->secret_key);
                    }

                    $attributes[] = [
                        'group' => $group,
                        'key'   => $key,
                        'value' => $value
                    ];
                }
            }
        }

        if ($config = $this->config->save($attributes)) {
            flash(trans('salary::config.save_success'));

            return redirect()->back();
        }

        flash()->error(trans('salary::config.save_failed'));

        return redirect()->back()->withInput();
    }
}