<?php
/**
 * Created by NgocNH.
 * Date: 8/18/16
 * Time: 8:47 AM
 */

namespace Modules\Salary\Http\Controllers;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Modules\Core\Http\Controllers\Backend\AdminController;
use Modules\Core\Repositories\ConfigRepository;
use Modules\Core\Transformers\ConfigTransformer;
use Modules\Salary\Entities\Encrypt;
use Modules\Salary\Http\Requests\MailIndexRequest;
use Modules\Salary\Http\Requests\MailSendRequest;

class MailController extends AdminController
{

    public function __construct(Manager $manager, ConfigRepository $config)
    {
        parent::__construct($manager);
        $this->config = $config;
    }


    public function getIndex(MailIndexRequest $request)
    {
        return $this->theme->of('salary::salary')->render();
    }


    public function postSend(MailSendRequest $request)
    {
        $configs        = $this->config->getByGroup('salary');
        $salary_configs = [ ];

        foreach ($configs as $config) {
            $salary_configs[$config->key] = $config->value;
        }

        if ( ! $salary_configs['mail_password'] = Encrypt::decrypt($salary_configs['mail_password'],
            $request->secret_key)
        ) {
            return redirect()->back()->withInput();
        }

        config()->set('mail.driver', 'smtp');
        config()->set('mail.host', 'smtp.gmail.com');
        config()->set('mail.port', '465');
        config()->set('mail.from',
            [ 'address' => [ $salary_configs['mail_address'], 'name' => $salary_configs['mail_name'] ] ]);
        config()->set('mail.encryption', 'ssl');
        config()->set('mail.username', $salary_configs['mail_address']);
        config()->set('mail.password', $salary_configs['mail_password']);

        $file_name = $request->file('salary_file')->getClientOriginalName();
        $request->file('salary_file')->move(storage_path('files'), $file_name);

        $salaries = \Excel::load(storage_path("files/$file_name"))->get();

        foreach ($salaries as $salary) {
            $mail_body = $salary_configs['mail_body'];
            $mail_body = str_replace('{{ten_nhan_vien}}', $salary->ho_va_ten, $mail_body);
            $mail_body = str_replace('{{ngay_cong}}', $salary->so_ngay_cong_chinh_thuc, $mail_body);
            $mail_body = str_replace('{{ngay_thu_viec}}', $salary->so_ngay_cong_thu_viec, $mail_body);
            $mail_body = str_replace('{{luong_chinh_thuc}}', $salary->tinh_luong_chinh_thuc, $mail_body);
            $mail_body = str_replace('{{luong_thu_viec}}', $salary->tinh_luong_thu_viec, $mail_body);
            $mail_body = str_replace('{{tong_luong}}', $salary->tong_luong_thoi_gian, $mail_body);
            $mail_body = str_replace('{{an_trua}}', $salary->phu_cap_an_trua_theo_ngay_cong, $mail_body);
            $mail_body = str_replace('{{phu_cap_1}}', $salary->phu_cap_xang_dt, $mail_body);
            $mail_body = str_replace('{{phu_cap_2}}', $salary->phu_cap_khac, $mail_body);
            $mail_body = str_replace('{{phu_cap_3}}', $salary->phu_cap_them_gio, $mail_body);
            $mail_body = str_replace('{{phu_cap_4}}', $salary->phu_cap_ngoai_ngu_theo_thang, $mail_body);
            $mail_body = str_replace('{{luong}}', $salary->tong_thu_nhap, $mail_body);
            $mail_body = str_replace('{{bao_hiem}}', $salary->bao_hiem_bat_buoc, $mail_body);
            $mail_body = str_replace('{{thue_tncn}}', $salary->thue_tncn, $mail_body);
            $mail_body = str_replace('{{luong_nhan}}', $salary->tong_thu_nhap, $mail_body);
            $mail_body = str_replace('{{luong_ck}}', $salary->chuyen_khoan, $mail_body);
            $mail_body = str_replace('{{luong_tien_mat}}', $salary->tien_mat, $mail_body);
            $mail_body = str_replace('{{tong_ngay_cong}}', $salary->thuc_nhan, $mail_body);
            $mail_body = str_replace('{{tai_khoan}}', $salary->so_tk, $mail_body);

            \Mail::queue('salary::mails.salary', [ 'salary' => $salary ],
                function ($message) use ($salary_configs, $salary, $request) {
                    config()->set('mail.driver', 'smtp');
                    config()->set('mail.host', 'smtp.gmail.com');
                    config()->set('mail.port', '465');
                    config()->set('mail.from', [ 'address' => [ $salary_configs['mail_address'], 'name' => $salary_configs['mail_name'] ] ]);
                    config()->set('mail.encryption', 'ssl');
                    config()->set('mail.username', $salary_configs['mail_address']);
                    config()->set('mail.password', $salary_configs['mail_password']);

                    $message->from($salary_configs['mail_address'], $salary_configs['mail_name'])->to($salary->email,
                        $salary->ho_va_ten)->subject($request->mail_subject);
                });
        }

        unlink(storage_path("files/$file_name"));

        return redirect()->back();
    }
}