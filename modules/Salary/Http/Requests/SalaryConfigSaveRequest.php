<?php
/**
 * Created by NgocNH.
 * Date: 8/16/16
 * Time: 2:36 PM
 */

namespace Modules\Salary\Http\Requests;

use Modules\Core\Http\Requests\BaseRequest;

class SalaryConfigSaveRequest extends BaseRequest
{

    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'salary.config.save' ]);
    }


    public function rules()
    {
        return [
            'secret_key' => 'required'
        ];
    }
}