<?php
/**
 * Created by NgocNH.
 * Date: 8/18/16
 * Time: 8:48 AM
 */

namespace Modules\Salary\Http\Requests;

use Modules\Core\Http\Requests\BaseRequest;

class MailIndexRequest extends BaseRequest
{

    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'salary.mail.index' ]);
    }


    public function rules()
    {
        return [ ];
    }
}