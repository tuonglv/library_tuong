<?php
/**
 * Created by NgocNH.
 * Date: 8/16/16
 * Time: 4:56 PM
 */

namespace Modules\Salary\Http\Requests;

use Modules\Core\Http\Requests\BaseRequest;

class SalaryConfigIndexRequest extends BaseRequest
{

    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'salary.config.index' ]);
    }


    public function rules()
    {
        return [ ];
    }
}