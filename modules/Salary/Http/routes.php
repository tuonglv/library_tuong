<?php

Route::group([ 'prefix' => 'salary', 'namespace' => 'Modules\Salary\Http\Controllers' ], function () {
    Route::controller('mail', 'MailController', [
        'getIndex' => 'salary.mail.index',
        'postSend' => 'salary.mail.send',
    ]);
    Route::controller('config', 'ConfigController', [
        'getIndex' => 'salary.config.index',
        'postSave' => 'salary.config.save'
    ]);
});