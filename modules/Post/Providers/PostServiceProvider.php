<?php namespace Modules\Post\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Post\Events\EventHandler;
use Modules\Post\Models\EloquentPost;
use Modules\Post\Models\EloquentTaxonomy;
use Modules\Post\Repositories\PostRepository;
use Modules\Post\Repositories\TaxonomyRepository;

class PostServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->events->subscribe(new EventHandler());

		$this->app->bind(TaxonomyRepository::class, function () {
			return new EloquentTaxonomy();
		});
		$this->app->bind(PostRepository::class, function () {
			return new EloquentPost();
		});
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('post.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'post'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/post');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path . '/modules/post';
		}, \Config::get('view.paths')), [$sourcePath]), 'post');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/post');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'post');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'post');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
