<?php

return [
    'post.post.index'  => [
        'name'         => 'post.post.index',
        'display_name' => 'Post List',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.post.create' => [
        'name'         => 'post.post.create',
        'display_name' => 'Post Create',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.post.edit'   => [
        'name'         => 'post.post.edit',
        'display_name' => 'Post Edit',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.post.trash' => [
        'name'         => 'post.post.trash',
        'display_name' => 'Post Trash',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],


    'post.category.index'   => [
        'name'         => 'post.category.index',
        'display_name' => 'Category List',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.category.create'   => [
        'name'         => 'post.category.create',
        'display_name' => 'Category Create',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.category.edit'   => [
        'name'         => 'post.category.edit',
        'display_name' => 'Category Edit',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.category.trash'   => [
        'name'         => 'post.category.trash',
        'display_name' => 'Category Trash',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],

    'post.tag.index'   => [
        'name'         => 'post.tag.index',
        'display_name' => 'Tag List',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.tag.create'   => [
        'name'         => 'post.tag.create',
        'display_name' => 'Tag Create',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.tag.edit'   => [
        'name'         => 'post.tag.edit',
        'display_name' => 'Tag Edit',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
    'post.tag.trash'   => [
        'name'         => 'post.tag.trash',
        'display_name' => 'Tag Trash',
        'module'       => 'post',
        'module_name'  => 'post::post.post'
    ],
];
