<?php
/**
 * Created by PhpStorm.
 * User: NgocNH
 * Date: 8/29/15
 * Time: 1:59 AM
 */

return [
    'post'          => [
        'route'       => 'post.index',
        'permissions' => [ 'post.post.index' ],
        'active'      => 'post/*',
        'class'       => '',
        'icon'        => 'fa fa-pencil',
        'name'        => 'post',
        'text'        => 'Posts',
        'order'       => 2,
        'subs'        => [
            'post_list' => [
                'route'       => 'post.index',
                'permissions' => [ 'post.post.index' ],
                'active'      => 'post/index',
                'class'       => '',
                'icon'        => '',
                'name'        => 'list',
                'text'        => 'Lists Post',
                'order'       => 1,
            ],
            'post_add'  => [
                'route'       => 'post.create',
                'permissions' => [ 'post.post.create' ],
                'active'      => 'post/create',
                'class'       => '',
                'icon'        => '',
                'name'        => 'list',
                'text'        => 'Add Post ',
                'order'       => 2
            ],
            'category'          => [
                'route'       => 'category.index',
                'permissions' => [ 'post.category.index' ],
                'active'      => 'category/*',
                'class'       => '',
                'icon'        => 'fa fa-list',
                'name'        => 'Category',
                'text'        => 'Categories',
                'order'       => 3,
            ],

            'tag'          => [
                'route'       => 'tag.index',
                'permissions' => [ 'post.tag.index' ],
                'active'      => 'tag/*',
                'class'       => '',
                'icon'        => 'fa fa-tag',
                'name'        => 'tag',
                'text'        => 'Tags',
                'order'       => 4
            ]
        ]
    ]
];