<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 28/06/2016
 * Time: 11:32
 */

namespace Modules\Post\Repositories;


use Modules\Core\Repositories\BaseRepository;

interface PostRepository extends BaseRepository
{
    public function getAll($columns = ['*']);
}