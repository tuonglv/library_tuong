<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 24/06/2016
 * Time: 15:37
 */

namespace Modules\Post\Repositories;


use Modules\Core\Repositories\BaseRepository;

interface TaxonomyRepository extends BaseRepository
{
    public function getAll($type = 'post_category', $columns = ['*']);

    public function paginateCategoriesByType($type = 'post_category', $limit = 10, $columns = [ '*' ]);
}