<div class="row">
	<div class="col-sm-12">
		<div class="panel">
			<div class="panel-header">
				<h3 class="panel-title">
					<i class="fa fa-table"></i> {!! trans('post::tag.list') !!}
				</h3>

				<div class="control-btn">
					<a href="{!! route('tag.create') !!}" class="btn btn-sm">{!! trans('post::tag.create') !!}</a>
				</div>
			</div>
			<div class="panel-body table-responsive">
				<div role="grid" class="dataTables_wrapper form-inline no-footer">
					<table id="user-datatables" class="table table-hover dataTable no-footer">
						<thead>
						<tr>
							<th>{!! trans('post::tag.name') !!}</th>
							<th>{!! trans('post::tag.description') !!}</th>
							<th>{!! trans('post::tag.order') !!}</th>
							<th>{!! trans('post::tag.action') !!}</th>
						</tr>
						</thead>
						<tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#user-datatables').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: '{!! route('tag.datatables') !!}',
				type: 'POST'
			},
			columns: [
				{data: 'name', name: 'name'},
				{data: 'description', name: 'description'},
				{data: 'order', order: 'order'},
				{data: 'action', name: 'action', orderable: false, searchable: false}
			]
		});
	});
</script>