<div id="main" class="regular">
    <div><h1 class="find_with">Find with
            Tag: {!! isset($posts_detail_by_taxonomies) && $posts_detail_by_taxonomies ? $posts_detail_by_taxonomies : '' !!} </h1>
    </div>
    </br>

    @if(isset($posts_detail_by) && $posts_detail_by )
        @foreach($posts_detail_by as $post)
            <article id="post-6"
                     class="list-item post-6 post type-post status-publish format-standard has-post-thumbnail hentry category-lifestyle">

                <div class="post-img">
                    <a href="{!! isset($post['slug']) ? URL::to('library/post/detail\/').$post['slug'] : '' !!}"><img
                                width="500" height="380"
                                src="{!! isset($post['image']['url']) ? $post['image']['url'] : 'http://192.168.0.138//files/files/no-image.png' !!}"
                                alt="{!! isset($post['title']) ? str_limit($post['excerpt'] ,170) : '' !!}"
                                class="attachment-misc-thumb size-misc-thumb wp-post-image" alt="flower-girl"></a>
                </div>

                <div class="list-content">
                    <div class="post-header">
                        <div class="post_title">
                            <a href="{!! isset($post['slug']) ? URL::to('library/post/detail\/').$post['slug'] : '' !!}">{!! isset($post['title']) ? str_limit(mb_strtoupper($post['title'],"UTF-8") ,110) : '' !!}</a>
                        </div>
                    </div>

                    <div class="post-entry">

                        <p>{!! isset($post['excerpt']) ? str_limit($post['excerpt'] ,170) : '' !!}</p>

                    </div>

                    @if(isset($post['taxonomies']) && $post['taxonomies'])
                        @foreach($post['taxonomies'] as $category)
                            <span class="cat"><a
                                        href="{!! isset($category['id']) ? URL::to('library/post/category\/').$category['id'] : '#' !!}"
                                        rel="category tag"> {!! isset($category) ? $category['name'] : '' !!}</a>&nbsp;&nbsp;</span>
                        @endforeach
                    @endif

                    <div class="post-meta">
                        <span class="meta-info">
                                 {!! isset($post['updated_at']) ? $post['updated_at'] : '' !!}
                            by <a href="{!! isset($post['author']['id']) && $post['author']['id'] ? URL::to('library/author\/').$post['author']['id'] : '#'  !!}"
                                  title="Posts by {!! isset($post['author']['name']) ? $post['author']['name'] : '' !!}"
                                  rel="author">{!! isset($post['author']['name']) ? $post['author']['name'] : '' !!}</a>
                        </span>
                    </div>
                </div>
            </article>
        @endforeach
        @if(isset($paginator) && $paginator->hasPages())
            <div class="front_pagination">
                <div class="col-md-6">
                    <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                        {!! theme()->partial('paginator', ['paginator' => $paginator]) !!}
                    </div>
                </div>

            </div>
        @endif

    @else
        No Result !!
    @endif
</div>