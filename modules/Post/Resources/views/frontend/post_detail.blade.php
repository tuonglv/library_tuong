<div id="main">
    <article id="post-6"
             class="post-6 post type-post status-publish format-standard has-post-thumbnail hentry category-lifestyle">
        {{--<div class="post-header">--}}
        {{--@if(isset($post_detail['taxonomies']) && $post_detail['taxonomies']->toArray())--}}
        {{--@foreach($post_detail['taxonomies']->toArray() as $category)--}}
        {{--<span class="cat title_post_detail"><a--}}
        {{--href="{!! isset($category['id']) ? \Illuminate\Support\Facades\URL::to('/library/post/category').'/'.$category['id'] : '#' !!}"--}}
        {{--rel="category tag"> {!! isset($category) ? $category['name'] : '' !!}</a>&nbsp;&nbsp;</span>--}}
        {{--@endforeach--}}
        {{--@endif--}}
        {{--<h2>--}}
        {{--<a href="{!! isset($post_popular['slug']) ? Url::to('post/detail').'/'.$post_popular['slug'] : '' !!}">{!! isset($post_popular['title']) ? $post_popular['title'] : '' !!}</a>--}}
        {{--</h2>--}}

        {{--</div>--}}
        <h1 class="title">{!! isset($post_detail['title']) ? mb_strtoupper($post_detail['title'],"UTF-8") : '' !!}</h1>
        <div class="post-entry">
            <p>{!! isset($post_detail['content']) ? $post_detail['content'] : '' !!}</p>
        </div>

        <div class="post-meta">

		<span class="meta-info">
                {!! isset($post_detail['updated_at']) ? $post_detail['updated_at'] : '' !!}
            by <a href="{!! isset($post_detail['author']['id']) && $post_detail['author']['id'] ? URL::to('library/author\/').$post_detail['author']['id'] : '#'  !!}"
                  title="Posts by {!! isset($post_detail['author']['name']) ? $post_detail['author']['name'] : '' !!}"
                  rel="author">{!! isset($post_detail['author']['name']) ? $post_detail['author']['name'] : '' !!}</a>
		</span>

            <div class="post-share">

                <a target="_blank"
                   href="https://www.facebook.com/sharer/sharer.php?u={!! isset($post_detail['slug']) ? URL::to('library/post/detail/'.$post_detail['slug']): '' !!}"><i
                            class="fa fa-facebook"></i></a>
                <a target="_blank"
                   href="https://twitter.com/home?status=Check%20out%20this%20article:%20Floral Wallpaper%20-%20{!! isset($post_detail['slug']) ? URL::to('/post/detail/'.$post_detail['slug']) : '' !!}"><i
                            class="fa fa-twitter"></i></a>

                {{--<a target="_blank"--}}
                {{--href="https://pinterest.com/pin/create/button/?url={!! isset($post_detail['slug']) ? URL::to('/post/detail/').'/'.$post_detail['slug'] : '' !!}&amp;media={!! isset($post_detail['image']['url']) ? URL::to('/post/detail/').$post_detail['image']['url'] : '' !!}&amp;description={!! isset($post_detail['title']) ? $post_detail['title'] : '' !!}"><i--}}
                {{--class="fa fa-pinterest"></i></a>--}}
                <a target="_blank"
                   href="https://plus.google.com/share?url={!! isset($post_detail['slug']) ? URL::to('/post/detail/'.$post_detail['slug']) : '' !!}"><i
                            class="fa fa-google-plus"></i></a>
                {{--<a href="{!! isset($post_detail['slug']) ? URL::to('/library/post/detail/').'/'.$post_detail['slug'] : '' !!}"><i--}}
                {{--class="fa fa-comments"></i></a>--}}
            </div>

        </div>

        <div class="post-author">

            <div class="author-img">
                <img alt="{!! isset($post_detail['author']['images']) && $post_detail['author']['images'] ? $post_detail['author']['images'] : '' !!}"
                     src="{!! isset($post_detail['author']['avatar']) ? $post_detail['author']['avatar'] : '/themes/default/assets/img/avatars/avatar1_big@2x.png' !!}"
                     srcset="{!! isset($post_detail['author']['avatar']) ? $post_detail['author']['avatar'] : '/themes/default/assets/img/avatars/avatar1_big@2x.png' !!}"
                     class="avatar avatar-80 photo" height="80" width="80"></div>

            <div class="author-content">
                <h5>
                    <a href="{!! isset($post_detail['author']['id']) && $post_detail['author']['id'] ? URL::to('library/author\/').$post_detail['author']['id'] : '' !!}"
                       title="Posts by solopine"
                       rel="author">{!! isset($post_detail['author']['name']) && $post_detail['author']['name'] ? $post_detail['author']['name'] : '' !!}</a>
                </h5>
                <p>
                    {!! isset($post_detail['author']['meta']['about']) ? str_limit($post_detail['author']['meta']['about'],200) : 'facebook'!!}
                </p>
            </div>
        </div>
    </article>

    <div class="post-related"><h4 class="block-heading">You Might Also Like</h4>
        @if(isset($related_articles) && $related_articles)
            @foreach($related_articles as $related_article)
                @if($related_article['id'] != $post_detail['id'])
                    <div class="item-related">
                        <a href="{!! isset($related_article['slug']) ? URL::to('/library/post/detail/').'/'.$related_article['slug'] : '' !!}"><img
                                    width="500" height="380"
                                    src="{!! isset($related_article['image']['url']) ? $related_article['image']['url'] : '/files/images/no-image.png' !!}"
                                    class="attachment-misc-thumb size-misc-thumb wp-post-image"
                                    alt="naturetrip"></a>

                        <h3>
                            <a href="{!! isset($related_article['slug']) ? URL::to('post/detail/').'/'.$related_article['slug'] : '' !!}">{!! isset($related_article['title']) && $related_article['title'] ? $related_article['title'] : '' !!}</a>
                        </h3>
                        <span class="date">{!! isset($related_article['created_at']) && $related_article['created_at'] ? $related_article['created_at'] : '' !!}</span>

                    </div>
                @endif
            @endforeach
        @endif

    </div>
    {{--comment fb--}}
    <div class="fb_comment">
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

        <div class="fb-comments"
             data-href="{!! isset($post_detail['slug']) ? URL::to('post/detail/').'/'.$post_detail['slug'] : '' !!}"
             data-width="740" data-numposts="5"></div>
    </div>
</div>
