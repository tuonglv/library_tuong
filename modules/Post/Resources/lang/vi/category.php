<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 27/06/2016
 * Time: 13:54
 */
return [
    'name'                          => 'Ten',
    'description'                   => 'Mo ta',
    'id'                            => '#',
    'list'                          => 'Danh Sach Category',
    'order'                         => 'order',
    'exit'                          => 'Edit',
    'create'                         => 'Them Moi',
    'action'                        => 'Thao tac',
    'find_not_found'                => '<strong>Error!</strong> User <strong><i>:id</i></strong> not found.',
    'create_success'                => '<strong>Success!</strong> User <strong><i>:name</i></strong> has been created.',
    'update_success'                => '<strong>Success!</strong> User <strong><i>:name</i></strong> has been updated.',
    'lock_success'                  => '<strong>Success!</strong> User <strong><i>:name</i></strong> has been locked.',
    'unlock_success'                => '<strong>Success!</strong> User <strong><i>:name</i></strong> has been unlocked.',
    'trash_success'                 => '<strong>Success!</strong> User <strong><i>:name</i></strong> has been trashed.',
    'restore_success'               => '<strong>Success!</strong> User <strong><i>:name</i></strong> has been restored.',
    'delete_success'                => '<strong>Success!</strong> User <strong><i>:name</i></strong> has been deleted.',
    'error'                         => '<strong>Error!</strong> Opps, something wrong happen in process, this process is failed.',
];