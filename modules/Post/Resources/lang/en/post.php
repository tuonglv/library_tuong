<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 27/06/2016
 * Time: 13:54
 */
return [
    'id'                            => '#',
    'category_placeholder'          => 'Select Category',
    'tag_placeholder'               => 'Select Tag',
    'posts'                         => 'List Posts',
    'author'                         => 'Author',
    'draft'                         => 'Draft',
    'title'                         => 'Title',
    'pending'                       => 'Pending',
    'publish'                       => 'Publish',
    'comment_status'                => 'Comment_status',
    'image'                         => 'Thumbnail',
    'add_image'                     => 'Add Image',
    'excerpt'                       => 'Excerpt',
    'content'                       => 'Content',
    'status'                        => 'Status',
    'taxonomies'                    => 'Category',
    'tags'                          => 'Tags',
    'list'                          => 'Post List',
    'create'                        => 'Add New',
    'edit'                          => 'Edit',
    'name'                          => 'Name',
    'description'                   => 'Description',
    'order'                         => 'Order',
    'action'                        => 'Action',
    'index'                        => 'Show all posts',
    'find_not_found'                => '<strong>Error!</strong> Post <strong><i>:id</i></strong> not found.',
    'create_success'                => '<strong>Success!</strong> Post <strong><i>:name</i></strong> has been created.',
    'update_success'                => '<strong>Success!</strong> Post <strong><i>:name</i></strong> has been updated.',
    'lock_success'                  => '<strong>Success!</strong> Post <strong><i>:name</i></strong> has been locked.',
    'unlock_success'                => '<strong>Success!</strong> Post <strong><i>:name</i></strong> has been unlocked.',
    'trash_success'                 => '<strong>Success!</strong> Post <strong><i>:name</i></strong> has been trashed.',
    'restore_success'               => '<strong>Success!</strong> Post <strong><i>:name</i></strong> has been restored.',
    'delete_success'                => '<strong>Success!</strong> Post <strong><i>:name</i></strong> has been deleted.',
    'error'                         => '<strong>Error!</strong> Opps, something wrong happen in process, this process is failed.',
];