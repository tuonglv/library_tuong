<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 24/06/2016
 * Time: 15:09
 */

namespace Modules\Post\Http\Controllers;


use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Modules\Core\Http\Controllers\Backend\AdminController;
use Modules\Post\Http\Requests\TagListRequest;
use Modules\Post\Http\Requests\TagUpdateRequest;
use Modules\Post\Http\Requests\TaxonomyStoreRequest;
use Modules\Post\Repositories\TaxonomyRepository;
use Modules\Post\Transformers\TagTaxonomyDatatablesTransformer;
use Modules\Post\Transformers\TaxonomyTransformer;
use Yajra\Datatables\Facades\Datatables;

class TagController extends AdminController
{
    public function __construct(Manager $manager, TaxonomyRepository $taxonomy)
    {
        parent::__construct($manager);
        $this->taxonomy = $taxonomy;
    }

    public function getIndex(TagListRequest $request)
    {
        return $this->theme->of('post::tag/tag_list')->render();
    }

    public function getCreate()
    {
        return $this->theme->of('post::tag/tag_list')->render();
    }

    public function getEdit($id)
    {
        $taxonomy         = $this->taxonomy->find($id);
        $taxonomy         = new Item($taxonomy, new TaxonomyTransformer(true), 'taxonomy');
        $taxonomy         = $this->manager->createData($taxonomy)->toArray();
        $data['tag'] = $taxonomy;

        if ($response = event('tag.edit.before.render', [ $data ])) {
            $data = $response[0];
        }
        return $this->theme->of('post::tag/tag_list', $data)->render();
    }

    public function postUpdate(TagUpdateRequest $request)
    {
        $attributes = [
            'attributes' => [
                'type' => 'post_tag',
                'parent' => null,
                'order' => $request->order ?: 1,
                'count' => 0
            ],
            'translate' => []
        ];

        foreach ($request->translate as $locale_id => $translate) {
            if ($translate['name']) {
                $attributes['translate'][] = [
                    'name' => $translate['name'],
                    'description' => $translate['description'],
                    'locale_id' => $locale_id
                ];
            }
        }
        if ($tag = $this->taxonomy->update($request->one,$attributes)) {
            event('tag.after.update');

            flash()->error(trans('post::tag.update_success'),['name' => $tag->name ?: '  updated']);
            return redirect()->route('tag.index');
        }

        return redirect()->back()->withInput();
    }

    public function postStore(TaxonomyStoreRequest $request)
    {
        $attributes = [
            'attributes' => [
                'type' => 'post_tag',
                'parent' => null,
                'order' => $request->order ?: 1,
                'count' => 0
            ],
            'translate' => []
        ];

        foreach ($request->translate as $locale_id => $translate) {
            if ($translate['name']) {
                $attributes['translate'][] = [
                    'name' => $translate['name'],
                    'description' => $translate['description'],
                    'locale_id' => $locale_id
                ];
            }
        }
        if ($tag = $this->taxonomy->create($attributes)) {
            event('tag.after.store');
            flash()->error(trans('post::tag.create_success'),['name' => $tag->name ?: '  created']);
            return redirect()->route('tag.index');
        }
        flash()->error(trans('post::tag.create_fail'),['name' =>  isset($tag->name) ? $tag->name : '' .'  create fail']);
        return redirect()->back()->withInput();
    }

    public function postDatatables()
    {
        return Datatables::of($this->taxonomy->getAll('post_tag'))->setTransformer(new TagTaxonomyDatatablesTransformer('tag'))->make(true);
    }


    public function postTrash($id, $external = false)
    {
        if ($tag = $this->taxonomy->find($id)) {
            if ($this->taxonomy->trash($tag)) {
                flash(trans('post::tag.trash_success', [ 'name' => $tag->name ?: ' deleted' ]));

                return $external ? $tag : redirect()->route('tag.index');
            }

            flash()->error(trans('post::general.error'));

            return $external ? false : redirect()->route('tag.index');
        }

        flash()->error(trans('post::tag.find_not_found', [ 'id' => $id ]));

        return $external ? false : redirect()->route('tag.index');

        flash()->error(trans('post::tag.trash_success'),['name' => $tag->name ?: '  deleted']);
    }

    public function getSelect2()
    {
        $taxonomy   = $this->taxonomy->paginateCategoriesByType('post_tag', 20);
        $collection = new Collection($taxonomy->getCollection(), new TaxonomyTransformer(), 'items');
        $collection->setPaginator(new IlluminatePaginatorAdapter($taxonomy));
        $data = $this->manager->createData($collection)->toArray();
        return $data;
    }

}