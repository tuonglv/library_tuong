<?php namespace Modules\Post\Http\Controllers;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Modules\Core\Http\Controllers\Backend\BackendController;
use Modules\Post\Entities\Post;
use Modules\Post\Http\Requests\PostListRequest;
use Modules\Post\Http\Requests\PostStoreRequest;
use Modules\Post\Http\Requests\PostUpdateRequest;
use Modules\Post\Models\EloquentPost;
use Modules\Post\Repositories\PostRepository;
use Modules\Post\Transformers\PostDatatablesTransformer;
use Modules\Post\Transformers\PostTransformer;
use Yajra\Datatables\Facades\Datatables;

class PostController extends BackendController
{

    public function __construct(Manager $manager, PostRepository $post)
    {
        parent::__construct($manager);
        $this->post = $post;
    }

    public function getIndex(PostListRequest $request)
    {
        return $this->theme->of('post::post/post_list')->render();
    }

    public function getCreate()
    {
        return $this->theme->of('post::post/post_view')->render();
    }

    public function getEdit($id)
    {
        $post = $this->post->find($id);
        $post = new Item($post, new PostTransformer(true), 'post');
        $post = $this->manager->createData($post)->toArray();
        $data['post'] = $post;

        if ($response = event('post.edit.before.render', [$data])) {
            $data = $response[0];
        }
        return $this->theme->of('post::post/post_view', $data)->render();
    }

    public function postStore(PostStoreRequest $request)
    {
        $attributes = [
            'attributes' => [
                'comment_status' => $request->comment_status,
                'parent' => $request->parent ?: null,
                'author' => auth()->user()['id'] ?: 1,
                'order' => $request->order ?: 1,
                'type' => $request->type ?: 'post',
                'status' => $request->status ?: 1,
                'comment_count' => $request->comment_count ?: 0,
            ],
            'translate' => [],
            'categories' => $request->taxonomies ? explode(',', $request->taxonomies) : false,
            'tags' => $request->tags ? explode(',', $request->tags) : false,
            'image' => $request->image ?: null,
        ];

        foreach ($request->translate as $locale_id => $translate) {
            $attributes['translate'][] = [
                'title' => $translate['title'],
                'excerpt' => $translate['excerpt'],
                'content' => $translate['content'],
                'locale_id' => $locale_id
            ];
        }
        if ($post = $this->post->create($attributes)) {
            event('post.after.store');
            return redirect()->route('post.index');
        }
        return redirect()->back()->withInput();
    }

    public function postUpdate(PostUpdateRequest $request)
    {
        $attributes = [
            'attributes' => [
                'comment_status' => $request->comment_status,
                'parent' => $request->parent ?: null,
                'author' => auth()->user()['id'] ?: 1,
                'order' => $request->order ?: 1,
                'type' => $request->type ?: 'post',
                'status' => $request->status ?: 1,
                'comment_count' => $request->comment_count ?: 0,
            ],
            'translate' => [],
            'categories' => $request->taxonomies ? explode(',', $request->taxonomies) : false,
            'tags' => $request->tags ? explode(',', $request->tags) : false,
            'image' => $request->image ?: null,
        ];

        foreach ($request->translate as $locale_id => $translate) {
            $attributes['translate'][] = [
                'title' => $translate['title'],
                'excerpt' => $translate['excerpt'],
                'content' => $translate['content'],
                'locale_id' => $locale_id
            ];
        }
        if ($post = $this->post->update($request->one, $attributes)) {
            event('post.after.update');

            flash()->error(trans('post::post.update_success'), ['name' => $post->title ?: '  updated']);
            return redirect()->route('post.index');
        }

        return redirect()->back()->withInput();
    }

    public function postDatatables()
    {
        if(auth()->user()->can('access.all')){
            return Datatables::of($this->post->select('*')->get())->setTransformer(new PostDatatablesTransformer())->make(true);
        }else{
            return Datatables::of($this->post->select('*')->where('posts.author','=',auth()->user()->id)->get())->setTransformer(new PostDatatablesTransformer())->make(true);
        }
    }


    public function postTrash($id, $external = false)
    {
        if ($post = $this->post->find($id)) {
            if ($this->post->trash($post)) {
                flash(trans('post::post.trash_success', ['name' => $post->title ?: ' deleted']));

                return $external ? $post : redirect()->route('post.index');
            }

            flash()->error(trans('post::general.error'));

            return $external ? false : redirect()->route('post.index');
        }

        flash()->error(trans('post::post.find_not_found', ['id' => $id]));

        return $external ? false : redirect()->route('post.index');
    }


}
