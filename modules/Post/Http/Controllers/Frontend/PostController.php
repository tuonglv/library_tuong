<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 01/07/2016
 * Time: 09:10
 */

namespace Modules\Post\Http\Controllers\Frontend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Modules\Core\Entities\Locale;
use Modules\Core\Http\Controllers\Frontend\HomeController;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\Taxonomy;
use Modules\Post\Entities\TaxonomyDetail;
use Modules\Post\Http\Requests\PostSearchRequest;
use Modules\Post\Repositories\PostRepository;
use Modules\Post\Transformers\PostTransformer;
use Modules\Post\Transformers\TaxonomyTransformer;

class PostController extends HomeController
{

    public function __construct(Manager $manager, PostRepository $post)
    {
        parent::__construct($manager);
        $this->post = $post;
    }

    public function eventPopular($data)
    {
        $posts =  Post::where('posts.status', '=', 'publish')->orderBy('created_at', 'desc')->paginate(8);
        $collection = new Collection($posts->getCollection(), new PostTransformer, 'post_populars');
        $collection->setPaginator(new IlluminatePaginatorAdapter($posts));
        $data['post_populars']              = $this->manager->createData($collection)->toArray();
        $data['paginator'] = $posts;

        if ($response = event('frontend.post_tops.before.render', [ $data ])) {
            $data = $response[0];
        }
        if ($response = event('frontend.post_taxonomies.before.render', [ $data, 'post_tag' ])) {
            $data = $response[0];
        }
        return $data;
    }

    public function getDetail(Request $request){
        $post_detail = $this->post->select('posts.*')->join('post_detail', 'posts.id' , '=', 'post_detail.post_id')->where('slug', '=' ,$request->one)->first();
        $post_detail = new Item($post_detail, new PostTransformer(true), 'post_detail');
        $post_detail = $this->manager->createData($post_detail)->toArray();
        $data['post_detail'] = $post_detail;

        $data['sidebar_status'] = true;
        if ($response = event('frontend.post_tops.before.render', [ $data ])) {
            $data = $response[0];
        }
        if ($response = event('frontend.post_taxonomies.before.render', [ $data, 'post_tag' ])) {
            $data = $response[0];
        }
        $post_taxonomy_id = isset($data['post_detail']['taxonomies'][0]['id']) ? $data['post_detail']['taxonomies'][0]['id'] : '';;
        $related_articles = $this->post->select('posts.*')
            ->join('post_taxonomy', 'posts.id' , '=', 'post_taxonomy.post_id')
            ->where('taxonomy_id', '=' ,$post_taxonomy_id)
            ->where('posts.id', '<>' ,$post_detail['id'])
            ->limit(3)->orderBy('posts.created_at','desc')
            ->get();
        foreach ($related_articles as $related_article){

            $related_articles = new Item($related_article, new PostTransformer(true), 'post_detail');
            $related_articles = $this->manager->createData($related_articles)->toArray();
            $data['related_articles'][] = $related_articles;
        }
        View::share($data);
        return $this->theme->of('post::frontend/post_detail')->render();
    }

    public function getTag(Request $request){
        $tag_details = $this->post->select('posts.*')->join('post_taxonomy', 'posts.id' , '=', 'post_taxonomy.post_id')->where('taxonomy_id', '=' ,$request->one)->limit(5)->get();
        foreach ($tag_details as $tag_detail){
            $tag_details = new Item($tag_detail, new PostTransformer(true), 'post_detail');
            $tag_details = $this->manager->createData($tag_details)->toArray();
            $data['posts_detail_by'][] = $tag_details;
        }
        $data['posts_detail_by_taxonomies'] = Taxonomy::select(['*'])->where('id','=',$request->one)->first()['name'];
        $data['sidebar_status'] = true;
        if ($response = event('frontend.post_tops.before.render', [ $data ])) {
            $data = $response[0];
        }
        if ($response = event('frontend.post_taxonomies.before.render', [ $data, 'post_tag' ])) {
            $data = $response[0];
        }

        View::share($data);
        return $this->theme->of('post::frontend/tag_detail')->render();
    }

    public function getCategory(Request $request){
        $taxonomies_details = $this->post->select('posts.*')
            ->join('post_taxonomy', 'post_taxonomy.post_id' , '=', 'posts.id')
            ->join('taxonomy_detail', 'post_taxonomy.taxonomy_id' , '=', 'taxonomy_detail.taxonomy_id')
            ->where('taxonomy_detail.slug', '=' ,$request->one)
            ->where('posts.status', '=' ,'publish')
            ->limit(5)
            ->get();
        foreach ($taxonomies_details as $taxonomies_detail){
            $taxonomies_details = new Item($taxonomies_detail, new PostTransformer(true), 'post_detail');
            $taxonomies_details = $this->manager->createData($taxonomies_details)->toArray();
            $data['posts_detail_by'][] = $taxonomies_details;
        }
        $data['posts_detail_by_taxonomies'] = TaxonomyDetail::select(['*'])
            ->where('slug','=',$request->one)
            ->first()['name'];
        $data['sidebar_status'] = true;
        if ($response = event('frontend.post_tops.before.render', [ $data ])) {
            $data = $response[0];
        }
        if ($response = event('frontend.post_taxonomies.before.render', [ $data, 'post_tag' ])) {
            $data = $response[0];
        }
        View::share($data);
        return $this->theme->of('post::frontend/category_detail')->render();
    }

    function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

    public function eventTaxonomies($data , $type = 'post_category'){

        $taxonomies = Taxonomy::select('*')->get();
        foreach($taxonomies as $taxonomie){
            if($taxonomie['type'] == 'post_category'){
                $categories = new Item($taxonomie, new TaxonomyTransformer(true), 'taxonomy');
                $data['categories'][] = $this->manager->createData($categories)->toArray();
            }else if($taxonomie['type'] == 'post_tag'){
                $tags = new Item($taxonomie, new TaxonomyTransformer(true), 'taxonomy');
                $data['tags'][] = $this->manager->createData($tags)->toArray();
            }
        }
        return $data;
    }

    public function eventPostTops($data){
        //post top 10
        $post_tops = $this->post->select('*')->limit(7)->where('posts.status', '=' , 'publish')->orderby('created_at','desc')->get();
        foreach($post_tops as $post_top){
            $post_tops = new Item($post_top, new PostTransformer(true), 'post_tops');
            $data['post_tops'][] = $this->manager->createData($post_tops)->toArray();
        }
        return $data;
    }

    public function getSearch(PostSearchRequest $request)
    {
        $locale_id = Locale::select(['id'])->where('code','=', \App::getLocale() )->first()->toArray();
        $post_searchs = $this->post->select('posts.*')
            ->join('post_detail', 'posts.id' , '=', 'post_detail.post_id')
            ->where('post_detail.title', 'like' , '%'.$request->title.'%')
            ->where('post_detail.locale_id', '=' , $locale_id)
            ->where('posts.status', '=' , 'publish')
            ->get();
        foreach($post_searchs as $post_search){
            $post_searchs = new Item($post_search, new PostTransformer(true), 'post_searchs');
            $data['post_searchs'][] = $this->manager->createData($post_searchs)->toArray();
        }
        $data['sidebar_status'] = true;
        if ($response = event('frontend.post_tops.before.render', [ $data ])) {
            $data = $response[0];
        }
        if ($response = event('frontend.post_tags.before.render', [ $data, 'post_tag' ])) {
            $data = $response[0];
        }
        View::share($data);
        return $this->theme->of('post::frontend/search')->render();
    }

}