<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 12/07/2016
 * Time: 17:32
 */

namespace Modules\Post\Http\Controllers\Frontend;


use League\Fractal\Manager;
use Modules\Core\Http\Controllers\Frontend\HomeController;
use Modules\Core\Repositories\UserRepository;

class AuthorController extends HomeController
{
    public function __construct(Manager $manager, UserRepository $user)
    {
        parent::__construct($manager);
        $this->user = $user;
    }

    public function getIndex(){
        echo 'index';
    }

}