<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 24/06/2016
 * Time: 15:09
 */

namespace Modules\Post\Http\Controllers;


use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Modules\Core\Http\Controllers\Backend\AdminController;
use Modules\Post\Http\Requests\CategoryUpdateRequest;
use Modules\Post\Http\Requests\TaxonomyStoreRequest;
use Modules\Post\Repositories\TaxonomyRepository;
use Modules\Post\Transformers\TaxonomyDatatablesTransformer;
use Modules\Post\Transformers\TaxonomyTransformer;
use Yajra\Datatables\Facades\Datatables;

class CategoryController extends AdminController
{
    public function __construct(Manager $manager, TaxonomyRepository $taxonomy)
    {
        parent::__construct($manager);
        $this->taxonomy = $taxonomy;
    }

    public function getIndex()
    {
        return $this->theme->of('post::category/category_list')->render();
    }

    public function getCreate()
    {
        return $this->theme->of('post::category/category_list')->render();
    }

    public function getEdit($id)
    {
        $taxonomy = $this->taxonomy->find($id);
        $taxonomy = new Item($taxonomy, new TaxonomyTransformer(true), 'taxonomy');
        $taxonomy = $this->manager->createData($taxonomy)->toArray();
        $data['category'] = $taxonomy;

        if ($response = event('category.edit.before.render', [$data])) {
            $data = $response[0];
        }
        return $this->theme->of('post::category/category_list', $data)->render();
    }

    public function postUpdate(CategoryUpdateRequest $request)
    {
        $attributes = [
            'attributes' => [
                'type' => 'post_category',
                'parent' => null,
                'order' => $request->order ?: 1,
                'count' => 0
            ],
            'translate' => []
        ];

        foreach ($request->translate as $locale_id => $translate) {
            if ($translate['name']) {
                $attributes['translate'][] = [
                    'name' => $translate['name'],
                    'description' => $translate['description'],
                    'locale_id' => $locale_id
                ];
            }
        }
        if ($category = $this->taxonomy->update($request->one, $attributes)) {
            event('tag.after.update');
            flash()->success(trans('post::category.update_success'), ['name' => $category->name ?: 'updated']);
            return redirect()->route('category.index');
        }

        return redirect()->back()->withInput();
    }

    public function postStore(TaxonomyStoreRequest $request)
    {
        $attributes = [
            'attributes' => [
                'type' => 'post_category',
                'parent' => null,
                'order' => $request->order ?: 1,
                'count' => 0
            ],
            'translate' => []
        ];

        foreach ($request->translate as $locale_id => $translate) {
            if ($translate['name']) {
                $attributes['translate'][] = [
                    'name' => $translate['name'],
                    'description' => $translate['description'],
                    'locale_id' => $locale_id
                ];
            }
        }

        if ($category = $this->taxonomy->create($attributes)) {
            event('category.after.store');
            flash()->error(trans('post::category.create_success'), ['name' => $category->name ?: '  created']);
            return redirect()->route('category.index');
        }

        return redirect()->back()->withInput();
    }


    public function postTrash($id, $external = false)
    {
        if ($tag = $this->taxonomy->find($id)) {
            if ($this->taxonomy->trash($tag)) {
                flash(trans('post::category.trash_success', ['name' => $tag->name ?: ' deleted']));

                return $external ? $tag : redirect()->route('category.index');
            }

            flash()->error(trans('post::general.error'));

            return $external ? false : redirect()->route('category.index');
        }

        flash()->error(trans('post::category.find_not_found', ['id' => $id]));

        return $external ? false : redirect()->route('category.index');
    }


    public function postDatatables()
    {
        return Datatables::of($this->taxonomy->getAll())->setTransformer(TaxonomyDatatablesTransformer::class)->make(true);
    }

    public function getSelect2()
    {
        $taxonomy = $this->taxonomy->paginateCategoriesByType('post_category', 20);
        $collection = new Collection($taxonomy->getCollection(), new TaxonomyTransformer, 'items');
        $collection->setPaginator(new IlluminatePaginatorAdapter($taxonomy));
        $data = $this->manager->createData($collection)->toArray();

        return $data;
    }
}