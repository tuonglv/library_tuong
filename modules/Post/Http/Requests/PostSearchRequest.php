<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 01/07/2016
 * Time: 15:42
 */

namespace Modules\Post\Http\Requests;


use Modules\Core\Http\Requests\BaseRequest;

class PostSearchRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}