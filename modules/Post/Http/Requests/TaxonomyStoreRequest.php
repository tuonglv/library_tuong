<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 24/06/2016
 * Time: 15:28
 */

namespace Modules\Post\Http\Requests;


use Modules\Core\Http\Requests\BaseRequest;

class TaxonomyStoreRequest extends BaseRequest
{
    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'post.tag.create' ]);
    }

    public function rules()
    {
        return [

        ];
    }

}