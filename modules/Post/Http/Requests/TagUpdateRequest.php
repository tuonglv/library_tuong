<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 27/06/2016
 * Time: 15:23
 */

namespace Modules\Post\Http\Requests;


use Modules\Core\Http\Requests\BaseRequest;

class TagUpdateRequest extends BaseRequest
{
    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'post.tag.edit' ]);
    }

    public function rules()
    {
        return [

        ];
    }
}