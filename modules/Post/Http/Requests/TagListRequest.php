<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 01/07/2016
 * Time: 15:42
 */

namespace Modules\Post\Http\Requests;


use Modules\Core\Http\Requests\BaseRequest;

class TagListRequest extends BaseRequest
{
    public function authorize()
    {
        return auth()->user()->can(['access.all', 'post.tag.index']);
    }

    public function rules()
    {
        return [];
    }
}