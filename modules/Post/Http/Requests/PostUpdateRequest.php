<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 28/06/2016
 * Time: 10:41
 */

namespace Modules\Post\Http\Requests;


use Modules\Core\Http\Requests\BaseRequest;

class PostUpdateRequest extends BaseRequest
{
    public function authorize()
    {
        return auth()->user()->can(['access.all', 'post.post.edit']);
    }

    public function rules()
    {
        return [];
    }
}