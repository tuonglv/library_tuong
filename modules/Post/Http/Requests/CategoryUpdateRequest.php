<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 27/06/2016
 * Time: 15:23
 */

namespace Modules\Post\Http\Requests;


use Modules\Core\Http\Requests\BaseRequest;

class CategoryUpdateRequest extends BaseRequest
{
    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'post.category.update' ]);
    }

    public function rules()
    {
        return [

        ];
    }
}