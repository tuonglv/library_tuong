<?php
$api = app('api.router');
Route::group([ 'prefix' => 'library/admin' , 'namespace' => 'Modules\Post\Http\Controllers'], function()
{

	Route::controller('post', 'PostController', [
		'getIndex'    	 => 'post.index',
		'getCreate'   	 => 'post.create',
		'getEdit'     	 => 'post.edit',
		'postStore'   	 => 'post.store',
		'postUpdate'  	 => 'post.update',
		'postDestroy' 	 => 'post.destroy',
		'postTrash'   	 => 'post.trash',
		'postDatatables' => 'post.datatables',
		'getError' 	  	 => 'post.error',
	]);

	Route::controller('tag', 'TagController', [
		'getIndex'    	 => 'tag.index',
		'getCreate'   	 => 'tag.create',
		'getEdit'     	 => 'tag.edit',
		'postStore'   	 => 'tag.store',
		'postUpdate'  	 => 'tag.update',
		'postDestroy' 	 => 'tag.destroy',
		'postTrash'   	 => 'tag.trash',
		'postDatatables' => 'tag.datatables',
		'getError' 	  	 => 'tag.error',
		'getSelect2' 	 => 'tag.select2',

	]);

	Route::controller('category', 'CategoryController', [
		'getIndex'    	 => 'category.index',
		'getCreate'   	 => 'category.create',
		'getEdit'     	 => 'category.edit',
		'postStore'   	 => 'category.store',
		'postUpdate'  	 => 'category.update',
		'postDestroy' 	 => 'category.destroy',
		'postTrash'   	 => 'category.trash',
		'postDatatables' => 'category.datatables',
		'getError' 	  	 => 'category.error',
		'getSelect2' 	 => 'category.select2',
	]);

});


Route::group(['prefix' => 'library'  ,'namespace' => 'Modules\Post\Http\Controllers\Frontend'], function()
{
	Route::controller('post', 'PostController', [
//		'getIndex'    	 	 => 'post.index',
		'getDetail'     	 => 'post.detail',
		'getTag'     	     => 'post.tag',
		'getCategory'     	 => 'post.category',
		'getSearch' 	 	 => 'post.search',
	]);

//	Route::controller('author', 'AuthorController', [
//		'getIndex'    	 => 'author.index',
//		'getEdit'     	 => 'author.edit',
//		'postUpdate'  	 => 'author.update',
//		'postDestroy' 	 => 'author.destroy',
//		'postTrash'   	 => 'author.trash',
//		'getError' 	  	 => 'author.error',
//	]);

});
