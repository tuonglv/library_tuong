<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 24/06/2016
 * Time: 15:38
 */

namespace Modules\Post\Models;


use Modules\Core\Models\EloquentModel;
use Modules\Post\Entities\Taxonomy;
use Modules\Post\Entities\TaxonomyDetail;
use Modules\Post\Repositories\TaxonomyRepository;

class EloquentTaxonomy extends EloquentModel implements TaxonomyRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    function model()
    {
        return Taxonomy::class;
    }

    public function create($attributes)
    {
        \DB::beginTransaction();
        try {
            $taxonomy = Taxonomy::create($attributes['attributes']);

            foreach ($attributes['translate'] as $translate) {
                $translate['taxonomy_id'] = $taxonomy->id;
                TaxonomyDetail::create($translate);
            }

            \DB::commit();
            \Log::info("Tag $taxonomy->id has been created ");

            return $taxonomy;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);
            return false;
        }
    }

    public function update($id, $attributes)
    {
        \DB::beginTransaction();
        try {
            $taxonomy = Taxonomy::find($id);
            $taxonomy->fill($attributes['attributes'])->save();
            foreach ($attributes['translate'] as $translate) {
                $taxonomy_detail = TaxonomyDetail::select('*')->where('taxonomy_id' , '=' , $id)->where('locale_id','=',$translate['locale_id'])->first();
                if($taxonomy_detail && $taxonomy_detail != null){
                    $data = [
                        'name' => $translate['name'],
                        'description' => $translate['description'],
                    ];
                    $taxonomy_detail->fill($data)->save();
                }else{
                    $data = [
                        'name'        => $translate['name'],
                        'description' => $translate['description'],
                        'locale_id'   => $translate['locale_id'],
                        'taxonomy_id' => $id,
                    ];
                    $create = TaxonomyDetail::create($data);
                }
            }


            \DB::commit();
            \Log::info("User $taxonomy->id has been created ");

            return $taxonomy;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);
            return false;
        }

    }

    public function getAll($type = 'post_category' , $columns = ['taxonomies.*'])
    {
        return $this->model->select($columns)
            ->join('taxonomy_detail', 'taxonomy_detail.taxonomy_id', '=', 'taxonomies.id')
            ->where('taxonomies.type', '=', $type)->groupBy('taxonomies.id');
    }

    public function paginateCategoriesByType($type = 'post_category', $limit = 10, $columns = [ '*' ])
    {
        return Taxonomy::where('type', '=', $type)->paginate($limit);
    }
}