<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 28/06/2016
 * Time: 11:42
 */

namespace Modules\Post\Models;



use Guzzle\Http\Url;
use Modules\Core\Models\EloquentModel;
use Modules\Post\Entities\File;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\PostDetail;
use Modules\Post\Entities\PostTaxonomy;
use Modules\Post\Entities\TaxonomyDetail;
use Modules\Post\Repositories\PostRepository;

class EloquentPost extends EloquentModel implements PostRepository
{
    public function __construct()
    {
        parent::__construct();
    }

    function model()
    {
        return Post::class;
    }

    public function create($attributes)
    {
        \DB::beginTransaction();
        try {
            $post = Post::create($attributes['attributes']);
            foreach($attributes['translate'] as $translate){
                $translate['post_id'] = $post->id;
                PostDetail::create($translate);
            }
            if(isset($attributes['categories']) && $attributes['categories']){
                foreach($attributes['categories'] as $category_id){
                    $post_taxonomy = [
                        'post_id'       => $post->id,
                        'taxonomy_id'   => $category_id,
                    ];
                    PostTaxonomy::create($post_taxonomy);
                }
            }
            if(isset($attributes['tags']) && $attributes['tags']){
                foreach($attributes['tags'] as $tag_id){
                    $post_taxonomy = [
                        'post_id'       => $post->id,
                        'taxonomy_id'   => $tag_id,
                    ];
                    PostTaxonomy::create($post_taxonomy);
                }
            }
            $post_details = PostDetail::where('post_id', '=' ,$post->id)->get()->toArray();
            if($attributes['image'] && $attributes['image'] != null){
                foreach($post_details as $post_detail){
                    $attributes_image = [
                        'url'   => $attributes['image'],
                        'path'  => $post_detail['slug'],
                    ];
                    File::create($attributes_image);
                }
            }

            \DB::commit();
            \Log::info("Tag $post->id has been created ");

            return $post;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);
            return false;
        }
    }

    public function getAll($columns = ['posts.*']){
        return $this->model->select($columns)->join('post_detail', 'posts.id', '=', 'post_detail.post_id')->get();
    }

    public function update($id,$attributes){
        \DB::beginTransaction();
        try {
            $post = Post::find($id);
            $post->fill($attributes['attributes'])->save();
            $post_details = PostDetail::select('*')->where('post_id' , '=' , $id)->get();
            foreach ($attributes['translate'] as $translate) {
                foreach($post_details as $post_detail){
                    if($post_detail->locale_id == $translate['locale_id']){
                        $data = [
                            'title' => $translate['title'],
                            'excerpt' => $translate['excerpt'],
                            'content' => $translate['content'],
                        ];
                        $post_detail->fill($data)->save();
                    }
                }
            }

            if($attributes['categories']){
                $post_taxonomies = PostTaxonomy::select('post_taxonomy.*')->join('taxonomies','post_taxonomy.taxonomy_id','=','taxonomies.id')
                    ->where('taxonomies.type', '=', 'post_category')
                    ->where('post_taxonomy.post_id', '=', $id)->get();
                if($post_taxonomies->toArray()){
                    PostTaxonomy::destroy(array_pluck($post_taxonomies->toArray(), 'id'));
                }
                foreach($attributes['categories'] as $category){
                    $post_taxonomy_attr = [
                        'post_id'     => $id,
                        'taxonomy_id' => $category,
                    ];
                    PostTaxonomy::create($post_taxonomy_attr);
                }
            }

            if($attributes['tags']){
                $post_taxonomies = PostTaxonomy::select('post_taxonomy.*')->join('taxonomies','post_taxonomy.taxonomy_id','=','taxonomies.id')
                    ->where('taxonomies.type', '=', 'post_tag')
                    ->where('post_taxonomy.post_id', '=', $id)->get();
                if($post_taxonomies->toArray()){
                    PostTaxonomy::destroy(array_pluck($post_taxonomies->toArray(), 'id'));
                }
                foreach($attributes['tags'] as $tag){
                    $post_taxonomy_attr = [
                        'post_id'     => $id,
                        'taxonomy_id' => $tag,
                    ];
                    PostTaxonomy::create($post_taxonomy_attr);
                }
            }

            $post_details = PostDetail::where('post_id', '=' ,$id)->get()->toArray();
            if($attributes['image'] && $attributes['image'] != null){
                foreach($post_details as $post_detail){
//                    if (!preg_match("/192.168.0.138/", $attributes['image'])) {
//                        $attributes['image'] = url().$attributes['image'];
//                    }
                    $attributes_image = [
                        'url' => $attributes['image'],
                        'path'  => $post_detail['slug'],
                    ];
                    $file = File::where('path', '=' ,$post_detail['slug'])->first();

                    if($file && $file->toArray() != null){
                        $file->fill($attributes_image)->save();
                    }else{
                        File::create($attributes_image);
                    }
                }
            }

            \DB::commit();
            \Log::info("User $post->id has been created ");

            return $post;
        } catch (\Exception $e) {
            \DB::rollback();
            \Log::error($e);
            return false;
        }
    }

    public function getTaxonomiesByType($post_id = '0' , $type = 'post_category', $columns = ['taxonomy_detail.name' , 'taxonomy_detail.taxonomy_id'])
    {
        return TaxonomyDetail::select($columns)
            ->join('taxonomies', 'taxonomy_detail.taxonomy_id', '=', 'taxonomies.id')
            ->join('post_taxonomy', 'post_taxonomy.taxonomy_id', '=', 'taxonomies.id')
            ->where('taxonomies.type', '=', $type)
            ->where('post_taxonomy.post_id', '=', $post_id)->groupBy('taxonomies.id')->get();
    }



}