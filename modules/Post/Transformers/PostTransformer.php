<?php
/**
 * Created by ngocnh.
 * Date: 8/1/15
 * Time: 3:09 PM
 */

namespace Modules\Post\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Locale;
use Modules\Core\Transformers\UserTransformer;
use Modules\Post\Entities\Post;

class PostTransformer extends TransformerAbstract
{

    public function __construct($translate = false)
    {
        $this->translate = $translate;
    }


    public function transform(Post $post)
    {
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer);

        $translates = [];

        if ($this->translate) {
            $locale = new Locale();
            foreach ($locale->all() as $locale) {
                $translates[$locale->code] = [
                    'title'     => $post->translate($locale->code)->title,
                    'excerpt'   => $post->translate($locale->code)->excerpt,
                    'content'   => $post->translate($locale->code)->content,
                    'locale_id' => $locale->id
                ];
            }
        }
        $tags = $post->taxonomies($post->id , 'post_tag');
        $taxonomies = $post->taxonomies($post->id , 'post_category');
        $image['url'] = $post->images($post->slug)->first();
        if($image['url']){
            $image['url'] = $image['url']->toArray()['url'];
        }

        $return = [
            'id'                => $post->id,
            'parent'            => $post->parent,
            'slug'              => $post->slug,
            'order'             => $post->order,
            'title'             => $post->title,
            'excerpt'           => $post->excerpt,
            'content'           => $post->content,
            'type'              => $post->type,
            'status'            => $post->status,
            'comment_status'    => $post->comment_status,
            'comment_count'     => $post->comment_count,
            'updated_at'        => $post->updated_at,
            'created_at'        => $post->created_at,
            'translate'         => $translates ?: false ,
            'taxonomies'        => $taxonomies,
            'tags'              => $tags,
            'image'             => $image,
        ];

        if ($author = $post->author()) {
            $author           = new Item($author, new UserTransformer(), 'user');
            $author           = $manager->createData($author)->toArray();
            $return['author'] = $author;
            $return['author']['name'] = isset($author['meta']['nickname'])  ? $author['meta']['nickname']  : (isset($author['username']) ? $author['username'] : '');
        }

//        if ($categories = $categories->files('featured')->get()) {
//            $featured = new Item($featured, new FileTransformer);
//            $return['featured'] = $manager->createData($featured)->toArray();
//        }

//        if ($images = $taxonomy->files('images')->get()) {
//            $images = new Collection($images, new FileTransformer, 'images');
//            $images = $manager->createData($images)->toArray();
//            $return = array_merge($return, $images);
//        }

//        if ($meta = $taxonomy->meta()->get()) {
//            foreach ($meta as $item) {
//                $return['meta'][$item->key] = $item->value;
//            }
            //$meta   = new Collection($meta, new TaxonomyMetaTransformer(), 'meta');
            //$meta   = $manager->createData($meta)->toArray();
            //$return = array_merge($return, $meta);
//        }

        return $return;
    }
}