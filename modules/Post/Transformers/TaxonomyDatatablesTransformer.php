<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 27/06/2016
 * Time: 11:26
 */

namespace Modules\Post\Transformers;


use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;
use Modules\Post\Entities\Taxonomy;

class TaxonomyDatatablesTransformer extends TransformerAbstract
{
    public function __construct($type = 'category')
    {
        $this->type = $type;
    }

    public function transform(Taxonomy $taxonomy){

        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $return = [
            'id'                => $taxonomy->id,
            'name'              => $taxonomy->name ,
            'description'       => $taxonomy->description,
            'order'             => $taxonomy->order,
            'count'             => $taxonomy->count,
        ];

        if ($this->type == 'tag') {
            $return['action'] = \Form::open(['url' => route('tag.trash', ['id' => $taxonomy->id]), 'class' => 'form-horizontal']);
            $return['action'] .= "<a href='".route('tag.edit', ['id' => $taxonomy->id])."' class='btn btn-sm btn-default' rel='tooltip' title='".trans('tag::user.edit')."'><i class='fa fa-pencil'></i></a>";
            $return['action'] .= \Form::button("<i class='fa fa-trash-o'></i>", [ 'type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'title' => trans('tag::user.trash') ]);
            $return['action'] .= \Form::close();
        }else {
            $return['action'] = \Form::open(['url' => route('category.trash', ['id' => $taxonomy->id]), 'class' => 'form-horizontal']);
            $return['action'] .= "<a href='" . route('category.edit', ['id' => $taxonomy->id]) . "' class='btn btn-sm btn-default' rel='tooltip' title='" . trans('category::user.edit') . "'><i class='fa fa-pencil'></i></a>";
            $return['action'] .= \Form::button("<i class='fa fa-trash-o'></i>", ['type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'title' => trans('category::user.trash')]);
            $return['action'] .= \Form::close();
        }
        return $return;
    }

}