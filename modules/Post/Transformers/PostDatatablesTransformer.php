<?php
/**
 * Created by PhpStorm.
 * post: tuonglv
 * Date: 24/06/2016
 * Time: 14:40
 */

namespace Modules\Post\Transformers;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;
use Modules\Core\Entities\Locale;
use Modules\Core\Transformers\UserTransformer;
use Modules\Post\Entities\Post;

class PostDatatablesTransformer extends TransformerAbstract
{
    public function transform(Post $post)
    {
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());
        $taxonomies = $post->taxonomies($post->id, 'post_category');
        if($taxonomies && $taxonomies->toArray()){
            $taxonomies = array_pluck($taxonomies->toArray(),'name');
            $taxonomies = implode('</br>', $taxonomies);
        }
        $return = [
            'parent'            => $post->parent,
            'order'             => $post->order,
            'title'             => $post->title,
            'excerpt'           => $post->excerpt,
            'content'           => $post->content,
            'type'              => $post->type,
            'status'            => $post->status,
            'taxonomies'        => $taxonomies,
            'comment_status'    => $post->comment_status,
            'comment_count'     => $post->comment_count,
        ];
        if ($author = $post->author()) {
            $author           = new Item($author, new UserTransformer(), 'user');
            $author           = $manager->createData($author)->toArray();
            $return['author'] = isset($author['meta']['nickname'])  ? $author['meta']['nickname']  : (isset($author['username']) ? $author['username'] : '');
        }

        $return['action'] = \Form::open(['url' => route('post.trash', ['id' => $post->id]), 'class' => 'form-horizontal']);
        $return['action'] .= "<a href='".route('post.edit', ['id' => $post->id])."' class='btn btn-sm btn-default' rel='tooltip' title='".trans('post::post.edit')."'><i class='fa fa-pencil'></i></a>";
        $return['action'] .= \Form::button("<i class='fa fa-trash-o'></i>", [ 'type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'title' => trans('post::post.trash') ]);
        $return['action'] .= \Form::close();

        return $return;
    }
}