<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 28/06/2016
 * Time: 11:57
 */

namespace Modules\Post\Entities;


use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class PostDetail extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    protected $table = "post_detail";

    protected $fillable = [ 'title', 'excerpt', 'content', 'slug' , 'locale_id', 'post_id' ];

    protected $dates = [ 'created_at', 'updated_at'];

    public $timestamps = true;

    public function post()
    {
        return $this->belongsTo(Post::class)->first();
    }
}