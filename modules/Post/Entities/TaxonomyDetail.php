<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 24/06/2016
 * Time: 15:22
 */

namespace Modules\Post\Entities;


use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class TaxonomyDetail extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];

    protected $table = "taxonomy_detail";

    protected $fillable = [ 'name', 'slug', 'description', 'locale_id', 'taxonomy_id' ];

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    public $timestamps = true;


    public function taxonomy()
    {
        return $this->belongsTo(Taxonomy::class)->first();
    }
}