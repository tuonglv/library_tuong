<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 28/06/2016
 * Time: 10:33
 */

namespace Modules\Post\Entities;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Core\Entities\User;
use Ngocnh\Translator\Translatable;
use Ngocnh\Translator\Contracts\Translatable as TranslatableContract;

class Post extends Model implements TranslatableContract
{
    use Translatable , SoftDeletes;

    protected $table = "posts";

    protected $fillable = [
        'author',
        'parent',
        'order',
        'type',
        'status',
        'comment_status',
        'comment_count'
    ];

    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    protected $translator = PostDetail::class;

    protected $translatedAttributes = [ 'title', 'excerpt', 'content','slug' ];

    protected $translatableForeign = 'post_id';

    public $timestamps = true;

    public function author(){
        return $this->belongsTo(User::class, 'author', 'id')->first();
    }

    public function post_taxonomy()
    {
        return $this->hasMany(PostTaxonomy::class, 'post_id', 'id');
    }

    public function taxonomies($post_id, $type = 'post_category', $columns = ['taxonomies.*']){
        return Taxonomy::select($columns)
            ->join('post_taxonomy', 'post_taxonomy.taxonomy_id', '=', 'taxonomies.id')
            ->join('posts', 'post_taxonomy.post_id', '=', 'posts.id')
            ->where('taxonomies.type', '=', $type)->where('posts.id' , '=' , $post_id)->get();
    }
    public function images($path , $columns = ['files.*']){
        return File::select($columns)->where('files.path', '=' , $path)->get();
    }
}