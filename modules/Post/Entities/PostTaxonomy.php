<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 28/06/2016
 * Time: 11:57
 */

namespace Modules\Post\Entities;


use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class PostTaxonomy extends Model
{
    protected $table = "post_taxonomy";

    protected $fillable = [ 'post_id', 'taxonomy_id'];

    protected $dates = [ 'created_at', 'updated_at'];

    public $timestamps = true;

    public function post()
    {
        return $this->belongsTo(Post::class)->first();
    }

    public function taxonomy()
    {
        return $this->belongsTo(Taxonomy::class)->first();
    }
}