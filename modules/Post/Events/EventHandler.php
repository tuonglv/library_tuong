<?php

namespace Modules\Post\Events;

class EventHandler
{

    public function subscribe($events)
    {
        // Get Popular Posts
        $events->listen('home.before.render', 'Modules\Post\Http\Controllers\Frontend\PostController@eventPopular');
        $events->listen('frontend.post_tops.before.render', 'Modules\Post\Http\Controllers\Frontend\PostController@eventPostTops');
        $events->listen('frontend.post_taxonomies.before.render', 'Modules\Post\Http\Controllers\Frontend\PostController@eventTaxonomies');
    }
}