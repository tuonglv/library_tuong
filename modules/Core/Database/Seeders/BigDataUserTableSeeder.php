<?php namespace Modules\Core\Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\User;

class BigDataUserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = Factory::create();

        for ($i = 0; $i < 1000000; $i++) {
            $username = "{$faker->userName}-{$i}";
            $email    = "{$username}@{$faker->domainName}";

            User::create([
                'username'  => $username,
                'password'  => bcrypt('123456'),
                'email'     => $email,
                'type'      => 'default',
                'status'    => 1,
                'locale_id' => 1
            ]);
        }
    }

}