<?php
$api = app('api.router');

Route::group([ 'prefix' => 'library/admin', 'namespace' => 'Modules\Core\Http\Controllers\Backend' ], function () {
    get('login', [ 'as' => 'admin.login', 'uses' => 'AuthController@adminLogin' ]);
    post('login', [ 'as' => 'admin.login', 'uses' => 'AuthController@adminDoLogin' ]);
    get('logout', [ 'as' => 'admin.logout', 'uses' => 'AuthController@adminLogout' ]);

    Route::controller('user', 'UserController', [
        'getIndex'        => 'user.index',
        'getCreate'       => 'user.create',
        'getEdit'         => 'user.edit',
        'getLock'         => 'user.lock',
        'getProfile'      => 'user.profile',
        'postStore'       => 'user.store',
        'postUpdate'      => 'user.update',
        'postUpdateProfile'      => 'user.update_profile',
        'postApplication' => 'user.application',
        'postTrash'       => 'user.trash',
        'postDestroy'     => 'user.destroy',
        'postDatatables'  => 'user.datatables',
    ]);

    Route::controller('role', 'RoleController', [
        'getIndex'    => 'role.index',
        'getCreate'   => 'role.create',
        'getEdit'     => 'role.edit',
        'postStore'   => 'role.store',
        'postUpdate'  => 'role.update',
        'postDestroy' => 'role.destroy'
    ]);

    get('/', [ 'as' => 'admin.dashboard', 'uses' => 'AdminController@index' ]);
    get('/locale/{code}', [ 'as' => 'locale', 'uses' => 'AdminController@locale' ]);

    Route::group([ 'prefix' => 'configuration' ], function () {
        post('save', [ 'as' => 'configuration.save', 'uses' => 'ConfigController@save' ]);

        Route::controller('setting', 'SettingController', [
            'getIndex' => 'configuration.setting.index',
            'getMail'  => 'configuration.mail.index'
        ]);
    });
});

Route::group([ 'namespace' => 'Modules\Core\Http\Controllers\Frontend' ], function () {
    get('/', ['as' => 'home', 'uses' => 'LandingPageController@getIndex']);
});
// Landing Page

Route::group(['prefix' => 'library', 'namespace' => 'Modules\Core\Http\Controllers\Frontend' ], function () {
    get('/', ['as' => 'home', 'uses' => 'HomeController@getIndex']);
    get('/about-me', ['as' => 'about-me', 'uses' => 'HomeController@getAboutMe']);
    get('/contact', ['as' => 'contact-me', 'uses' => 'HomeController@getContactMe']);
    get('/TOS', ['as' => 'contact-me', 'uses' => 'HomeController@getRules']);
    get('/author/{any}', ['as' => 'author', 'uses' => 'HomeController@getAuthor']);
    get('login', [ 'as' => 'login', 'uses' => 'AuthController@login' ]);
    post('login', [ 'as' => 'login', 'uses' => 'AuthController@do_login' ]);
    get('logout', [ 'as' => 'logout', 'uses' => 'AuthController@logout' ]);
    get('register', [ 'as' => 'register', 'uses' => 'AuthController@register' ]);
    post('register', [ 'as' => 'register', 'uses' => 'AuthController@do_register' ]);
    get('forget-password', [ 'as' => 'forget_password', 'uses' => 'AuthController@forget_password' ]);
    get('/fr_logout', [ 'as' => 'fr_logout', 'uses' => 'HomeController@logout' ]);
});

$api->version('v1', [ 'namespace' => 'Modules\Core\Http\Controllers\API\v1' ], function ($api) {
    $api->post('user/login', 'User@login');
    $api->post('contact', 'General@contact');
    $api->get('setting', [ 'middleware' => 'api.auth', 'providers' => [ 'oauth' ], 'uses' => 'Setting@index' ]);
    $api->resource('user', 'User');
});