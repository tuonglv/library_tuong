<?php
/**
 * Created by PhpStorm.
 * User: tuonglv
 * Date: 11/07/2016
 * Time: 15:32
 */

namespace Modules\Core\Http\Controllers\Frontend;


use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use Modules\Core\Http\Controllers\CoreController;

class LandingPageController extends CoreController
{
    public function __construct(Manager $manager)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->manager->setSerializer(new ArraySerializer());
        $this->theme = theme( 'frontend3', 'default1');
    }

    public function getIndex(){
        return $this->theme->of('core::frontend.landing_page')->render();
    }
}