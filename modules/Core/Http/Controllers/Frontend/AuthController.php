<?php
/**
 * Created by PhpStorm.
 * User: kienzo
 * Date: 6/21/16
 * Time: 1:56 PM
 */

namespace Modules\Core\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Modules\Core\Http\Controllers\CoreController;
use Modules\Core\Http\Requests\frontend\UserStoreRequest;
use Modules\Core\Http\Requests\LoginRequest;
use Modules\Core\Repositories\RoleRepository;
use Modules\Core\Repositories\UserRepository;
use Modules\Core\Transformers\UserTransformer;

class AuthController extends CoreController
{
    public function __construct(Manager $manager, UserRepository $user , RoleRepository $role)
    {
        parent::__construct($manager);
        $this->user   = $user;
        $this->role   = $role;
    }

    public function login()
    {
        return view('core::frontend.login');
    }

    public function do_login(LoginRequest $request)
    {
        $email = $request->email;
        $password = $request->password;

        if (auth()->attempt(['email' => $email, 'password' => $password, 'status' => 1]))
        {
            $user = auth()->user();
            $user->last_visited = new \DateTime();
            $user->save();
            $image = $user->files('featured')->first();
            $nickname = $user->meta_key('nickname') ? $user->meta_key('nickname')->meta_value : ($user->meta_key('first_name') && $user->meta_key('last_name') ? $user->meta_key('first_name')->meta_value . ' ' . $user->meta_key('last_name')->meta_value : "");


            session()->put('user', [
                'id' => $user->id,
                'email' => $user->email,
                'name' => $nickname,
                'avatar' => $image ? ($image->url ?: url($image->path)) : '',
                'logged_in' => true
            ]);

            event('user.login', [$user]);

            if ($request->ajax()) {
                $manager = new Manager();
                $user = new Item(auth()->user(), new UserTransformer());

                return response()->json($manager->createData($user)->toArray(), 200);
            }

            return redirect()->route('admin.dashboard')->withCookie(cookie()->forever('user', [
                'CandyUsername' => $user->email,
                'CandyName' => $user->meta_key('nickname') ? $user->meta_key('nickname')->meta_value : ($user->meta_key('first_name') ? $user->meta_key('first_name') . ' ' : '') . ($user->meta_key('last_name') ? $user->meta_key('last_name')->meta_value : ''),
                'CandyAvatar' => $image ? ($image->url ?: url($image->path)) : ''
            ]));

        } else {
            if ($request->ajax()) {
                return response()->json(['error' => trans('core::login.login_failed')], 422);
            }

            flash()->error(trans('core::login.login_failed'));

            return redirect()->back()->withInput();
        }
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('login');
    }

    public function register()
    {
        return view('core::frontend.register');
    }

    public function do_register(UserStoreRequest $request)
    {
        $attributes = [
            'attributes' => [
                'email' => $request->email,
                'password' => $request->password,
                'activation_key' => $request->status ? null : str_random(24),
                'type' => $request->type,
                'status' => 1,
            ],
            'roles' => [2],
            'image' => $request->image ?: false
        ];

        if ($request->meta) {
            foreach ($request->meta as $key => $value) {
                $attributes['meta'][$key] = $value;
            }
        }

        if ($request->username) {
            $attributes['attributes']['username'] = $request->username;
        }

        if ($response = event('user.before.store', [$attributes])) {
            $attributes = $response[0];
        }

        if ($user = $this->user->create($attributes)) {
            event('user.after.store', [$user]);
            flash(trans('core::user.create_success', ['name' => $user->username ?: $user->email]));
            return redirect()->route('login');
        } else {
            flash()->error(trans('core::general.error'));
            return redirect()->back()->withInput();
        }
    }
}