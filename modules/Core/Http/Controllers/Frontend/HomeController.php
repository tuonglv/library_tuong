<?php
/**
 * Created by PhpStorm.
 * User: kienzo
 * Date: 6/21/16
 * Time: 10:24 AM
 */

namespace Modules\Core\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Modules\Core\Entities\User;
use Modules\Core\Transformers\UserTransformer;
use Modules\Post\Entities\Post;
use Modules\Post\Transformers\PostTransformer;

class HomeController extends FrontendController
{
    private $data;

    public function __construct(Manager $manager)
    {
        parent::__construct($manager);
    }

    public function getIndex()
    {
        $this->data = [];
        if ($response = event('home.before.render', [$this->data])) {
            $this->data = $response[0];
        }
        $this->data['sidebar_status'] = true;
        View::share($this->data);
        return $this->theme->of('core::frontend.default2')->render();
    }

    public function getAboutMe()
    {
        return $this->theme->of('core::frontend.about_me')->render();
    }

    public function getRules()
    {
        return $this->theme->of('core::frontend.rules')->render();
    }

    public function getContactMe()
    {
        $data['search_form_status'] = false;
        View::share($data);
        return $this->theme->of('core::frontend.contact_me')->render();
    }

    public function getAuthor($id)
    {
        $user         = User::find($id);
        $user         = new Item($user, new UserTransformer(), 'user');
        $user         = $this->manager->createData($user)->toArray();
        $data['user'] = $user;
        $data['search_form_status'] = false;

        $author_posts = Post::select('*')
            ->where('posts.status', '=' , 'publish')
            ->where('posts.author','=',$id)
            ->orderby('created_at','desc')
            ->paginate(5);
        $collection = new Collection($author_posts->getCollection(), new PostTransformer, 'author_posts');
        $collection->setPaginator(new IlluminatePaginatorAdapter($author_posts));
        $data['author_posts'] = $this->manager->createData($collection)->toArray();
        View::share($data);
        return $this->theme->of('core::frontend.author')->render();
    }

    public function logout(){
        auth()->logout();
        return redirect('/');
    }

}