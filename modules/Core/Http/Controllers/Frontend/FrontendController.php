<?php
/**
 * Created by PhpStorm.
 * User: kienzo
 * Date: 6/20/16
 * Time: 3:17 PM
 */

namespace Modules\Core\Http\Controllers\Frontend;

use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use Modules\Core\Http\Controllers\CoreController;

class FrontendController extends CoreController
{

    public function __construct(Manager $manager)
    {
        parent::__construct();
        event('frontend.before.load');
        $this->manager = $manager;
        $this->manager->setSerializer(new ArraySerializer);
        $this->theme = theme( 'frontend2', 'default');
        event('frontend.after.load');
    }
}