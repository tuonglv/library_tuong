<?php
/**
 * Created by ngocnh.
 * Date: 8/6/15
 * Time: 12:01 AM
 */

namespace Modules\Core\Http\Requests;

class RoleListRequest extends BaseRequest
{

    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'core.role.index' ]);
    }


    public function rules()
    {
        return [];
    }
}