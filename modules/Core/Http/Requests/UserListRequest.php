<?php
/**
 * Created by ngocnh.
 * Date: 8/6/15
 * Time: 12:01 AM
 */

namespace Modules\Core\Http\Requests;

class UserListRequest extends BaseRequest
{

    public function authorize()
    {
        return auth()->user()->can([ 'access.all', 'core.user.index3' ]);
    }


    public function rules()
    {
        return [];
    }
}