<div id="main" class="regular">
    @if(isset($post_populars) && $post_populars )
        @foreach($post_populars['post_populars'] as $post_popular)
            <article id="post-6"
                     class="list-item post-6 post type-post status-publish format-standard has-post-thumbnail hentry category-lifestyle">

                <div class="post-img">
                    <a href="{!! isset($post_popular['slug']) ? 'post/detail/'.$post_popular['slug'] : '' !!}"><img
                                width="500" height="380"
                                src="{!! isset($post_popular['image']['url']) ? $post_popular['image']['url'] : '/files/images/no-image.png' !!}"
                                alt="{!! isset($post_popular['title']) ? str_limit($post_popular['excerpt'] ,170) : '' !!}"
                                class="attachment-misc-thumb size-misc-thumb wp-post-image" alt="flower-girl"></a>
                </div>

                <div class="list-content">
                    <div class="post-header">
                            <div class="post_title">
                                <a href="{!! isset($post_popular['slug']) ? 'post/detail/'.$post_popular['slug'] : '' !!}">{!! isset($post_popular['title']) ? str_limit(mb_strtoupper($post_popular['title'],"UTF-8") ,105) : '' !!}</a>
                            </div>
                    </div>

                    <div class="post-entry">

                        <p>{!! isset($post_popular['excerpt']) ? str_limit($post_popular['excerpt'] ,170) : '' !!}</p>

                    </div>

                    @if(isset($post_popular['taxonomies']) && $post_popular['taxonomies'])
                        @foreach($post_popular['taxonomies'] as $category)
                            <span class="cat"><a
                                        href="{!! isset($category['id']) ? URL::to('/library/post/category\/').$category['slug'] : '#' !!}"
                                        rel="category tag"> {!! isset($category) ? $category['name'] : '' !!}</a>&nbsp;&nbsp;</span>
                        @endforeach
                    @endif

                    <div class="post-meta">
                        <span class="meta-info">
                                 {!! isset($post_popular['updated_at']) ? $post_popular['updated_at'] : '' !!}
                            by <a href="{!! isset($post_popular['author']['id']) && $post_popular['author']['id'] ? URL::to('library/author\/').$post_popular['author']['id'] : '#'  !!}"
                                  title="Posts by {!! isset($post_popular['author']['name']) ? $post_popular['author']['name'] : '' !!}"
                                  rel="author">{!! isset($post_popular['author']['name']) ? $post_popular['author']['name'] : '' !!}</a>
                        </span>
                    </div>
                </div>

            </article>
        @endforeach
            @if($paginator->hasPages())
                <div class="front_pagination">
                    <div class="col-md-12">
                        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                            {!! theme()->partial('paginator', ['paginator' => $paginator]) !!}
                        </div>
                    </div>

                </div>
            @endif

    @else
        No Result !!
    @endif
</div>

{{--sidebar--}}

        <!-- END CONTENT -->