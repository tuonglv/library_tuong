<div id="main" class="regular">
    <div id="main" class="regular">
        <div>
            <h1 class="find_with">Find with Search: {!! isset($_GET['title']) ? $_GET['title'] : '' !!} </h1>
        </div>
        @if(isset($post_searchs) && $post_searchs )
            @foreach($post_searchs as $post_search)
                <article id="post-6"
                         class="post-6 post type-post status-publish format-standard has-post-thumbnail hentry category-lifestyle">
                    <div class="post-img">
                        <a href="{!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'] : '' !!}"><img
                                    width="740" height="493"
                                    src="{!! isset($post_search['image']['url']) ? $post_search['image']['url'] : '' !!}"
                                    class="attachment-full-thumb size-full-thumb wp-post-image"
                                    alt="{!! isset($post_search['title']) ? $post_search['title'] : '' !!}"
                                    sizes="(max-width: 740px) 100vw, 740px"></a>
                    </div>

                    <div class="post-header">
                        @if(isset($post_search['taxonomies']) && $post_search['taxonomies'])
                            @foreach($post_search['taxonomies'] as $category)
                                <span class="cat"><a
                                            href="{!! isset($category['id']) ? URL::to('post/category/').'/'.$category['id'] : '#' !!}"
                                            rel="category tag"> {!! isset($category) ? $category['name'] : '' !!}</a>&nbsp;&nbsp;</span>
                                {{--{!! dd($post_search['taxonomies']->first()->toArray()['id']) !!}--}}
                            @endforeach
                        @endif
                        <h2>
                            <a href="{!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'] : '' !!}">{!! isset($post_search['title']) ? $post_search['title'] : '' !!}</a>
                        </h2>

                    </div>

                    <div class="post-entry">

                        <p>{!! isset($post_search['excerpt']) ? str_limit($post_search['excerpt'] ,677) : '' !!}</p>
                        <p>
                            <a href="{!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'] : '' !!}"
                               class="more-link"><span
                                        class="more-button">Continue Reading</span></a></p>


                    </div>

                    <div class="post-meta">

                <span class="meta-info">
                     {!! isset($post_search['updated_at']) ? $post_search['updated_at'] : '' !!}
                    by <a href="about-me"
                          title="Posts by {!! isset($post_detail['author']['name']) ? $post_detail['author']['name'] : '' !!}"
                          rel="author">{!! isset($post_detail['author']['name']) ? $post_detail['author']['name'] : '' !!}</a>
                </span>

                        <div class="post-share">

                            <a target="_blank"
                               href="https://www.facebook.com/sharer/sharer.php?u={!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'].'/' : '' !!}"><i
                                        class="fa fa-facebook"></i></a>
                            <a target="_blank"
                               href="https://twitter.com/home?status=Check%20out%20this%20article:%20Floral Wallpaper%20-%20{!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'] : '' !!}"><i
                                        class="fa fa-twitter"></i></a>

                            <a target="_blank"
                               href="https://pinterest.com/pin/create/button/?url={!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'] : '' !!}&amp;media={!! isset($post_search['image']['url']) ? URL::to('/post/detail/').$post_search['image']['url'] : '' !!}&amp;description={!! isset($post_search['title']) ? $post_search['title'] : '' !!}"><i
                                        class="fa fa-pinterest"></i></a>
                            <a target="_blank"
                               href="https://plus.google.com/share?url={!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'] : '' !!}"><i
                                        class="fa fa-google-plus"></i></a>
                            <a href="{!! isset($post_search['slug']) ? URL::to('/post/detail/').'/'.$post_search['slug'] : '' !!}"><i
                                        class="fa fa-comments"></i></a>
                        </div>

                    </div>


                </article>
            @endforeach

            <div class="pagination">

                <div class="older"><a href="#">Older Posts <i
                                class="fa fa-angle-double-right"></i></a></div>
                <div class="newer"></div>

            </div>

        @else
            No result find with search key!!!!
        @endif
    </div>