<?php
/**
 * Created by PhpStorm.
 * User: kienzo
 * Date: 6/21/16
 * Time: 10:22 AM
 */
?>

<div class="block-main-1-wrapper">
    <div class="container">
        <div class="block-main-1 news-masonry">
            <div class="block-item-wrapper item-width-2">
                <div class="block-item news-layout-3 big"><a href="index.html" class="label-topic-1"><span>news</span></a><a href="index.html#" class="news-image"><span class="mask-gradient style-4"></span><img src="themes/frontend/assets/img/news/block-main-1.jpg" alt="" class="img-responsive"></a>
                    <div
                            class="news-content"><a href="index.html#" class="title">GoFundMe campaign launched to help raise $53 milion for Kanye West</a>
                        <ul class="info">
                            <li><a href="index.html#" class="link">By timothy</a></li>
                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                            <li><a href="index.html#" class="link">15 likes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="block-item-wrapper item-width-1">
                <div class="block-item news-layout-3 business"><a href="business.html" class="label-topic-1"><span>business</span></a><a href="index.html#" class="news-image"><span class="mask-gradient style-2"></span><img src="themes/frontend/assets/img/news/block-main-2.jpg" alt="" class="img-responsive"></a>
                    <div
                            class="news-content"><a href="index.html#" class="title">Chennai Pharma Entrepreneur Braved Odds To Sell Meds In Latin America</a>
                        <ul class="info">
                            <li><a href="index.html#" class="link">By timothy</a></li>
                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                            <li><a href="index.html#" class="link">15 likes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="block-item-wrapper item-width-1">
                <div class="block-item news-layout-3 entertainment"><a href="entertainment.html" class="label-topic-1"><span>entertainment</span></a><a href="index.html#" class="news-image"><span class="mask-gradient style-3"></span><img src="themes/frontend/assets/img/news/block-main-3.jpg" alt="" class="img-responsive"></a>
                    <div
                            class="news-content"><a href="index.html#" class="title">The Only Thing In The Universe That Baffles Neil deGrasse Tyson</a>
                        <ul class="info">
                            <li><a href="index.html#" class="link">By timothy</a></li>
                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                            <li><a href="index.html#" class="link">15 likes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MAIN NEWS-->
<div class="main-news-wrapper padding-top-60 padding-bottom-60">
    <div class="container">
        <!-- SLIDE 1-->
        <div class="title-topic-2">editor's choice</div>
        <div class="slide-3-wrapper margin-bottom-60">
            <div class="item">
                <div class="news-layout-1"><a href="index.html" class="label-topic-1"><span>news</span></a>
                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/slide-1.jpg" alt="" class="img-responsive"></a>
                    <div class="news-content"><a href="index.html#" class="title">Gates: We need an 'energy miracle' to stop climate change</a>
                        <ul class="info">
                            <li><a href="index.html#" class="link">By timothy</a></li>
                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                            <li><a href="index.html#" class="link">15 likes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="news-layout-1 travel"><a href="travel.html" class="label-topic-1"><span>travel</span></a>
                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/slide-2.jpg" alt="" class="img-responsive"></a>
                    <div class="news-content"><a href="index.html#" class="title">The Réunion oliveis indigenous to the island of La Réunion</a>
                        <ul class="info">
                            <li><a href="index.html#" class="link">By timothy</a></li>
                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                            <li><a href="index.html#" class="link">15 likes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="news-layout-1 sport"><a href="sport.html" class="label-topic-1"><span>sport</span></a>
                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/slide-3.jpg" alt="" class="img-responsive"></a>
                    <div class="news-content"><a href="index.html#" class="title">'SNL' pokes fun at controversy over 'Formation'</a>
                        <ul class="info">
                            <li><a href="index.html#" class="link">By timothy</a></li>
                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                            <li><a href="index.html#" class="link">15 likes</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-xs-12 main-news main-left">
                <!-- BLOCK 1-->
                <div class="block-news-1 margin-bottom-60">
                    <div class="title-topic-2">popular news</div>
                    <div class="block-news-content slide">
                        <div class="slide-item">
                            <div class="layout-main-news">
                                <div class="news-layout-1 big">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-1.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="label-topic-2">news</a><a href="index.html#" class="title">US planning for new drone base in Northwest Africa</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="layout-list-news">
                                <div class="single-recent-post-widget">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-1.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">news</a><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a></div>
                                </div>
                                <div class="single-recent-post-widget business">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-2.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">business</a><a href="index.html#" class="title">Others Get a Breather as G.O.P. Front-Runners Attack One Another</a></div>
                                </div>
                                <div class="single-recent-post-widget travel">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-3.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">travel</a><a href="index.html#" class="title">In Transit: At Budget Hotels, Local Flavor Is Now Included</a></div>
                                </div>
                                <div class="single-recent-post-widget entertainment">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-4.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">entertainment</a><a href="index.html#" class="title">Well: Getting Pregnant After a Miscarriage</a></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slide-item">
                            <div class="layout-main-news">
                                <div class="news-layout-1 big">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-5.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="label-topic-2">news</a><a href="index.html#" class="title">Baking With Buffy: Sarah Michelle Gellar's Food Startup</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="layout-list-news">
                                <div class="single-recent-post-widget">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-5.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">news</a><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a></div>
                                </div>
                                <div class="single-recent-post-widget business">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-6.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">business</a><a href="index.html#" class="title">Health Benefits Options: Advancing An Employee-Centric</a></div>
                                </div>
                                <div class="single-recent-post-widget travel">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-7.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">travel</a><a href="index.html#" class="title">Corporate India's Financial Problems Worsen</a></div>
                                </div>
                                <div class="single-recent-post-widget entertainment">
                                    <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-1.jpg" alt="" class="img-wrapper"></a>
                                    <div class="post-info"><a href="index.html#" class="label-topic-2">entertainment</a><a href="index.html#" class="title">Heads Roll At BHP Billiton As Profit Flips To A Loss</a></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- BANNER ADV-->
                <div class="banner-adv-wrapper margin-bottom-60">
                    <a href="index.html#" class="banner-adv-728x90"><img src="themes/frontend/assets/img/banner-images/banner-728x90-2.jpg" alt="" class="img-responsive"></a>
                </div>
                <!-- BLOCK 5-->
                <div class="block-news-5 margin-bottom-60">
                    <div class="title-topic-2">hot topic</div>
                    <div class="block-news-content">
                        <div class="layout-main-news">
                            <div class="news-layout-3 big"><a href="index.html" class="label-topic-1"><span>news</span></a><a href="index.html#" class="news-image"><span class="mask-gradient style-4"></span><img src="themes/frontend/assets/img/news/block-main-4.jpg" alt="" class="img-responsive"></a>
                                <div
                                        class="news-content"><a href="index.html#" class="title">Facebook No Longer Just Has A 'Like' Button, Thanks To GL</a></div>
                            </div>
                        </div>
                        <div class="layout-list-news">
                            <div class="row-wrapper clearfix">
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-8.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="title">Baking With Buffy: Sarah Michelle Gellar's Food Startup</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">5 mins ago</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-9.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="title">Abortion clinics in the US are closing faster than ever — here's why</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">2 hours ago</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-10.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="title">A popular free movie streaming site is coming back from the dead</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">5 hours ago</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-11.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="title">These two ISIS battles could change everything in the Middle East</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">1 day ago</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- BANNER ADV-->
                <div class="banner-adv-wrapper margin-bottom-60">
                    <a href="index.html#" class="banner-adv-728x90"><img src="themes/frontend/assets/img/banner-images/banner-728x90-3.jpg" alt="" class="img-responsive"></a>
                </div>
                <!-- BLOCK 2-->
                <div class="block-news-2">
                    <div class="title-topic news">news</div>
                    <div class="block-news-content slide">
                        <div class="slide-item">
                            <div class="layout-main-news">
                                <div class="news-layout-1">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-2.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="title">The problems in the US economy may be more widespread.</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="layout-list-news">
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Health Benefits Options: Advancing An Employee-Centric Shopping Experience</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="slide-item">
                            <div class="layout-main-news">
                                <div class="news-layout-1">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-3.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="layout-list-news">
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Secrets in Paradise: The Best Caribbean Beaches You’ve Never Heard Of</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Baking With Buffy: Sarah Michelle Gellar's Food Startup</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Here's one Reason Ben Carson is Still Fighting for The GOP Nomination</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">Larsen, Lennarth Take 2nd-round Lead at Ladies Masters</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="single-recent-post-widget">
                                    <div class="post-info"><a href="index.html#" class="title">A popular free movie streaming site is coming back from the dead</a>
                                        <ul class="info">
                                            <li><a href="index.html#" class="link">By timothy</a></li>
                                            <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                            <li><a href="index.html#" class="link">15 likes</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 sidebar">
                <div class="sidebar-wrapper">
                    <div class="col-2">
                        <div class="socials-tab-widget widget">
                            <div class="title-topic-2">trending on socials</div>
                            <ul role="tablist" class="nav nav-tabs nav-justified socials-tab">
                                <li class="active">
                                    <a href="index.html#social-1" role="tab" data-toggle="tab" class="tab facebook"> <span>Facebook</span></a>
                                </li>
                                <li>
                                    <a href="index.html#social-2" role="tab" data-toggle="tab" class="tab twitter"> <span>Twitter</span></a>
                                </li>
                                <li>
                                    <a href="index.html#social-3" role="tab" data-toggle="tab" class="tab google"> <span>Google+</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes-->
                            <div class="tab-content">
                                <div id="social-1" role="tabpanel" class="tab-pane fade in active">
                                    <div class="list-news">
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">British Grand Prix Owner Reveals It Has Run Out Of Cash</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Afghanistan: Western Cure Worse Than Taliban Disease</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How Low Oil Prices Could Reshape Geopolitics--And Drive Peace In Asia</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Heads Roll At BHP Billiton As Profit Flips To A Loss</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">China 2.0: How Upgraded Cities Are Driving The Future Of China</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Corporate India's Financial Problems Worsen</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">China Chases Tough Photo Market With Getty Images Investment</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="social-2" role="tabpanel" class="tab-pane fade">
                                    <div class="list-news">
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Afghanistan: Western Cure Worse Than Taliban Disease</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How Low Oil Prices Could Reshape Geopolitics--And Drive Peace In Asia</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Heads Roll At BHP Billiton As Profit Flips To A Loss</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">China 2.0: How Upgraded Cities Are Driving The Future Of China</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Corporate India's Financial Problems Worsen</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">China Chases Tough Photo Market With Getty Images Investment</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">British Grand Prix Owner Reveals It Has Run Out Of Cash</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="social-3" role="tabpanel" class="tab-pane fade">
                                    <div class="list-news">
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Heads Roll At BHP Billiton As Profit Flips To A Loss</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">China 2.0: How Upgraded Cities Are Driving The Future Of China</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Corporate India's Financial Problems Worsen</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">China Chases Tough Photo Market With Getty Images Investment</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How Low Oil Prices Could Reshape Geopolitics--And Drive Peace In Asia</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">British Grand Prix Owner Reveals It Has Run Out Of Cash</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a></div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Afghanistan: Western Cure Worse Than Taliban Disease</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="banner-widget widget">
                            <div class="section-content">
                                <a href="index.html#" class="link"><img src="themes/frontend/assets/img/banner-images/travel_ads_1.png" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="latest-news-widget widget">
                            <div class="title-topic-2">Latest news</div>
                            <div class="block-news-3">
                                <div class="block-news-content">
                                    <div class="layout-main-news">
                                        <div class="news-layout-1 sport"><a href="sport.html" class="label-topic-1"><span>sport</span></a>
                                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-1.jpg" alt="" class="img-responsive"></a>
                                            <div class="news-content"><a href="index.html#" class="title">Larsen, Lennarth Take 2nd-round Lead at Ladies Masters</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layout-list-news">
                                        <div class="single-recent-post-widget">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-1.jpg" alt="" class="img-wrapper"></a>
                                            <div class="post-info"><a href="index.html" class="label-topic-2">news</a><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a></div>
                                        </div>
                                        <div class="single-recent-post-widget travel">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-2.jpg" alt="" class="img-wrapper"></a>
                                            <div class="post-info"><a href="travel.html" class="label-topic-2">travel</a><a href="index.html#" class="title">Secrets in Paradise: The Best Caribbean Beaches You’ve Never Heard Of</a></div>
                                        </div>
                                        <div class="single-recent-post-widget entertainment">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-3.jpg" alt="" class="img-wrapper"></a>
                                            <div class="post-info"><a href="entertainment.html" class="label-topic-2">entertainment</a><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a></div>
                                        </div>
                                        <div class="single-recent-post-widget business">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-4.jpg" alt="" class="img-wrapper"></a>
                                            <div class="post-info"><a href="business.html" class="label-topic-2">business</a><a href="index.html#" class="title">Here's one Reason Ben Carson is Still Fighting for The GOP Nomination</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="banner-widget widget">
                            <div class="section-content">
                                <a href="index.html#" class="link"><img src="themes/frontend/assets/img/banner-images/travel_ads_2.png" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="banner-chart-main padding-top-60 padding-bottom-60">
    <div class="container">
        <div class="title-topic">Markets number</div>
        <div class="socials-tab-widget business">
            <ul role="tablist" class="nav nav-tabs nav-justified socials-tab banner-stock-tab">
                <li class="active">
                    <a href="index.html#tab-chart-1" role="tab" data-toggle="tab" class="tab tab-related"> <span>Indices</span></a>
                </li>
                <li>
                    <a href="index.html#tab-chart-1" role="tab" data-toggle="tab" class="tab tab-more"> <span>currencies</span></a>
                </li>
                <li>
                    <a href="index.html#tab-chart-1" role="tab" data-toggle="tab" class="tab tab-related"> <span>commodities</span></a>
                </li>
                <li>
                    <a href="index.html#tab-chart-1" role="tab" data-toggle="tab" class="tab tab-more"> <span>stocks</span></a>
                </li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content">
                <div id="tab-chart-1" role="tabpanel" class="tab-pane fade in active">
                    <div class="list-charts-wrapper">
                        <div class="item">
                            <div class="chart-wrapper">
                                <div class="name">Dow Jones</div>
                                <div class="value-total">16,697.29</div><canvas id="myChart-1" width="224" height="150"></canvas>
                                <div class="value-change">212.30 (1.30%)</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chart-wrapper">
                                <div class="name">Nasdaq</div>
                                <div class="value-total">4,582.20</div><canvas id="myChart-2" width="224" height="150"></canvas>
                                <div class="value-change">39.60 (0.90%)</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chart-wrapper">
                                <div class="name">S&amp;P 500</div>
                                <div class="value-total">1,951.70</div><canvas id="myChart-3" width="224" height="150"></canvas>
                                <div class="value-change">21.90 (1.10%)</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chart-wrapper">
                                <div class="name">FTSE 100</div>
                                <div class="value-total">6,012.81</div><canvas id="myChart-4" width="224" height="150"></canvas>
                                <div class="value-change">145.63 (2.50%)</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="chart-wrapper">
                                <div class="name">Hang Seng</div>
                                <div class="value-total">18,888.75</div><canvas id="myChart-5" width="224" height="150"></canvas>
                                <div class="value-change">48.07(0.30%)</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-news-wrapper padding-top-60 padding-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 main-news">
                <div class="row catelogy-topic">
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <!-- BLOCK 3-->
                        <div class="block-news-3 business">
                            <div class="title-topic">business</div>
                            <div class="block-news-content">
                                <div class="layout-main-news">
                                    <div class="news-layout-1">
                                        <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-3.jpg" alt="" class="img-responsive"></a>
                                        <div class="news-content"><a href="index.html#" class="title">30 Under 30 Asia List: 300 Top Young Leaders, Inventors</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="layout-list-news">
                                    <ul role="tablist" class="nav nav-tabs nav-justified tab-news-1">
                                        <li class="active"><a href="index.html#finance" role="tab" data-toggle="tab" class="tab">finance</a></li>
                                        <li><a href="index.html#investing" role="tab" data-toggle="tab" class="tab">investing</a></li>
                                        <li><a href="index.html#markets" role="tab" data-toggle="tab" class="tab">markets</a></li>
                                    </ul>
                                    <!-- Tab panes-->
                                    <div class="tab-content">
                                        <div id="finance" role="tabpanel" class="tab-pane fade in active">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="investing" role="tabpanel" class="tab-pane fade">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="markets" role="tabpanel" class="tab-pane fade">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <!-- BLOCK 3-->
                        <div class="block-news-3 entertainment">
                            <div class="title-topic">entertainment</div>
                            <div class="block-news-content">
                                <div class="layout-main-news">
                                    <div class="news-layout-1">
                                        <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-4.jpg" alt="" class="img-responsive"></a>
                                        <div class="news-content"><a href="index.html#" class="title">First look at 'Underground' with John Legend</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="layout-list-news">
                                    <ul role="tablist" class="nav nav-tabs nav-justified tab-news-1">
                                        <li class="active"><a href="index.html#movie" role="tab" data-toggle="tab" class="tab">movie</a></li>
                                        <li><a href="index.html#music" role="tab" data-toggle="tab" class="tab">music</a></li>
                                        <li><a href="index.html#book" role="tab" data-toggle="tab" class="tab">book</a></li>
                                    </ul>
                                    <!-- Tab panes-->
                                    <div class="tab-content">
                                        <div id="movie" role="tabpanel" class="tab-pane fade in active">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="music" role="tabpanel" class="tab-pane fade">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="book" role="tabpanel" class="tab-pane fade">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <!-- BLOCK 3-->
                        <div class="block-news-3 lifestyle">
                            <div class="title-topic">lifestyle</div>
                            <div class="block-news-content">
                                <div class="layout-main-news">
                                    <div class="news-layout-1">
                                        <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-5.jpg" alt="" class="img-responsive"></a>
                                        <div class="news-content"><a href="index.html#" class="title">How to Gina Rodriguez gets out of speeding tickets</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="layout-list-news">
                                    <ul role="tablist" class="nav nav-tabs nav-justified tab-news-1">
                                        <li class="active"><a href="index.html#fashion" role="tab" data-toggle="tab" class="tab">fashion</a></li>
                                        <li><a href="index.html#health" role="tab" data-toggle="tab" class="tab">health</a></li>
                                        <li><a href="index.html#food" role="tab" data-toggle="tab" class="tab">food</a></li>
                                    </ul>
                                    <!-- Tab panes-->
                                    <div class="tab-content">
                                        <div id="fashion" role="tabpanel" class="tab-pane fade in active">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="health" role="tabpanel" class="tab-pane fade">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="food" role="tabpanel" class="tab-pane fade">
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BANNER ADV-->
                    <div class="col-xs-12 banner-hidden">
                        <div class="banner-adv-wrapper margin-bottom-60">
                            <a href="index.html#" class="banner-adv-728x90"><img src="themes/frontend/assets/img/banner-images/banner-728x90-3.jpg" alt="" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <!-- BLOCK 3-->
                        <div class="block-news-3 tech">
                            <div class="title-topic">tech</div>
                            <div class="block-news-content">
                                <div class="layout-main-news">
                                    <div class="news-layout-1">
                                        <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-6.jpg" alt="" class="img-responsive"></a>
                                        <div class="news-content"><a href="index.html#" class="title">30 Under 30 Asia List: 300 Top Young Leaders, Inventors</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="layout-list-news">
                                    <div class="single-recent-post-widget">
                                        <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-5.jpg" alt="" class="img-wrapper"></a>
                                        <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <!-- BLOCK 3-->
                        <div class="block-news-3 sport">
                            <div class="title-topic">sport</div>
                            <div class="block-news-content">
                                <div class="layout-main-news">
                                    <div class="news-layout-1">
                                        <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-7.jpg" alt="" class="img-responsive"></a>
                                        <div class="news-content"><a href="index.html#" class="title">First look at 'Underground' with John Legend</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="layout-list-news">
                                    <div class="single-recent-post-widget">
                                        <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-6.jpg" alt="" class="img-wrapper"></a>
                                        <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <!-- BLOCK 3-->
                        <div class="block-news-3 travel">
                            <div class="title-topic">travel</div>
                            <div class="block-news-content">
                                <div class="layout-main-news">
                                    <div class="news-layout-1">
                                        <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-8.jpg" alt="" class="img-responsive"></a>
                                        <div class="news-content"><a href="index.html#" class="title">How to Gina Rodriguez gets out of speeding tickets</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="layout-list-news">
                                    <div class="single-recent-post-widget">
                                        <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/news/small-news-7.jpg" alt="" class="img-wrapper"></a>
                                        <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="single-recent-post-widget">
                                        <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                            <ul class="info">
                                                <li><a href="index.html#" class="link">By timothy</a></li>
                                                <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                <li><a href="index.html#" class="link">15 likes</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-xs-12 main-news main-left">
                <!-- BLOCK 5-->
                <div class="block-news-5 margin-bottom-60">
                    <div class="title-topic-2">watch it</div>
                    <div class="block-news-content">
                        <div class="layout-main-news">
                            <div class="news-layout-1 video">
                                <div class="news-image">
                                    <!--span.mask-gradient.style-5--><iframe src="https://www.youtube.com/embed/lvtfD_rJ2hE" allowfullscreen="allowfullscreen" class="news-video"></iframe></div>
                                <div class="news-content">
                                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                </div>
                            </div>
                        </div>
                        <div class="layout-list-news">
                            <div class="row-wrapper clearfix">
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-12.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">U.S. Stocks Close Sharply Lower On New China Concerns</a></div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-13.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">Asia's Markets Steeply in the Red, Nikkei in Bear Market</a></div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-14.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">According to Consumer Reports, These Are The Best Cars for 2016</a></div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-15.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a></div>
                                </div>
                            </div>
                            <div class="row-wrapper clearfix">
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-16.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">Baking With Buffy: Sarah Michelle Gellar's Food Startup</a></div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-17.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">Tesla Underestimates How Much It Costs To Charge A Model S</a></div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-18.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a></div>
                                </div>
                                <div class="items news-layout-1 small">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/small-news-19.jpg" alt="" class="img-responsive"><span class="btn-play"><i class="fa fa-play"></i></span><span class="time">1:25</span></a>
                                    <div class="news-content"><a href="index.html#" class="title">Facebook No Longer Just Has A 'Like' Button, Thanks To Global Launch</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- BLOCK 4-->
                <div class="block-news-4">
                    <div class="title-topic-2">voices</div>
                    <div class="block-news-content">
                        <div class="layout-main-news">
                            <div class="news-layout-2">
                                <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/news-1.jpg" alt="" class="img-responsive"></a>
                                <div class="news-content"><a href="index.html#" class="title">Kanye To GoFundMe Supporters: I Don't Want Your Money</a>
                                    <ul class="info">
                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                    </ul>
                                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                            </div>
                        </div>
                        <div class="layout-list-news">
                            <div class="left-news business">
                                <ul role="tablist" class="nav nav-tabs nav-justified tab-news-1">
                                    <li><a href="index.html#opinions" role="tab" data-toggle="tab" class="tab text-left">opinions</a></li>
                                </ul>
                                <!-- Tab panes-->
                                <div class="tab-content">
                                    <div id="opinions" role="tabpanel" class="tab-pane fade in active">
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="right-news business">
                                <ul role="tablist" class="nav nav-tabs nav-justified tab-news-1">
                                    <li><a href="index.html#latest-voices" role="tab" data-toggle="tab" class="tab text-left">latest-voices</a></li>
                                </ul>
                                <!-- Tab panes-->
                                <div class="tab-content">
                                    <div id="latest-voices" role="tabpanel" class="tab-pane fade in active">
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Will We Be Forced To Welcome Our Insecticide-Resistant Bed Bug Overlords?</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="single-recent-post-widget">
                                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 sidebar scroll-sidebar">
                <div class="title-topic">right now</div>
                <div class="scroll-sidebar-content widget">
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-2.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">China 'Doom' Fears Linger, but Oil Prices Rise</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">3 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-5.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">5 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-6.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">6 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-7.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">7 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">8 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Honestbee's Joel Sng Sets Big Regional Goals For Grocery Delivery Startup</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Facebook No Longer Just Has A 'Like' Button, Thanks To Global Launch Of Emoji 'Reactions'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-3.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">India Aims for Self-Sufficiency, Ends State Coal Monopoly</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">12 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">The Prosecutor In The OJ Simpson Case isn't Pleased With 'American Crime Story'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">14 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-8.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-9.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-2.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">China 'Doom' Fears Linger, but Oil Prices Rise</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">3 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-5.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">5 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-6.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">6 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-7.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">7 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">8 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Honestbee's Joel Sng Sets Big Regional Goals For Grocery Delivery Startup</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Facebook No Longer Just Has A 'Like' Button, Thanks To Global Launch Of Emoji 'Reactions'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-3.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">India Aims for Self-Sufficiency, Ends State Coal Monopoly</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">12 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">The Prosecutor In The OJ Simpson Case isn't Pleased With 'American Crime Story'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">14 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-8.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-9.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-2.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">China 'Doom' Fears Linger, but Oil Prices Rise</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">3 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-5.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">5 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-6.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">6 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-7.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">7 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">8 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Honestbee's Joel Sng Sets Big Regional Goals For Grocery Delivery Startup</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Facebook No Longer Just Has A 'Like' Button, Thanks To Global Launch Of Emoji 'Reactions'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-3.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">India Aims for Self-Sufficiency, Ends State Coal Monopoly</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">12 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">The Prosecutor In The OJ Simpson Case isn't Pleased With 'American Crime Story'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">14 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-8.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-9.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-2.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">China 'Doom' Fears Linger, but Oil Prices Rise</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">3 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-5.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">4 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">5 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-6.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">6 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-7.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">7 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">How The Philippines Got Asia's Worst Internet Service</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">8 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Honestbee's Joel Sng Sets Big Regional Goals For Grocery Delivery Startup</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Facebook No Longer Just Has A 'Like' Button, Thanks To Global Launch Of Emoji 'Reactions'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">10 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="news-layout-1">
                            <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sidebar/news-3.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">India Aims for Self-Sufficiency, Ends State Coal Monopoly</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">12 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">The Prosecutor In The OJ Simpson Case isn't Pleased With 'American Crime Story'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">14 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-8.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-9.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">The Prosecutor In The OJ Simpson Case isn't Pleased With 'American Crime Story'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">14 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-8.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-9.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">The Prosecutor In The OJ Simpson Case isn't Pleased With 'American Crime Story'</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">14 mins ago</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-8.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">Goldman Sachs to Pay $5 Billion in Mortgage Settlement</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Fed's Interest Rate Hike Was 'Close Call' for Some</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <div class="post-info"><a href="index.html#" class="title">Spotify Moving Onto Google Cloud Is A Big Win For Google Over Amazon And Microsoft</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="news-item">
                        <div class="single-recent-post-widget">
                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/sidebar/small-news-9.jpg" alt="" class="img-responsive"></a>
                            <div class="post-info"><a href="index.html#" class="title">China Shares Turn Higher After Wild Start to 2016</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="banner-news-main padding-top-60 padding-bottom-60">
    <div class="container">
        <div class="title-topic">In case you missed it</div>
        <div class="banner-news-wrapper">
            <div class="main-news">
                <div class="main-slide owl-carousel">
                    <div data-item="item-1" class="item">
                        <div class="block-item news-layout-3 big"><a href="index.html#" class="news-image"><span class="mask-gradient style-1"></span><img src="themes/frontend/assets/img/news/block-main-1.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">Blogging Best Practices: Make It Easy To Subscribe To Your Blog</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div data-item="item-2" class="item">
                        <div class="block-item news-layout-3 big"><a href="index.html#" class="news-image"><span class="mask-gradient style-4"></span><img src="themes/frontend/assets/img/news/block-main-2.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">If Your Real Estate Blog Is Made by WIX Check What Google Says About The Drop</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div data-item="item-3" class="item">
                        <div class="block-item news-layout-3 big"><a href="index.html#" class="news-image"><span class="mask-gradient style-2"></span><img src="themes/frontend/assets/img/news/block-main-3.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">Suzanne commented on Kitson Harvey's blog post Morning Fix: In Offline Mode</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div data-item="item-4" class="item">
                        <div class="block-item news-layout-3 big"><a href="index.html#" class="news-image"><span class="mask-gradient style-3"></span><img src="themes/frontend/assets/img/news/block-main-2.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">Alex Guarnaschelli Blogs: Everything You Need to Know About Buying Food</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div data-item="item-5" class="item">
                        <div class="block-item news-layout-3 big"><a href="index.html#" class="news-image"><span class="mask-gradient style-5"></span><img src="themes/frontend/assets/img/news/block-main-1.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">Blogger campaigns to make Israel's voice heard in NY Times</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div data-item="item-6" class="item">
                        <div class="block-item news-layout-3 big"><a href="index.html#" class="news-image"><span class="mask-gradient style-6"></span><img src="themes/frontend/assets/img/news/block-main-3.jpg" alt="" class="img-responsive"></a>
                            <div class="news-content"><a href="index.html#" class="title">How to Make Money Blogging:Yes - You Can Still Make Money Blogging</a>
                                <ul class="info">
                                    <li><a href="index.html#" class="link">By timothy</a></li>
                                    <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                    <li><a href="index.html#" class="link">15 likes</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="title-news">
                <ul data-mcs-theme="minimal-dark" class="slide-items mCustomScrollbar light">
                    <li class="item-1 active"><a href="index.html#" class="title">Blogging Best Practices: Make It Easy To Subscribe To Your Blog</a></li>
                    <li class="item-2"><a href="index.html#" class="title">If Your Real Estate Blog Is Made by WIX Check What Google Says About The Drop</a></li>
                    <li class="item-3"><a href="index.html#" class="title">Suzanne commented on Kitson Harvey's blog post Morning Fix: In Offline Mode</a></li>
                    <li class="item-4"><a href="index.html#" class="title">Alex Guarnaschelli Blogs: Everything You Need to Know About Buying Food and the executive chef at New York City's Butter</a></li>
                    <li class="item-5"><a href="index.html#" class="title">Blogger campaigns to make Israel's voice heard in NY Times</a></li>
                    <li class="item-6"><a href="index.html#" class="title">How to Make Money Blogging:Yes - You Can Still Make Money Blogging</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main-news-wrapper padding-top-60 padding-bottom-60">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-xs-12 main-news main-left">
                <!-- GALLERY PICTURE-->
                <div class="title-topic">gallery</div>
                <div class="gallery-slide entertainment margin-bottom-60">
                    <ul role="tablist" class="nav nav-tabs nav-justified tab-news-2">
                        <li class="active"><a href="index.html#tab1" role="tab" data-toggle="tab" class="tab">The Stars at Grammy Awards</a></li>
                        <li><a href="index.html#tab2" role="tab" data-toggle="tab" class="tab">The USA Elections</a></li>
                        <li><a href="index.html#tab3" role="tab" data-toggle="tab" class="tab">Top 10 Travel Place This Summer</a></li>
                        <li><a href="index.html#tab4" role="tab" data-toggle="tab" class="tab">Samsung Galaxy S7 Review</a></li>
                        <li><a href="index.html#tab5" role="tab" data-toggle="tab" class="tab">Oscar best picture</a></li>
                    </ul>
                    <!-- Tab panes-->
                    <div class="tab-content">
                        <div id="tab1" role="tabpanel" class="tab-pane fade in active">
                            <div class="slider-for style-btn-slide">
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-1.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-2.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-3.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-4.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-5.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-6.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-7.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-8.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-nav">
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-1.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-2.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-3.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-4.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-5.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-6.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-7.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-8.jpg" alt="" class="img-responsive"></div>
                                </div>
                            </div>
                        </div>
                        <div id="tab2" role="tabpanel" class="tab-pane fade">
                            <div class="slider-for style-btn-slide">
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-1.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-2.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-3.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-4.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-5.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-6.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-7.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-8.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-nav">
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-1.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-2.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-3.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-4.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-5.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-6.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-7.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-8.jpg" alt="" class="img-responsive"></div>
                                </div>
                            </div>
                        </div>
                        <div id="tab3" role="tabpanel" class="tab-pane fade">
                            <div class="slider-for style-btn-slide">
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-1.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-2.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-3.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-4.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-5.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-6.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-7.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-8.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-nav">
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-1.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-2.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-3.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-4.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-5.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-6.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-7.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-8.jpg" alt="" class="img-responsive"></div>
                                </div>
                            </div>
                        </div>
                        <div id="tab4" role="tabpanel" class="tab-pane fade">
                            <div class="slider-for style-btn-slide">
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-1.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-2.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-3.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-4.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-5.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-6.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-7.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/sport/gallery-8.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-nav">
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-1.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-2.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-3.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-4.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-5.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-6.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-7.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/sport/gallery-8.jpg" alt="" class="img-responsive"></div>
                                </div>
                            </div>
                        </div>
                        <div id="tab5" role="tabpanel" class="tab-pane fade">
                            <div class="slider-for style-btn-slide">
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-1.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-2.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-3.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-4.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-5.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-6.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-7.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                </div>
                                <div class="items news-layout-3">
                                    <a href="index.html#" class="news-image"><img src="themes/frontend/assets/img/news/gallery-8.jpg" alt="" class="img-responsive"></a>
                                    <div class="news-content">
                                        <p class="description">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-nav">
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-1.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-2.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-3.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-4.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-5.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-6.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-7.jpg" alt="" class="img-responsive"></div>
                                </div>
                                <div class="items">
                                    <div class="news-image"><img src="themes/frontend/assets/img/news/gallery-8.jpg" alt="" class="img-responsive"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- BANNER ADV-->
                <div class="banner-adv-wrapper">
                    <a href="index.html#" class="banner-adv-728x90"><img src="themes/frontend/assets/img/banner-images/banner-728x90-3.jpg" alt="" class="img-responsive"></a>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 sidebar">
                <div class="sidebar-wrapper">
                    <div class="follow-on-social-widget widget">
                        <div class="title-topic-2">follow us on social</div>
                        <div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
                            <div class="panel panel-collapse">
                                <div id="headingOne" role="tab" class="panel-heading">
                                    <div class="panel-title facebook"><i class="icons fa fa-facebook"></i><a role="button" data-toggle="collapse" data-parent="#accordion" href="index.html#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="link">1,250 fans<span class="sub-text">like</span></a></div>
                                </div>
                                <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">directnews</span><span class="text">@directnewswp</span></div>
                                                    <div class="right-text"><span class="text">1 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Practice makes perfect! What a great idea...</span><a href="index.html#" class="link">@newswptheme</a></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">edugate</span><span class="text">@edugate</span></div>
                                                    <div class="right-text"><span class="text">1 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Blogging Best Practices: Make It Easy To Subscribe...</span><a href="index.html#" class="link">@themeforest</a></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">bignews</span><span class="text">@bignewswp</span></div>
                                                    <div class="right-text"><span class="text">8 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Practice makes perfect! What a great idea...</span></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-collapse">
                                <div id="headingTwo" role="tab" class="panel-heading">
                                    <div class="panel-title twitter"><i class="icons fa fa-twitter"></i><a role="button" data-toggle="collapse" data-parent="#accordion" href="index.html#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="link collapsed">2,450 followers<span class="sub-text">follow</span></a></div>
                                </div>
                                <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">exploore</span><span class="text">@exploore</span></div>
                                                    <div class="right-text"><span class="text">5 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Blogging Best Practices: Make It Easy To Subscribe...</span><a href="index.html#" class="link">@exploore</a></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">sunhouse</span><span class="text">@sunhouse</span></div>
                                                    <div class="right-text"><span class="text">8 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Spotify Moving Onto Google Cloud Is...</span><a href="index.html#" class="link">@themeforest</a></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">giganews</span><span class="text">@giganews</span></div>
                                                    <div class="right-text"><span class="text">8 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Practice makes perfect! What a great idea...</span></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-collapse">
                                <div id="headingThree" role="tab" class="panel-heading">
                                    <div class="panel-title google"><i class="icons fa fa-google-plus"></i><a role="button" data-toggle="collapse" data-parent="#accordion" href="index.html#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="link collapsed">892 subcribers<span class="sub-text">subcribe</span></a></div>
                                </div>
                                <div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">bignews</span><span class="text">@bignewswp</span></div>
                                                    <div class="right-text"><span class="text">2 day ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Spotify Moving Onto Google Cloud Is...</span><a href="index.html#" class="link">@newswptheme</a></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">edugate</span><span class="text">@edugatewp</span></div>
                                                    <div class="right-text"><span class="text">5 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Practice makes perfect! What a great idea...</span><a href="index.html#" class="link">@themeforest</a></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="directnew-recent-post">
                                            <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/more-image/avatar.png" alt="" class="img-wrapper"></a>
                                            <div class="post-info">
                                                <div class="title">
                                                    <div class="left-text"><span class="strongtext">directnews</span><span class="text">@directnewswp</span></div>
                                                    <div class="right-text"><span class="text">1 hour ago</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="description"><span class="text">Practice makes perfect! What a great idea...</span></div>
                                                <ul class="info">
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-reply"></i><span>reply</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-refresh"></i><span>retweet</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-star"></i><span>favourite</span></a></li>
                                                    <li><a href="index.html#" class="link"><i class="icons fa fa-ellipsis-h"></i><span>more</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="banner-widget widget">
                        <div class="section-content">
                            <a href="index.html#" class="link"><img src="themes/frontend/assets/img/banner-images/travel_ads_1.png" alt="" class="img-responsive"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
