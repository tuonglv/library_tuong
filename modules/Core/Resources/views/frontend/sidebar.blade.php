@extends('core::frontend.default2')

@section('sidebar')

<aside id="sidebar">

    <div id="solopine_about_widget-2" class="widget solopine_about_widget"><h4 class="widget-heading"><span>About Me</span>
        </h4>
        <div class="about-widget">

            <img src="http://solopine.com/florence/wp-content/uploads/2014/11/abou3.jpg" alt="About Me"/>

            <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack
                nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me..</p>


        </div>

    </div>
    <div id="solopine_social_widget-2" class="widget solopine_social_widget"><h4 class="widget-heading"><span>Subscribe &#038; Follow</span>
        </h4>
        <div class="widget-social">
            <a href="http://facebook.com/groups/257713851081977/?fref=ts#" target="_blank"><i
                        class="fa fa-facebook"></i></a> <a
                    href="http://twitter.com/solopinedesigns" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="http://instagram.com/solopine" target="_blank"><i class="fa fa-instagram"></i></a> <a
                    href="http://pinterest.com/solopinedesigns" target="_blank"><i class="fa fa-pinterest"></i></a>
            <a href="http://bloglovin.com/#" target="_blank"><i class="fa fa-heart"></i></a> <a
                    href="http://plus.google.com/#" target="_blank"><i class="fa fa-google-plus"></i></a> <a
                    href="http://#.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a> <a href="#"
                                                                                                    target="_blank"><i
                        class="fa fa-rss"></i></a></div>


    </div>
    <div id="text-2" class="widget widget_text">
        <div class="textwidget"><img
                    src="http://solopine.com/florence/wp-content/uploads/2014/11/bannerspot2.png" alt="banner"/>
        </div>
    </div>
    <div id="solopine_facebook_widget-2" class="widget solopine_facebook_widget"><h4 class="widget-heading">
            <span>Find us on Facebook</span></h4>
        <iframe src="http://www.facebook.com/plugins/likebox.php?href=http://www.facebook.com/solopinedesigns&amp;width=300&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false&amp;height=290&amp;show_border=false"
                scrolling="no" frameborder="0"
                style="border:none; overflow:hidden; width:300px; height:290px; background:#fff;"
                allowTransparency="true"></iframe>


    </div>
    <div id="tag_cloud-2" class="widget widget_tag_cloud"><h4 class="widget-heading"><span>Tagcloud</span></h4>
        <div class="tagcloud"><a href='http://solopine.com/florence/tag/adventure/'
                                 class='tag-link-4 tag-link-position-1' title='1 topic' style='font-size: 8pt;'>adventure</a>
            <a href='http://solopine.com/florence/tag/floral/' class='tag-link-10 tag-link-position-2'
               title='1 topic' style='font-size: 8pt;'>floral</a>
            <a href='http://solopine.com/florence/tag/forest/' class='tag-link-5 tag-link-position-3'
               title='1 topic' style='font-size: 8pt;'>forest</a>
            <a href='http://solopine.com/florence/tag/instagram/' class='tag-link-18 tag-link-position-4'
               title='1 topic' style='font-size: 8pt;'>instagram</a>
            <a href='http://solopine.com/florence/tag/leaf/' class='tag-link-7 tag-link-position-5'
               title='1 topic' style='font-size: 8pt;'>leaf</a>
            <a href='http://solopine.com/florence/tag/nature/' class='tag-link-3 tag-link-position-6'
               title='1 topic' style='font-size: 8pt;'>nature</a>
            <a href='http://solopine.com/florence/tag/new-york/' class='tag-link-13 tag-link-position-7'
               title='1 topic' style='font-size: 8pt;'>New York</a>
            <a href='http://solopine.com/florence/tag/northwest/' class='tag-link-8 tag-link-position-8'
               title='1 topic' style='font-size: 8pt;'>northwest</a>
            <a href='http://solopine.com/florence/tag/ocean/' class='tag-link-19 tag-link-position-9'
               title='1 topic' style='font-size: 8pt;'>ocean</a>
            <a href='http://solopine.com/florence/tag/pacific/' class='tag-link-9 tag-link-position-10'
               title='1 topic' style='font-size: 8pt;'>pacific</a>
            <a href='http://solopine.com/florence/tag/pine/' class='tag-link-6 tag-link-position-11'
               title='1 topic' style='font-size: 8pt;'>pine</a></div>
    </div>
    <div id="categories-2" class="widget widget_categories"><h4 class="widget-heading"><span>Categories</span>
        </h4><label class="screen-reader-text" for="cat">Categories</label><select name='cat' id='cat'
                                                                                   class='postform'>
            <option value='-1'>Select Category</option>
            <option class="level-0" value="2">Lifestyle</option>
            <option class="level-0" value="15">Music</option>
            <option class="level-0" value="12" selected="selected">Travel</option>
        </select>

        <script type='text/javascript'>
            /* <![CDATA[ */
            (function () {
                var dropdown = document.getElementById("cat");

                function onCatChange() {
                    if (dropdown.options[dropdown.selectedIndex].value > 0) {
                        location.href = "http://solopine.com/florence/?cat=" + dropdown.options[dropdown.selectedIndex].value;
                    }
                }

                dropdown.onchange = onCatChange;
            })();
            /* ]]> */
        </script>

    </div>
    <div id="solopine_latest_news_widget-2" class="widget solopine_latest_news_widget"><h4
                class="widget-heading"><span>Latest Posts</span></h4>
        <ul class="side-newsfeed">


            <li>

                <div class="side-item">

                    <div class="side-image">
                        <a href="http://solopine.com/florence/floral-wallpaper/" rel="bookmark"><img width="500"
                                                                                                     height="380"
                                                                                                     src="http://solopine.com/florence/wp-content/uploads/2014/11/flower-girl-500x380.jpg"
                                                                                                     class="side-item-thumb wp-post-image"
                                                                                                     alt="flower-girl"/></a>
                    </div>
                    <div class="side-item-text">
                        <h4><a href="http://solopine.com/florence/floral-wallpaper/" rel="bookmark">Floral
                                Wallpaper</a></h4>
                        <span class="side-item-meta">November 20, 2014</span>
                    </div>
                </div>

            </li>


            <li>

                <div class="side-item">

                    <div class="side-image">
                        <a href="http://solopine.com/florence/exploring-new-york/" rel="bookmark"><img
                                    width="500" height="380"
                                    src="http://solopine.com/florence/wp-content/uploads/2014/11/newyork-500x380.jpg"
                                    class="side-item-thumb wp-post-image" alt="newyork"/></a>
                    </div>
                    <div class="side-item-text">
                        <h4><a href="http://solopine.com/florence/exploring-new-york/" rel="bookmark">Exploring
                                New York</a></h4>
                        <span class="side-item-meta">November 20, 2014</span>
                    </div>
                </div>

            </li>


            <li>

                <div class="side-item">

                    <div class="side-image">
                        <a href="http://solopine.com/florence/summer-outfit/" rel="bookmark"><img width="500"
                                                                                                  height="380"
                                                                                                  src="http://solopine.com/florence/wp-content/uploads/2014/11/outfit-500x380.jpg"
                                                                                                  class="side-item-thumb wp-post-image"
                                                                                                  alt="outfit"/></a>
                    </div>
                    <div class="side-item-text">
                        <h4><a href="http://solopine.com/florence/summer-outfit/" rel="bookmark">Summer
                                Outfit</a></h4>
                        <span class="side-item-meta">November 20, 2014</span>
                    </div>
                </div>

            </li>


            <li>

                <div class="side-item">

                    <div class="side-image">
                        <a href="http://solopine.com/florence/pacific-northwest/" rel="bookmark"><img
                                    width="500" height="380"
                                    src="http://solopine.com/florence/wp-content/uploads/2014/11/gal1-500x380.jpg"
                                    class="side-item-thumb wp-post-image" alt="gal1"/></a>
                    </div>
                    <div class="side-item-text">
                        <h4><a href="http://solopine.com/florence/pacific-northwest/" rel="bookmark">Pacific
                                Northwest</a></h4>
                        <span class="side-item-meta">November 20, 2014</span>
                    </div>
                </div>

            </li>


            <li>

                <div class="side-item">

                    <div class="side-image">
                        <a href="http://solopine.com/florence/cosmic-love/" rel="bookmark"><img width="500"
                                                                                                height="380"
                                                                                                src="http://solopine.com/florence/wp-content/uploads/2014/11/dirtypawas-500x380.jpg"
                                                                                                class="side-item-thumb wp-post-image"
                                                                                                alt="dirtypawas"/></a>
                    </div>
                    <div class="side-item-text">
                        <h4><a href="http://solopine.com/florence/cosmic-love/" rel="bookmark">Cosmic Love</a>
                        </h4>
                        <span class="side-item-meta">November 20, 2014</span>
                    </div>
                </div>

            </li>


        </ul>

    </div>
    <div id="archives-2" class="widget widget_archive"><h4 class="widget-heading"><span>Archives</span></h4>
        <label class="screen-reader-text" for="archives-dropdown-2">Archives</label>
        <select id="archives-dropdown-2" name="archive-dropdown"
                onchange='document.location.href=this.options[this.selectedIndex].value;'>

            <option value="">Select Month</option>
            <option value='http://solopine.com/florence/2014/11/'> November 2014</option>

        </select>
    </div>
</aside>
@stop