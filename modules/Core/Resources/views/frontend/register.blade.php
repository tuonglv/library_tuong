<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
    <title>Login and Registration Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="{!! url('themes/frontend2/assets/css/login/demo.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! url('themes/frontend2/assets/css/login/styles.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! url('themes/frontend2/assets/css/login/animate-custom.css') !!}" />
</head>
<body>
<div class="container">
    <header>
    </header>
    <section>
        <div id="container_demo" >
            <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>
            <div id="wrapper">
                <div id="login" class="animate form">
                    <form  method="post" action="{!! route('register') !!}" autocomplete="on">
                        <h1> Sign up </h1>
                        <p>
                            <label for="username" class="uname" data-icon="u">Your username</label>
                            <input value="{!! isset($meta['nickname'][0])? old($meta[0]['nickname']) : '' !!}" id="usernamesignup" name="meta[nickname]" required="required" type="text" placeholder="mysuperusername690" />
                        </p>
                        <p>
                            <label for="emailsignup" class="youmail" data-icon="e" > Your email</label>
                            <input value="{!! isset($meta['email'][0])? old($meta[0]['email']) : '' !!}" id="emailsignup" name="email" required="required" type="email" placeholder="mysupermail@mail.com"/>
                        </p>
                        <p>
                            <label for="passwordsignup" class="youpasswd" data-icon="p">Your password </label>
                            <input value="{!! isset($meta['password'][0])? old($meta[0]['password']) : '' !!}" id="passwordsignup" name="password" required="required" type="password" placeholder="eg. X8df!90EO"/>
                        </p>
                        <p>
                            <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>
                            <input value="{!! isset($meta['password_confirm'][0])? old($meta[0]['password_confirm']) : '' !!}" id="passwordsignup_confirm" name="password_confirm" required="required" type="password" placeholder="eg. X8df!90EO"/>
                        </p>
                        <p class="signin button">
                            <input type="submit" value="Sign up"/>
                        </p>
                        <p class="change_link">
                            Already a member ?
                            <a href="{!! route('login') !!}" class="to_register"> Login </a>
                        </p>
                        <input type="hidden" name="type" value="default"/>
                    </form>
                </div>

            </div>
        </div>
    </section>
</div>
</body>
</html>