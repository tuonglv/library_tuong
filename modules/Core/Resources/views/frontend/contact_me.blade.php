<div class="container">

    <div id="content">
        <article id="post-85" class="post-85 page type-page status-publish has-post-thumbnail hentry">
            <div class="post-header">

                <h1>Contact Me</h1>

            </div>

            <div class="post-entry">


                <div class="wpcf7" id="wpcf7-f5-p85-o1" lang="en-US" dir="ltr">
                    <div class="screen-reader-response"></div>
                    <form name="" action="/library/contact-me" method="get" class="wpcf7-form" novalidate="novalidate">
                        <div style="display: none;">
                            <input type="hidden" name="_wpcf7" value="5">
                            <input type="hidden" name="_wpcf7_version" value="4.0.1">
                            <input type="hidden" name="_wpcf7_locale" value="en_US">
                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5-p85-o1">
                            <input type="hidden" name="_wpnonce" value="7a035b503b">
                        </div>
                        <p>Họ Tên (không được để trống)<br>
                            <span class="wpcf7-form-control-wrap your-name"><input required="required" type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </p>
                        <p>Email (không được để trống)<br>
                            <span class="wpcf7-form-control-wrap your-email"><input required="required" type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </p>
                        <p>Tiêu Đề<br>
                            <span class="wpcf7-form-control-wrap your-subject"><input required="required" type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </p>
                        <p>Nội Dung<br>
                            <span class="wpcf7-form-control-wrap your-message"><textarea required="required" name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </p>
                        <p><input type="submit" value="Gửi Đi" class="btn-send wpcf7-form-control wpcf7-submit"><img class="ajax-loader" src="#" alt="Sending ..." style="visibility: hidden;"></p>
                        <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>

            </div>

        </article>

        <!-- END CONTENT -->
    </div>

    <!-- END CONTAINER -->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.btn-send').click(function(){
            var your_name        = $("input[name='your-name']").val();
            var your_email       = $("input[name='your-email']").val();
            var your_subject     = $("input[name='your-subject']").val();
            var your_message     = $("input[name='your-message']").val();
            if(your_name != null && your_email != null && your_subject != null && your_message != null ){
                alert('Cám ơn bạn đã đóng góp ý kiến ');
            }
        });
    });
</script>