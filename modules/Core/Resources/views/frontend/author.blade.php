<div class="container">

    <div id="content">

        <div id="main" class="fullwidth">


            <article id="post-69" class="post-69 page type-page status-publish hentry">


                <div class="post-header">

                    <h1>About Me</h1>

                </div>
                <div class="post-entry">
                    <p><img class="img_author alignright wp-image-70 size-full"
                            src="{!! isset($user['avatar']) ? $user['avatar'] : 'http://solopine.com/florence/wp-content/uploads/2014/11/aboutpage.jpg' !!} "
                            alt="aboutpage" width="380" height="380" sizes="(max-width: 380px) 100vw, 380px"></p>
                    <div class="abount_text_left">

                        <p>
                            Tên: {!! isset($user['username']) ? $user['username'] : (isset($user['meta']['nickname']) ?$user['meta']['nickname'] : '') !!}</p>
                        <p>Năm Sinh: {!! isset($user['meta']['birthday']) ? $user['meta']['birthday'] : '' !!}</p>
                        <p>Quê Quán/ Địa
                            chỉ: {!! isset($user['meta']['address']) ? $user['meta']['address'] : '' !!}</p>
                        <p>Số Điện Thoại: {!! isset($user['meta']['phone']) ? $user['meta']['phone'] : '' !!}</p>
                        <p>Skype: {!! isset($user['meta']['skype']) ? $user['meta']['skype'] : '' !!}</p>
                        <p>Facebook: {!! isset($user['meta']['facebook']) ? $user['meta']['facebook'] : '' !!}</p>
                        <p>Mô tả : {!! isset($user['meta']['about']) ? $user['meta']['about'] : '' !!}</p>
                        <p>Skills: {!! isset($user['meta']['skills']) ? $user['meta']['skills'] : '' !!}</p>
                    </div>
                </div>
            </article>
        </div>

        <div class="author_posted">
               &nbsp;&nbsp;Bài viết của tác giả
            @if(isset($author_posts['author_posts']) && $author_posts['author_posts'] )
                @foreach($author_posts['author_posts'] as $post)
                    <article id="post-6"
                             class="list-item post-6 post type-post status-publish format-standard has-post-thumbnail hentry category-lifestyle">
                        <div class="post-img">
                            <a href="{!! isset($post['slug']) ? URL::to('library/post/detail\/').$post['slug'] : '' !!}"><img
                                        width="500" height="380"
                                        src="{!! isset($post['image']['url']) ? $post['image']['url'] : 'http://192.168.0.138//files/files/no-image.png' !!}"
                                        alt="{!! isset($post['title']) ? str_limit($post['excerpt'] ,170) : '' !!}"
                                        class="attachment-misc-thumb size-misc-thumb wp-post-image"
                                        alt="flower-girl"></a>
                        </div>

                        <div class="list-content">
                            <div class="post-header">
                                <div class="post_title">
                                    <a href="{!! isset($post['slug']) ? URL::to('library/post/detail\/').$post['slug'] : '' !!}">{!! isset($post['title']) ? str_limit(mb_strtoupper($post['title'],"UTF-8") ,110) : '' !!}</a>
                                </div>
                            </div>
                            <div class="post-entry">
                                <p>{!! isset($post['excerpt']) ? str_limit($post['excerpt'] ,170) : '' !!}</p>
                            </div>
                        </div>
                        <div class="post-meta">
                        <span class="meta-info">
                                 {!! isset($post['updated_at']) ? $post['updated_at'] : '' !!}
                        </span>
                        </div>
                    </article>
                @endforeach
                @if(isset($paginator) && $paginator->hasPages())
                    <div class="front_pagination">
                        <div class="col-md-6">
                            <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                {!! theme()->partial('paginator', ['paginator' => $paginator]) !!}
                            </div>
                        </div>

                    </div>
                @endif
            @else
                No Post !!
            @endif
        </div>


        <!-- END CONTENT -->
    </div>

    <!-- END CONTAINER -->
</div>