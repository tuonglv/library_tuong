<div class="container">

    <div id="content">
        <article id="post-85" class="post-85 page type-page status-publish has-post-thumbnail hentry">
            <div class="post-header">
                <h1>Điều khoản sử dụng BeeBlog (Terms Of Service)</h1>
            </div>
            <div>
                <h4>1. Giới thiệu</h4>

                <p>BeeBlog là sản Website được tạo ra bởi công ty Beetsoft.Co, LTD. Sản phầm dựa trên nền tảng chia sẻ kiến thức chung không phải chỉ riêng trong công ty mà cho cả cộng đồng kỹ thuật của cộng đồng công nghệ thông tin.
                    Sử dụng BeeBlog đồng nghĩa với việc bạn đã đồng ý với các "điều khoản sử dụng" trong thoả thuận này.</p>

                <h4>2. Điều khoản sử dụng là gì?</h4>
                    <p>a) Điều khoản sử dụng là những điều kiện liên quan đến việc sử dụng các BeeBlog, sản phẩm được cung cấp bởi Beetsoft.Co, LTD. Nó định nghĩa các mối quan hệ giữa khách hàng sử dụng dịch vụ (sau đây gọi tắt "User") với Công ty.</p>
                    <p>b) Bằng cách sử dụng BeeBlog bạn chấp nhận "Điều khoản sử dụng" của chúng tôi và đồng ý tuân theo chúng.</p>
                    <p>c) Điều khoản trong thoả thuận này có thể sửa đổi mà không thông báo trước. Các sửa đổi sẽ được thông báo trên trang web hoặc theo một cách nhất định bởi Beetsoft.Co, LTD. Kể từ sau khi thông báo, nếu bạn vẫn sử dụng BeeBlog sẽ được coi là đã đồng ý với những sửa đổi đó.</p>
                    <p>Phiên bản mới nhất của các khoản được giả định được áp dụng trong ưu tiên cho tất cả các phiên bản trước đó, và được đăng trên dịch vụ này là hợp lệ.</p>

                <h4>3. CÁC ĐIỀU KHOẢN TRONG VIỆC đăng kí & sử dụng dịch vụ</h4>

                    <p>Để truy cập vào Dịch Vụ chỉ có sẵn cho người dùng đăng ký, vì vậy những người muốn sử dụng dịch vụ phải chọn một ID và mật khẩu để đăng ký tài khoản
                    Khi bạn đăng ký sử dụng dịch vụ này, cam kết rằng mình có đủ tuổi pháp lý để tạo thành một hợp đồng ràng buộc và không phải là người bị hạn chế nhận các dịch vụ theo pháp luật của Việt Nam hoặc các phán quyết khác.
                    Ngoài ra bạn phải cung cấp cho chúng tôi một số thông tin theo yêu cầu của Công ty. Bạn phải đồng ý với các điều kiện sau đây có liên quan đến tài liệu cá nhân :</p>
                <p>a) cung cấp thông tin một cách trung thực, chính xác và đầy đủ về bản thân theo hướng dẫn trong mẫu đơn đăng ký sử dụng Dịch Vụ (“Đăng ký Dữ liệu”) và</p>
                <p>b) duy trì và cập nhật kịp thời Dữ Liệu Đăng Ký để giữ cho nó đúng, chính xác và đầy đủ.</p>
                <p>c) Nếu bạn cung cấp bất kỳ thông tin nào không đúng sự thật, không chính xác, không trung thực hoặc không đầy đủ, hoặc Beeblog có cơ sở hợp lý để nghi ngờ rằng thông tin đó là không đúng sự thật, không chính xác, không trung thực hoặc không đầy đủ, Beeblog có quyền đình chỉ hoặc chấm dứt tài khoản của bạn và từ chối bất kỳ và tất cả các sử dụng Dịch vụ (hoặc bất kỳ phần nào trong đó) hiện tại hoặc tương lai.</p>

                <h4>4. Tài khoản thành viên, mật khẩu và bảo mật</h4>
                <p>Bạn sẽ tạo một mật khẩu và tài khoản sau khi hoàn tất quá trình đăng ký sử dụng Dịch Vụ. Bạn có trách nhiệm giữ kín mật khẩu và tài khoản, và hoàn toàn chịu trách nhiệm cho tất cả các hoạt động diễn ra với mật khẩu hoặc tài khoản của bạn.</p>
                <p>Bạn đồng ý :</p>
                <p>a) thông báo ngay lập tức cho Personal Brand về bất kỳ việc sử dụng trái phép mật khẩu hoặc tài khoản của bạn hoặc bất kỳ vi phạm bảo mật.</p>
                <p>b) đảm bảo rằng bạn luôn thoát khỏi tài khoản của bạn vào cuối mỗi lần truy cập. Beeblog không thể và sẽ không chịu trách nhiệm cho bất kỳ tổn thất hoặc thiệt hại phát sinh từ việc bạn thất bại trong việc thực hiện theo mục 5 này.</p>

                <h4>5. Nội dung trên BeeBlog</h4>
                <p>Bạn hiểu rằng tất cả các thông tin, dữ liệu, văn bản, phần mềm, âm nhạc, âm thanh, hình ảnh, đồ họa, video, tin nhắn, tags hoặc các vật liệu khác, cho dù được đăng công khai hoặc được gửi riêng (“Nội dung”), là trách nhiệm duy nhất của con người chịu trách nhiệm cho Nội dung đó.</p>
                <p>Điều này có nghĩa là bạn, chứ không phải Beelblog, hoàn toàn chịu trách nhiệm về tất cả các nội dung mà bạn tải lên, đăng, truyền tải hoặc phương thức khác qua Dịch Vụ.</p>
                <p>Beelblog không kiểm soát nội dung đăng qua Dịch Vụ và, vì vậy, không bảo đảm tính chính xác, nguyên vẹn hoặc chất lượng của nội dung đó. Bạn hiểu rằng khi sử dụng Dịch vụ, bạn có thể tiếp xúc với Nội Dung gây khó chịu, không đứng đắn, hoặc gây mâu thuẫn.</p>
                <p>Trong mọi trường hợp thương hiệu cá nhân chịu trách nhiệm về bất kỳ nội dung bất kỳ, bao gồm, nhưng không giới hạn, bất kỳ lỗi hoặc sai sót trong nội dung bất kỳ, hoặc bất kỳ tổn thất hoặc thiệt hại của bất cứ loại nào phát sinh như là kết quả của việc sử dụng của bất kỳ nội dung được đăng, gửi qua email, truyền tải hoặc phương thức khác qua Dịch Vụ.</p>

                <b>Bạn đồng ý rằng, không sử dụng Dịch vụ cho mục đích:</b>

                <p>a) tải lên, đăng, truyền tải hoặc phương thức khác bất kỳ nội dung trái pháp luật, gây hại, đe dọa, lạm dụng, sách nhiễu, có hại, phỉ báng, khiếm nhã, khiêu dâm, bôi nhọ, xâm hại của người khác riêng tư, hận thù, hoặc phân biệt chủng tộc, dân tộc hoặc không thích hợp;</p>
                <p>b) gây hại cho trẻ vị thành niên dưới bất kỳ hình thức nào;</p>
                <p>c) mạo danh bất kỳ cá nhân hoặc tổ chức nào, bao gồm, nhưng không giới hạn, nhân viên quan chức của Beeblog, lãnh đạo diễn đàn, hướng dẫn hoặc máy chủ, hoặc tuyên truyền giả dối hoặc xuyên tạc tư cách của một người hoặc tổ chức;</p>
                <p>d) giả mạo tiêu đề hoặc sửa đổi các định dạng để che giấu nguồn gốc của bất kỳ nội dung nào truyền qua Dịch vụ;</p>
                <p>e) tải lên, đăng, truyền tải hoặc phương thức khác bất kỳ Nội dung mà bạn không có quyền cung cấp theo bất kỳ luật hoặc theo các mối quan hệ hợp đồng hoặc ủy thác nào (chẳng hạn như thông tin bên trong, độc quyền và thông tin bí mật được biết đến hoặc tiết lộ trong các mối quan hệ hoặc theo các thỏa thuận không được tiết lộ);</p>
                <p>f) tải lên, đăng, truyền tải hoặc bằng phương thức khác bất kỳ Nội Dung nào vi phạm các bằng sáng chế, thương hiệu, bí mật thương mại, bản quyền hoặc các quyền trí tuệ hoặc quyền sở hữu khác (“Quyền”) của bất kỳ bên nào;</p>
                <p>g) tải lên, đăng, truyền tải hoặc bằng phương thức khác bất kỳ các tài liệu chứa các vi rút phần mềm hoặc các mã số máy tính khác, các tập tin hoặc các chương trình được thiết kế để gây cản trở, phá hỏng hoặc hạn chế các chức năng của phần mềm, phần cứng hay thiết bị viễn thông;</p>
                <p>h) tải lên các tập tin, tài liệu có chứa hoặc có đường dẫn đến vi-rút hoặc bất cứ mã máy tính, tập tin hoặc chương trình nào khác để gây hại, cản trở hoặc hạn chế sự hoạt động bình thường của Beeblog và người dùng khác.</p>
                <p>h) làm gián đoạn dòng chảy bình thường của cuộc đối thoại, khiến màn hình “chạy” nhanh hơn so với người sử dụng Dịch Vụ khác có thể đánh máy, hoặc có hành động gây ảnh hưởng tiêu cực đến khả năng tham gia trong việc trao đổi của người sử dụng khác;</p>
                <p>i) cản trở hoặc phá rối Dịch Vụ hoặc các máy chủ hoặc mạng kết nối với Dịch Vụ, hoặc không tuân theo bất kỳ yêu cầu, thủ tục, chính sách hoặc quy định của mạng kết nối với Dịch vụ;</p>
                <p>j) cung cấp các tài liệu hỗ trợ hoặc các nguồn (hoặc che giấu hoặc ngụy trang bản chất, vị trí, nguồn gốc, hoặc sở hữu các tài liệu hỗ trợ hoặc nguồn) cho bất kỳ tổ chức nào theo chỉ định của Chính phủ Việt Nam là một tổ chức khủng bố nước ngoài căn cứ theo pháp luật hiện hành;</p>
                <p>k) “rình mò” hoặc quấy rối người khác</p>
                <p>l) thu thập hoặc lưu trữ dữ liệu cá nhân về người dùng khác trong kết nối với các hành vi bị cấm và các hoạt động được quy định tại các điều khoản vừa nói ở trên.</p>

                <h4>6. Không bán lại dịch vụ</h4>
                <p>Bạn đồng ý không sản sinh thêm, nhân bản, sao chap, bán, trao đổi, bán lại hoặc khai thác dưới bất kỳ mục đích thương mại nào bất cứ phần nào của Dịch vụ (bao gồm cả ID thành viên của bạn), sự sử dụng Dịch vụ, hoặc quyền truy cập Dịch vụ.</p>

                <h4>7. Trích dẫn Nội Dung trên Beeblog</h4>
                <p>Bạn có thể xem, chiết xuất thông tin trên Beeblog (in, tải, chuyển tiếp...) hoặc chia sẻ cho người khác nhưng chỉ cho mục đích cá nhân và phi thương mại với điều kiện phải trích dẫn đường link trực tiếp đến nội dung gốc, ghi rõ tên tác giả và nguồn tại Kipalog. Bạn nhận thức rằng mọi hành vi sao chép, trích dẫn, sửa đổi, phân phối, xuất bản, lưu thông... vì mục đích thương mại dưới mọi hình thức hoặc không ghi đầy đủ thông tin như trên là xâm hại quyền của chủ sở hữu nội dung, Kipalog có quyền yêu cầu bạn chấm dứt việc sử dụng dịch vụ và bồi thường thiệt hại(nếu có).</p>

                <h4>8. Gỡ bỏ Nội Dung trên Beeblog</h4>
                <p>Bạn xác nhận rằng Beeblog không chịu bất kỳ trách nhiệm hoặc nghĩa vụ nào theo bất kỳ cách nào về bất kỳ Nội Dung nào được những người khác cung cấp và không có nhiệm vụ phải kiểm tra trước các Nội Dung đó.</p>
                <p>Tuy nhiên, Kipalog bảo lưu quyền vào mọi thời điểm quyết định Nội Dung có phù hợp và tuân thủ với thỏa thuận này hay không, và có thể kiểm tra trước, di rời, từ chối, điều chỉnh và/hoặc gỡ bỏ Nội Dung vào bất kỳ lúc nào, mà không cần thông báo trước và theo toàn quyền quyết định của Beeblog, nếu Nội Dung đó được xem là vi phạm thỏa thuận này.</p>

                <h4>9. Thay đổi điều khoản</h4>
                <p>Điều khoản trong thoả thuận này có thể sửa đổi mà không thông báo trước. Các sửa đổi sẽ được thông báo trên trang web hoặc theo một cách nhất định bởi Beetsoft.Co, LTD. Kể từ sau khi thông báo, nếu bạn vẫn sử dụng BeeBlog sẽ được coi là đã đồng ý với những sửa đổi đó.</p>
                <p>Phiên bản mới nhất của các khoản được giả định được áp dụng trong ưu tiên cho tất cả các phiên bản trước đó, và được đăng trên dịch vụ này là hợp lệ.</p>
            </div>

        </article>

        <!-- END CONTENT -->
    </div>

    <!-- END CONTAINER -->
</div>
