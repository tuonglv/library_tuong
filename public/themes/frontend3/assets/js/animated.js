$(function() {
    $('a[href*=#section]').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
    });
});


$(function() {
    $('.to-top').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0}, 500, 'linear');
    });
});
