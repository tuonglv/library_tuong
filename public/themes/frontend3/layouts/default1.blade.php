<!DOCTYPE html>
<!-- saved from url=(0036)http://keysoft-wp.themetek.com/demo/ -->
<html lang="en" class=" js no-touch">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BeetSoft ..CO - Solution </title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="../themes/frontend3/assets/css/default.css">
    <link rel="stylesheet" type="text/css" href="../themes/frontend3/assets/css/component.css">
    <link rel="stylesheet" type="text/css" href="../themes/frontend3/assets/css/animated.css">
    {{--<link rel="stylesheet" type="text/css" href="{!! url('/themes/frontend3/assets/css/default.css') !!}">--}}
    {{--<link rel="stylesheet" type="text/css" href="{!! url('/themes/frontend3/assets/css/component.css') !!}">--}}
    {{--<link rel="stylesheet" type="text/css" href="{!! url('/themes/frontend3/assets/css/animated.css') !!}">--}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script async="" src="../themes/frontend3/assets/js/analytics.js"></script>
    <script src="../themes/frontend3/assets/js/modernizr.custom.js"></script>
    <script src="../themes/frontend3/assets/js/animated.js"></script>
    {{--<script async="" src="{!! url('/themes/frontend3/assets/js/analytics.js') !!}"></script>--}}
    {{--<script src="{!! url('/themes/frontend3/assets/js/modernizr.custom.js') !!}"></script>--}}
    {{--<script src="{!! url('themes/frontend3/assets/js/animated.js') !!}/"></script>--}}
</head>
<body>
<div class="container">

    <div class="img_top demo" id="section07">
        <a href="#section08"><span></span><span></span><span></span>Scroll</a>
    </div>

    <div class="gray red" id="section08">
        <h2>BSER</h2>
        <ul class="grid cs-style-3">
            <li>
                <a href="{!! URL::to('/library/post/category/cau-chuyen-hay') !!}">
                    <figure>
                        <img src="{!! url('files/images/cau_chuyen_hay.jpg') !!}" alt="img04"
                             style="height: 213px;">
                        <figcaption>
                            <h3>CÂU CHUYỆN HAY</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="{!! URL::to('/library/post/category/goc-cong-ty') !!}">
                    <figure>
                        <img src="{!! url('files/images/bs.jpg') !!}" alt="img04">
                        <figcaption>
                            <h3>GÓC CÔNG TY</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="{!! URL::to('/library/post/category/phong-truyen-thong') !!}">
                    <figure>
                        <img src="{!! url('files/images/phong_truyen_thong.png') !!}" alt="img04">
                        <figcaption>
                            <h3>PHÒNG TRUYỀN THỐNG</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
        </ul>
    </div>
    <div class="white blue">
        <h2>CÔNG NGHỆ</h2>
        <ul class="grid cs-style-3">
            <li>
                <a href="{!! URL::to('/library/post/category/cong-nghe-moi') !!}">
                    <figure>
                        <img src="{!! url('files/images/new-Technology.jpg')!!}" alt="img04">
                        <figcaption>
                            <h3>CÔNG NGHỆ MỚI</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="{!! URL::to('/library/post/category/tools') !!}">
                    <figure>
                        <img src="{!! url('files/images/webdevelopertools_zip.jpg') !!}" alt="img04">
                        <figcaption>
                            <h3>TOOLS</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="{!! URL::to('/library/post/category/lib') !!}">
                    <figure>
                        <img src="{!! url('files/images/VM_Xplatform.png') !!}" alt="img04">
                        <figcaption>
                            <h3>LIB</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
        </ul>

    </div>
    <div class="gray green">
        <h2><span>NGOẠI NGỮ</span></h2>
        <ul class="grid cs-style-3">
            <li>
                <a href="{!! URL::to('/library/post/category/tieng-anh') !!}">
                    <figure>
                        <img src="{!! url('files/images/category_english.jpg') !!}" alt="img04">
                        <figcaption>
                            <h3>TIẾNG ANH</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="{!! URL::to('/library/post/category/tieng-nhat') !!}">
                    <figure>
                        <img src="{!! url('files/images/category_japan.jpg') !!}" alt="img04">
                        <figcaption>
                            <h3>TIẾNG NHẬT</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="{!! URL::to('/library/post/category/ngoai-ngu-khac') !!}">
                    <figure>
                        <img src="{!! url('files/images/ngoai_ngu_khac.jpg') !!}" alt="img04">
                        <figcaption>
                            <h3>NGOẠI NGỮ KHÁC</h3>
                        </figcaption>
                    </figure>
                </a>
            </li>
        </ul>
    </div>
</div>

<!-- /container -->
<footer>
    <div id="footer-copyright" style="text-align: center; font-size: 12px;">
        <div class="container">
            <span class="left">Beetsoft Co., LTD - All Rights Reserved</span>
            {{--<a href="#" class="to-top"> Back to top <i class="fa fa-angle-double-up"></i></a>--}}
        </div>
    </div>
</footer>

{{--<script>--}}
{{--(function (i, s, o, g, r, a, m) {--}}
{{--i['GoogleAnalyticsObject'] = r;--}}
{{--i[r] = i[r] || function () {--}}
{{--(i[r].q = i[r].q || []).push(arguments)--}}
{{--}, i[r].l = 1 * new Date();--}}
{{--a = s.createElement(o),--}}
{{--m = s.getElementsByTagName(o)[0];--}}
{{--a.async = 1;--}}
{{--a.src = g;--}}
{{--m.parentNode.insertBefore(a, m)--}}
{{--})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');--}}

{{--ga('create', 'UA-73199302-2', 'auto');--}}
{{--ga('send', 'pageview');--}}

{{--</script>--}}
<script src="../themes/frontend3/assets/js/toucheffects.js"></script>
{{--<script src="{!! url('/themes/frontend3/assets/js/toucheffects.js') !!}"></script>--}}


</body>
</html>