<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => [
        'before'             => function ($theme) {
            $theme->setTitle('Blogger');
        },
        'beforeRenderTheme'  => function ($theme) {
            $theme->asset()->add('styles', url('themes/frontend2/assets/css/styles.css'), ['core-css']);
            $theme->asset()->add('contact-form', url('themes/frontend2/assets/css/contact-form.css'), ['core-css']);
            $theme->asset()->add('jetpack', url('themes/frontend2/assets/css/jetpack.css'), ['core-css']);
            $theme->asset()->add('bxslider', url('themes/frontend2/assets/css/jquery.bxslider.css'), ['core-css']);
            $theme->asset()->add('responsive', url('themes/frontend2/assets/css/responsive.css'), ['core-css']);
            $theme->asset()->add('slicknav', url('themes/frontend2/assets/css/slicknav.css'), ['core-css']);
            $theme->asset()->add('instag-slider', url('themes/frontend2/assets/css/instag-slider.css'), ['core-css']);
            $theme->asset()->add('font-awesome', url('assets/plugins/font-awesome/css/font-awesome.min.css'), ['core-css']);

            $theme->asset()->add('jquery', url('assets/plugins/jquery/jquery-2.1.4.min.js'), ['core-js']);
        },
        'beforeRenderLayout' => [
            'default' => function ($theme) {
                $theme->asset()->container('footer')->add('jquery-form', url('assets/plugins/jquery-form/jquery.form.js'), ['core-js']);
                $theme->asset()->container('footer')->add('scripts', url('themes/frontend2/assets/js/scripts.js'), ['core-js']);
                $theme->asset()->container('footer')->add('jquery-slicknav', url('assets/plugins/slicknav/dist/jquery.slicknav.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('jquery-bxslider', url('assets/plugins/bxslider/dist/jquery.bxslider.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('fitvids', url('assets/plugins/fitvids/dist/fitvids.js'), ['core-js']);
                $theme->asset()->container('footer')->add('retina', url('assets/plugins/retina/retina.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('embed', url('themes/frontend2/assets/js/embed.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('solopine', url('themes/frontend2/assets/js/solopine.js'), ['core-js']);
            }

        ]

    ]

];