{{--BEGIN TOP-BAR--}}
<div id="top-bar">

    <div class="container">

        <div id="navigation-wrapper">
            <ul id="menu-main-menu" class="menu">
                <li id="menu-item-19"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-has-children menu-item-19">
                    <a href="{!! URL::to('/') !!}">TRANG CHỦ</a></li>
                <li id="menu-item-94"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-94">
                    <a href="{!! URL::to('/library/post/category/phong-truyen-thong') !!}">PHÒNG TRUYỀN THỐNG</a>
                </li>
                {{--<li id="menu-item-84" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-84"><a--}}
                {{--href="/library/about-me">About Me</a></li>--}}
                <li id="menu-item-88" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-88"><a
                            href="/library/post/category/cong-nghe-moi">CÔNG NGHỆ MỚI</a></li>
                <li id="menu-item-21" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-21">
                <a href="/library/post/category/tools">TOOLS</a></li>
                <li id="menu-item-21" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-21">
                <a href="/library/post/category/lib">LIB</a></li>
                <li id="menu-item-21" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-21">
                <a href="/library/post/category/tieng-nhat">TIẾNG NHẬT</a></li>
                <li id="menu-item-21" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-21">
                    <a href="#">KHÁC</a>
                    <ul class="sub-menu">
                        <li id="menu-item-103"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-103"><a
                                    href="{!! URL::to('/library/post/category/tieng-anh') !!}">TIẾNG ANH</a></li>
                        <li id="menu-item-103"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-103"><a
                                    href="{!! URL::to('/library/post/category/ngon-ngu-khac') !!}">NGÔN NGỮ KHÁC</a></li>
                        <li id="menu-item-103"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-103"><a
                                    href="{!! URL::to('/library/post/category/cau-chuyen-hay') !!}">CÂU CHUYỆN HAY</a></li>
                        <li id="menu-item-103"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-103"><a
                                    href="{!! URL::to('/library/post/category/goc-cong-ty') !!}">GÓC CÔNG TY</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="menu-mobile"></div>

        <div class="btn_login">
            @if(auth()->user())
                <a href="/library/admin">{!!  trans('core::frontend.hello').auth()->user()['username'] !!}</a>
            @else
                <a href="/library/login">{!! trans('core::frontend.login') !!}</a>
            @endif

            @if(auth()->user())
                    <a href="{!! route('fr_logout') !!}">{!! trans('core::frontend.logout') !!}</a>
            @else
                <a href="/library/register">{!! trans('core::frontend.register') !!}</a>
            @endif

        </div>
        <div id="top-social">
        </div>
    </div>

</div>
{{--END TOP-BAR--}}
<header id="header">

    <div class="container">

            {{--search form --}}
            @if(!isset($search_form_status))
                <div id="logo">
                    <div class="row wrap_search">
                        <div class="col-md-12 col-xs-12 col-sm-12 input-group">
                            <form role="search" method="get" id="searchform" action="{!! route('post.search') !!}">
                                <div>
                                    <input type="text" placeholder="Search title post ..." name="title" id="s" class="input_search"/>
                                    <button type="submit" class="btn btn-default btn_search">Search</button>
                                </div>
                            </form>
                        </div><!-- /input-group -->
                    </div><!-- /.row -->
                </div>
            @endif


    </div>

</header>