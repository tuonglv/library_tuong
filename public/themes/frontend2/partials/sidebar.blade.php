@if(isset($sidebar_status) && $sidebar_status)
    <aside id="sidebar">
        {{--<div id="solopine_about_widget-2" class="widget solopine_about_widget">--}}
            {{--<h4 class="widget-heading">--}}
                {{--<span>About Me</span>--}}
            {{--</h4>--}}
            {{--<div class="about-widget">--}}

                {{--<a href="{!! URL::to('/about-me') !!}"><img--}}
                            {{--src="http://solopine.com/florence/wp-content/uploads/2014/11/abou3.jpg" alt="About Me"/></a>--}}

                {{--<p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack--}}
                    {{--nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me..</p>--}}

            {{--</div>--}}

        {{--</div>--}}

        {{--<div id="solopine_social_widget-2" class="widget solopine_social_widget"><h4 class="widget-heading"><span>Subscribe &#038; Follow</span>--}}
            {{--</h4>--}}
            {{--<div class="widget-social">--}}
                {{--tuonglv--}}
                {{--<a target="_blank"--}}
                   {{--href="https://www.facebook.com/sharer/sharer.php?u={!! isset($post_popular['slug']) ? URL::to('/library/post/detail/').'/'.$post_popular['slug'].'/' : '' !!}"><i--}}
                            {{--class="fa fa-facebook"></i></a>--}}
                {{--<a target="_blank"--}}
                   {{--href="https://twitter.com/home?status=Check%20out%20this%20article:%20Floral Wallpaper%20-%20{!! isset($post_popular['slug']) ? URL::to('/library/post/detail/').'/'.$post_popular['slug'] : '' !!}"><i--}}
                            {{--class="fa fa-twitter"></i></a>--}}
                {{--<a href="http://instagram.com/solopine" target="_blank"><i class="fa fa-instagram"></i></a>--}}
                {{--<a target="_blank"--}}
                   {{--href="https://pinterest.com/pin/create/button/?url={!! isset($post_popular['slug']) ? URL::to('/library/post/detail/').'/'.$post_popular['slug'] : '' !!}&amp;media={!! isset($post_popular['image']['url']) ? URL::to('/library/post/detail/').$post_popular['image']['url'] : '' !!}&amp;description={!! isset($post_popular['title']) ? $post_popular['title'] : '' !!}"><i--}}
                            {{--class="fa fa-pinterest"></i></a>--}}
                {{--<a href="http://bloglovin.com/#" target="_blank"><i class="fa fa-heart"></i></a>--}}
                {{--<a target="_blank"--}}
                   {{--href="https://plus.google.com/share?url={!! isset($post_popular['slug']) ? URL::to('/library/post/detail/').'/'.$post_popular['slug'] : '' !!}"><i--}}
                            {{--class="fa fa-google-plus"></i></a>--}}
                {{--<a href="http://#.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a>--}}
                {{--<a href="#" target="_blank"><i class="fa fa-rss"></i></a></div>--}}
        {{--</div>--}}

        <div id="solopine_latest_news_widget-2" class="widget solopine_latest_news_widget"><h4
                    class="widget-heading"><span>Latest Posts</span></h4>
            <ul class="side-newsfeed">
                @if(isset($post_tops) && $post_tops)
                    @foreach($post_tops as $post_top)
                        <li>
                            <div class="side-item">
                                <div class="side-image">
                                    <a href="{!! isset($post_top['slug']) && $post_top['slug'] ? URL::to('/library/post/detail\/').$post_top['slug'] : '#' !!}"
                                       rel="bookmark"><img width="500"
                                                           height="380"
                                                           src="{!! isset($post_top['image']['url']) && $post_top['image']['url'] ? $post_top['image']['url'] : '/files/images/no-image.png' !!}"
                                                           class="side-item-thumb wp-post-image"
                                                           alt="{!! isset($post_top['title']) ? $post_top['title'] : ''  !!}"/></a>
                                </div>
                                <div class="side-item-text">
                                    <h4>
                                        <a href="{!! isset($post_top['slug']) && $post_top['slug'] ? URL::to('/library/post/detail\/').$post_top['slug'] : '#' !!}"
                                           rel="bookmark">{!! isset($post_top['title']) ? $post_top['title'] : ''  !!}</a>
                                    </h4>
                                    <span class="side-item-meta">{!! isset($post_popular['updated_at']) ? $post_popular['updated_at'] : '' !!}</span>
                                </div>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>

        {{--<div id="text-2" class="widget widget_text">--}}
            {{--<div class="textwidget"><img--}}
                        {{--src="http://solopine.com/florence/wp-content/uploads/2014/11/bannerspot2.png" alt="banner"/>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div id="tag_cloud-2" class="widget widget_tag_cloud"><h4 class="widget-heading"><span>Tagcloud</span></h4>
            <div class="tagcloud">
                @if(isset($tags) && $tags)
                    @foreach($tags as $tag)
                        <a href='{!! isset($tag['id']) ? URL::to('/library/post/tag\/').$tag['id'] : '#' !!}'
                           class='tag-link-4 tag-link-position-1' title='1 topic'
                           style='font-size: 8pt;'>{!! isset($tag['name']) ? $tag['name'] : '' !!} </a>
                    @endforeach
                @endif
            </div>
        </div>
    </aside>
@endif