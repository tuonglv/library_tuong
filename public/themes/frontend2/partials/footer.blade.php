<div class="scrollTop">
    <a href="#" class="scrollup">Scroll</a>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 750) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });

        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
    });
</script>
<style>
    .scrollup {
        width: 40px;
        height: 40px;
        position: fixed;
        bottom: 90px;
        right: 100px;
        display: none;
        text-indent: -9999px;
        background: url('../../../files/images/icon_top.png') no-repeat;
        background-size: 100%;
    }
</style>

{{--FOOTER --}}

{{--<footer id="footer">--}}
    {{--<div class="fh5co-footer-style-2">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-6 col-md-push-6 footer-style-2-link">--}}
                    {{--<ul>--}}
                        {{--<li><a href="/library/rules/">Điều Khoản</a></li>--}}
                        {{--<li><a href="/library/author/1">Giới thiệu</a></li>--}}
                        {{--<li><a href="/library/contact-me/">Liên Hệ</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div id="footer-copyright">--}}
        {{--<div class="container">--}}
            {{--<span class="left">Copyright@ 2016 Beetsoft Co., LTD</span>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</footer>--}}
<footer id="footer">
    <div class="row col-md-12 col-xs-12 col-sm-12">
        <div class="copyright col-md-4 col-xs-4 col-sm-4 col-md-offset-2">
            <span class="left">Copyright©Beetsoft Co., LTD</span>
        </div>

        <div class="col-md-6 col-xs-6 col-sm-6">
            <ul>
                <li><a href="http://blog.beetsoft.com.vn:8008/library/TOS/">Điều Khoản Sử Dụng</a></li>
                <!--<li><a href="http://blog.beetsoft.com.vn:8008/library/author/1">Giới thiệu</a></li>-->
                <li><a href="http://blog.beetsoft.com.vn:8008/library/contact/">Phản Hồi</a></li>
                <li><a href="https://www.facebook.com/groups/257713851081977/?fref=ts" target="_blank">FanPage</a></li>
            </ul>
        </div>
    </div>
</footer>
