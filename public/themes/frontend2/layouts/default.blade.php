<!DOCTYPE html>
<html lang="en-US">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../../files/images/blue-logo.png">
    <title>BEETSOFT - PASSIONATE - FUNNY - CREATIVE - CRAZY</title>

    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/solopine.com\/florence\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.5.2"}};
        !function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;if(!g||!g.fillText)return!1;switch(g.textBaseline="top",g.font="600 32px Arial",a){case"flag":return g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3;case"diversity":return g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,d=c[0]+","+c[1]+","+c[2]+","+c[3],g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e;case"simple":return g.fillText(h(55357,56835),0,0),0!==g.getImageData(16,16,1,1).data[0];case"unicode8":return g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
            <!-- BEGIN PAGE STYLE -->
    {!! Theme::asset()->styles() !!}
            <!-- END PAGE STYLE -->
    {!! Theme::asset()->scripts() !!}
    <link rel='stylesheet' id='default_headings_font-css'  href='http://fonts.googleapis.com/css?family=Oswald%3A400%2C700&#038;ver=4.5.2' type='text/css' media='all' />
    <link rel='stylesheet' id='default_para_font-css'  href='http://fonts.googleapis.com/css?family=Crimson+Text%3A400%2C700%2C400italic%2C700italic&#038;ver=4.5.2' type='text/css' media='all' />
    <link rel='stylesheet' id='default_body_font-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C700italic%2C400%2C700&#038;subset=cyrillic%2Clatin&#038;ver=4.5.2' type='text/css' media='all' />
    <script type='text/javascript' src='http://solopine.com/florence/wp-includes/js/jquery/jquery.js?ver=1.12.3'></script>
    <script type='text/javascript' src='http://solopine.com/florence/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.0'></script>
    <script type='text/javascript' src='http://solopine.com/florence/wp-content/plugins/bwp-minify/min/?f=florence/wp-content/plugins/instagram-slider-widget/assets/js/jquery.flexslider-min.js'></script>
    <link rel='https://api.w.org/' href='http://solopine.com/florence/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://solopine.com/florence/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://solopine.com/florence/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.5.2" />
    <style type="text/css">

        #logo { padding:60px 0 50px; }

        #top-bar, .slicknav_menu { background:#ffffff; }		.menu li a, .slicknav_nav a { color:#777777; }		.menu li.current-menu-item a, .menu li.current_page_item a, .menu li a:hover {  color:#EF9D87; }
        .slicknav_nav a:hover { color:#EF9D87; background:none; }

        .menu .sub-menu, .menu .children { background: #ffffff; }
        ul.menu ul a, .menu ul ul a { border-top: 1px solid #f4f4f4; color:#999999; }
        ul.menu ul a:hover, .menu ul ul a:hover { color: #ffffff; background:#EF9D87; }

        #top-social a i { color:#c2c2c2; }
        #top-social a:hover i { color:#EF9D87 }

        #top-search a { background:#EF9D87 }
        #top-search a { color:#ffffff }

        #footer-instagram { background:#ffffff; }
        #footer-instagram h4.block-heading { color:#000000; }

        #footer-social { background:#EF9D87; }
        #footer-social a i { color:#EF9D87; background:#ffffff; }
        #footer-social a { color:#ffffff; }

        #footer-copyright { color:#999999; background:#ffffff;  }

        .widget-heading { color:#161616; }
        .widget-heading > span:before, .widget-heading > span:after { border-color: #d8d8d8; }

        .widget-social a i { color:#ffffff; background:#EF9D87; }

        a, .author-content a.author-social:hover { color:#EF9D87; }
        .more-button:hover, .post-share a i:hover, .post-pagination a:hover, .pagination a:hover, .widget .tagcloud a { background:#EF9D87; }
        .more-button:hover, .post-share a i:hover { border-color:#EF9D87;  }
        .post-entry blockquote p { border-left:3px solid #EF9D87; }

        /* Custom CSS for WP Instagram Widget plugin */
        #footer-instagram .instagram-pics li {
            width:12.5%;
            display:inline-block;
        }
        #footer-instagram .instagram-pics li img {
            max-width:100%;
            height:auto;
            vertical-align:middle;
        }
        #sidebar .instagram-pics li {
            width:32%;
            display:inline-block;
            border-bottom:none;
            padding:0;
            margin:0;
            margin-right:2%;
        }
        #sidebar .instagram-pics li:nth-of-type(3n+3) {
            margin-right:0;
        }
    </style>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-57133010-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body class="archive category category-travel category-12">

        <!-- HEADER AND TOP-BAR-->
{!! theme()->partial('header') !!}

{{--CONTENT--}}
{{--{!! theme()->partial('content') !!}--}}
<div class="container wrapper_content">
    <div id="content">
        {!! Theme::content() !!}
        {!! Theme::partial('sidebar') !!}
    </div>
    <!-- END CONTAINER -->
</div>
<!-- FOOTER-->
{!! theme()->partial('footer') !!}

<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"loaderUrl":"http:\/\/solopine.com\/florence\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ...","cached":"1"};
    /* ]]> */
</script>
<!-- LIBRARY JS-->
{!! Theme::asset()->container('footer')->scripts() !!}
        <!-- MAIN JS-->
{!! Theme::asset()->container('footer-scripts')->scripts() !!}

</body>

</html>
