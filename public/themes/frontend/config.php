<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => [
        'before'             => function ($theme) {
            // You can remove this line anytime.
            $theme->setTitle('Copyright ©  2013 - Laravel.in.th');
        },
        'beforeRenderTheme'  => function ($theme) {
                //$theme->asset()->add('style', url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900'), ['core-css']);
                //$theme->asset()->add('style', url('https://fonts.googleapis.com/css?family=PT+Serif:300,400,700,900'), ['core-css']);
                $theme->asset()->add('font-awesome', url('themes/frontend/assets/font/font-icon/font-awesome/css/font-awesome.css'), ['core-css']);
                $theme->asset()->add('weather-font', url('themes/frontend/assets/font/font-icon/weather-fonts/weather-font.css'), ['core-css']);
                $theme->asset()->add('bootstrap', url('themes/frontend/assets/libs/bootstrap/css/bootstrap.min.css'), ['core-css']);
                $theme->asset()->add('slick', url('themes/frontend/assets/libs/slick-slider/slick.css'), ['core-css']);
                $theme->asset()->add('slick-theme', url('themes/frontend/assets/libs/slick-slider/slick-theme.css'), ['core-css']);
                $theme->asset()->add('jquery-mCustomScrollbar', url('themes/frontend/assets/libs/custom-scroll/jquery.mCustomScrollbar.min.css'), ['core-css']);
                $theme->asset()->add('owl-carousel', url('themes/frontend/assets/libs/owl-carousel/assets/owl.carousel.css'), ['core-css']);
                $theme->asset()->add('layout', url('themes/frontend/assets/css/layout.css'), ['core-css']);
                $theme->asset()->add('components', url('themes/frontend/assets/css/components.css'), ['core-css']);
                $theme->asset()->add('responsive', url('themes/frontend/assets/css/responsive.css'), ['core-css']);

                $theme->asset()->add('jquery', url('themes/frontend/assets/libs/jquery/jquery-2.2.3.min.js'), ['core-js']);
        },
        'beforeRenderLayout' => [
            'default' => function ($theme) {
                $theme->asset()->container('footer')->add('bootstrap', url('themes/frontend/assets/libs/bootstrap/js/bootstrap.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('jquery-smoothscroll', url('themes/frontend/assets/libs/smooth-scroll/jquery-smoothscroll.js'), ['core-js']);
                $theme->asset()->container('footer')->add('slick', url('themes/frontend/assets/libs/slick-slider/slick.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('jquery-easing', url('themes/frontend/assets/libs/easy-ticker/jquery.easing.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('jquery-easy-ticker', url('themes/frontend/assets/libs/easy-ticker/jquery.easy-ticker.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('jquery-mCustomScrollbar', url('themes/frontend/assets/libs/custom-scroll/jquery.mCustomScrollbar.concat.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('isotope', url('themes/frontend/assets/libs/isotope/isotope.pkgd.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('fit-columns', url('themes/frontend/assets/libs/isotope/fit-columns.js'), ['core-js']);
                $theme->asset()->container('footer')->add('owl-carousel', url('themes/frontend/assets/libs/owl-carousel/owl.carousel.min.js'), ['core-js']);
                $theme->asset()->container('footer')->add('jquery-simpleWeather', url('themes/frontend/assets/libs/weather/jquery.simpleWeather.min.js'), ['core-js']);
            }
        ]

    ]

];