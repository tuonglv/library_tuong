<!DOCTYPE html>
<html lang="en">

<head>
    <title>Directnews | Home Default</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- BEGIN PAGE STYLE -->
    {!! Theme::asset()->styles() !!}
            <!-- END PAGE STYLE -->
    {!! Theme::asset()->scripts() !!}
</head>

<body>
<!--BEGIN PAGE LOADER-->
<div class="body-wrapper">
    <!-- MENU MOBILE-->
    <!-- WRAPPER CONTENT-->
    <div class="wrapper-content">
        <!-- HEADER-->
        {!! theme()->partial('header') !!}
        <!-- MAIN CONTENT-->
        <div class="main-content">
            <!-- BLOCK MAIN-->
            {!! Theme::content() !!}
        </div>
        <!-- FOOTER-->
        {!! theme()->partial('footer') !!}
        <!-- BUTTON BACK TO TOP-->
        <div class="btn-wrapper back-to-top"><a href="" class="btn btn-transparent"><i class="fa fa-angle-double-up"></i></a></div>
    </div>
</div>
<!-- LIBRARY JS-->
{!! Theme::asset()->container('footer')->scripts() !!}
<!-- MAIN JS-->
{!! Theme::asset()->container('footer-scripts')->scripts() !!}
<script src="themes/frontend/assets/js/main.js">
</script>
<!-- LOADING SCRIPTS FOR PAGE-->
{{--<script src="themes/frontend/assets/js/pages/news.js"></script>--}}
{{--<script src="themes/frontend/assets/js/pages/carousel-slide.js"></script>--}}
{{--<script src="themes/frontend/assets/libs/chart/chart.min.js"></script>--}}
{{--<script src="themes/frontend/assets/js/pages/chart.js"></script>--}}
</body>

</html>