jQuery(function($) {
    "use strict";

    var SLZ = window.SLZ || {};

    /*=======================================
    =             MAIN FUNCTION             =
    =======================================*/
    SLZ.mainFunction = function() {

        // slider category sport
        $('.category-sport-list').slick({
            infinite: false,
            speed: 400,
            slidesToShow: 9,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 8
                    }
                },
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 6
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 415,
                    settings: {
                        slidesToShow: 3
                    }
                }
            ]
        });

        // slider for logo
        $('.slide-logo-list').slick({
            infinite: true,
            speed: 400,
            slidesToShow: 5,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 5000,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 415,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    };

    /*======================================
    =            INIT FUNCTIONS            =
    ======================================*/

    $(document).ready(function() {
        SLZ.mainFunction();
    });

    /*=====  End of INIT FUNCTIONS  ======*/
});
