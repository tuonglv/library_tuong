<header>
    <div class="header-main news">
        <div class="header-middle fixed-header">
            <div class="container">
                <div class="header-middle-wrapper">
                    <div class="wrapper-logo-menu">
                        <div class="hamburger-menu"><i class="icons fa fa-bars"></i><i class="icons-2 fa fa-long-arrow-up"></i></div>
                        <div class="logo-wrapper">
                            <a href="index.html" class="logo"><img src="themes/frontend/assets/img/logo/logo-news.png" alt="" class="img-responsive" /></a>
                        </div>
                    </div>
                    <ul class="sub-navigation list-inline">
                        <li><a href="index.html#" class="link"><span class="title">latest</span><span class="description">Post this hour</span></a></li>
                        <li><a href="index.html#" class="link"><span class="title">popular</span><span class="description">Featured this time</span></a></li>
                        <li><a href="index.html#" class="link"><span class="title">list</span><span class="description">News subjects</span></a></li>
                        <li><a href="index.html#" class="link"><span class="title">video</span><span class="description">News by video</span></a></li>
                    </ul>
                    <div class="more-infomation">
                        <div id="weather"></div>
                        <div class="language"><a href="javascript:void(0)" class="dropdown-text"><span>Edition: uS</span><i class="topbar-icon icons-dropdown fa fa-angle-down"></i></a>
                            <ul class="dropdown-language list-unstyled hide">
                                <li><a href="index.html#" class="link">US</a></li>
                                <li><a href="index.html#" class="link">France</a></li>
                                <li><a href="index.html#" class="link">Spain</a></li>
                            </ul>
                        </div>
                        <div class="button-search"><i class="icons fa fa-search"></i>
                            <div class="nav-search hide">
                                <form><input type="text" placeholder="Search" class="searchbox" /><button type="submit" class="searchbutton fa fa-search"></button></form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mega-menu">
                <div class="container">
                    <div class="mega-menu-wrapper padding-top-100 padding-bottom-100">
                        <div class="row">
                            <div class="col-md-2 col-sm-4 col-xs-4 news">
                                <div class="catelory-widget"><a href="index.html" class="title-topic news">Page</a>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="about-us.html" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">about us</span></a></li>
                                            <li><a href="category-1col.html" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">category 1 col</span></a></li>
                                            <li><a href="category-2col.html" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">category 2 col</span></a></li>
                                            <li><a href="blog-list.html" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">blog list</span></a></li>
                                            <li><a href="author-list.html" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">author list</span></a></li>
                                            <li><a href="faq.html" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">faq</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-4 sport">
                                <div class="catelory-widget"><a href="sport.html" class="title-topic">Sport</a>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Football</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Olympic</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Photo Store</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Video</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Stars</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Live Score</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-4 travel">
                                <div class="catelory-widget"><a href="travel.html" class="title-topic">travel</a>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Flight</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Experience</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Destination</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Travel Blog</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Tour Guide</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Video</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-4 tech">
                                <div class="catelory-widget"><a href="tech.html" class="title-topic">tech</a>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Football</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Olympic</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Photo Store</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Video</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Stars</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Live Score</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-4 lifestyle">
                                <div class="catelory-widget"><a href="lifeStyle.html" class="title-topic">LifeStyle</a>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Flight</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Experience</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Destination</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Travel Blog</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Tour Guide</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">Video</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-4 entertainment">
                                <div class="catelory-widget"><a href="entertainment.html" class="title-topic">Entertainment</a>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">stars</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">story</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">entertainment & life</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">show</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">recent events</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">photo store</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="slide-news-wrapper-2 margin-top-30">
                                    <div class="title-topic news">Featured</div>
                                    <div class="slide-4-wrapper">
                                        <div class="item">
                                            <div class="news-layout-3 small"><a href="index.html#" class="news-image"><span class="mask-gradient style-2"></span><img src="themes/frontend/assets/img/entertainment/news-1.jpg" alt="" class="img-responsive"/></a>
                                                <div class="news-content"><a href="index.html#" class="title">U.S. Stocks Close Sharply Lower On New China</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="news-layout-3 small"><a href="index.html#" class="news-image"><span class="mask-gradient style-9"></span><img src="themes/frontend/assets/img/entertainment/news-2.jpg" alt="" class="img-responsive"/></a>
                                                <div class="news-content"><a href="index.html#" class="title">U.S. Stocks Close Sharply Lower On New China</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="news-layout-3 small"><a href="index.html#" class="news-image"><span class="mask-gradient style-7"></span><img src="themes/frontend/assets/img/entertainment/news-3.jpg" alt="" class="img-responsive"/></a>
                                                <div class="news-content"><a href="index.html#" class="title">U.S. Stocks Close Sharply Lower On New China</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="news-layout-3 small"><a href="index.html#" class="news-image"><span class="mask-gradient style-10"></span><img src="themes/frontend/assets/img/entertainment/news-4.jpg" alt="" class="img-responsive"/></a>
                                                <div class="news-content"><a href="index.html#" class="title">U.S. Stocks Close Sharply Lower On New China</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="news-layout-3 small"><a href="index.html#" class="news-image"><span class="mask-gradient style-3"></span><img src="themes/frontend/assets/img/entertainment/news-3.jpg" alt="" class="img-responsive"/></a>
                                                <div class="news-content"><a href="index.html#" class="title">U.S. Stocks Close Sharply Lower On New China</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="news-layout-3 small"><a href="index.html#" class="news-image"><span class="mask-gradient style-6"></span><img src="themes/frontend/assets/img/entertainment/news-4.jpg" alt="" class="img-responsive"/></a>
                                                <div class="news-content"><a href="index.html#" class="title">U.S. Stocks Close Sharply Lower On New China</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">May 15, 2016</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="breaking-news">
                    <div class="text">breaking news:</div>
                    <div class="wrapper-news">
                        <div class="vticker">
                            <ul class="list-unstyled">
                                <li>
                                    <a href="blog-single.html" class="link"> <span class="text">'The Division' Open Beta Starts Tomorrow On Xbox One</span><span class="btn-read"><i class="icons fa fa-long-arrow-right"></i>read story</span></a>
                                </li>
                                <li>
                                    <a href="blog-single.html" class="link"> <span class="text">The world's most eco-friendly cities that you must go to</span><span class="btn-read"><i class="icons fa fa-long-arrow-right"></i>read story</span></a>
                                </li>
                                <li>
                                    <a href="blog-single.html" class="link"> <span class="text">How much does childbirth cost around the world?</span><span class="btn-read"><i class="icons fa fa-long-arrow-right"></i>read story</span></a>
                                </li>
                                <li>
                                    <a href="blog-single.html" class="link"> <span class="text">Apple wine makes human more energy for winter</span><span class="btn-read"><i class="icons fa fa-long-arrow-right"></i>read story</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="btn-close-breaking-news"><i class="icons fa fa-times"></i></div>
                </div>
            </div>
        </div>
    </div>
</header>