<footer>
    <div class="footer-main news">
        <div class="container">
            <div class="footer-top">
                <div class="logo-wrapper">
                    <a href="index.html" class="logo"><img src="themes/frontend/assets/img/logo/logo-news.png" alt="" /><span class="text">The Template for News - Magazine</span></a>
                </div>
                <nav class="navigation">
                    <ul class="nav-links nav navbar-nav">
                        <li class="active"><a href="index.html" class="main-menu"><span class="text">News</span></a></li>
                        <li><a href="business.html" class="main-menu"><span class="text">Business</span></a></li>
                        <li><a href="travel.html" class="main-menu"><span class="text">Travel</span></a></li>
                        <li><a href="entertainment.html" class="main-menu"><span class="text">Entertaiment</span></a></li>
                        <li><a href="tech.html" class="main-menu"><span class="text">Tech</span></a></li>
                        <li><a href="sport.html" class="main-menu"><span class="text">Sport</span></a></li>
                        <li><a href="lifestyle.html" class="main-menu"><span class="text">Lifestyle</span></a></li>
                        <li><a href="video.html" class="main-menu"><span class="text">Video</span></a></li>
                    </ul>
                </nav>
                <div class="clearfix"></div>
            </div>
            <div class="footer-middle padding-top-60 padding-bottom-60">
                <div class="row">
                    <div class="col-2 col-md-5">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="catelory-widget widget">
                                    <div class="title-widget">categories</div>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">news (89)</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">sport (63)</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">business (57)</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">entertainment (48)</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">tech (36)</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">lifestyle (25)</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="catelory-widget widget">
                                    <div class="title-widget">useful links</div>
                                    <div class="content-widget">
                                        <ul class="list-unstyled info-list">
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">the company</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">our team</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">testimonials</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">terms of service</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">privacy policy</span></a></li>
                                            <li><a href="index.html#" class="link"><i class="icons fa fa-angle-double-right"></i><span class="text">contact</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2 col-md-7">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="recent-posts-widget widget">
                                    <div class="title-widget">recent posts</div>
                                    <div class="content-widget">
                                        <div class="recent-posts-list">
                                            <div class="single-recent-post-widget">
                                                <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/footer/recent-post-1.jpg" alt="" class="img-wrapper" /></a>
                                                <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 29</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-recent-post-widget">
                                                <a href="index.html#" class="thumb"><img src="themes/frontend/assets/img/footer/recent-post-2.jpg" alt="" class="img-wrapper" /></a>
                                                <div class="post-info"><a href="index.html#" class="title">Nevada G.O.P. Caucuses Are Test of Strength For Trump</a>
                                                    <ul class="info">
                                                        <li><a href="index.html#" class="link">By timothy</a></li>
                                                        <li><a href="index.html#" class="link">May 29</a></li>
                                                        <li><a href="index.html#" class="link">15 likes</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="about-us-widget widget">
                                    <div class="title-widget">about us</div>
                                    <div class="content-widget">
                                        <p class="text">DirectNews is modern, clean & clear HTML template has been designed & developed for online Newspaper & Magazine.</p>
                                        <ul class="list-unstyled about-us-info-list">
                                            <li><i class="icons fa fa-envelope-o"></i><a href="index.html#" class="link">hello@directnews.com</a></li>
                                            <li><i class="icons fa fa-phone"></i><a href="index.html#" class="link">P: 3333 222 1111</a></li>
                                            <li><i class="icons fa fa-map-marker"></i><a href="index.html#" class="link">2050 Main St, P.O. Box 1234 Anytown, MA 12345</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="name-company pull-left">&copy; SWLABS</div>
                <div class="social-list pull-right">
                    <ul class="list-inline list-unstyled">
                        <li><a href="index.html#" class="link facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="index.html#" class="link twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="index.html#" class="link pinterest"><i class="fa fa-pinterest-p"></i></a></li>
                        <li><a href="index.html#" class="link google"><i class="fa fa-google"></i></a></li>
                        <li><a href="index.html#" class="link instagram"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
                <div class="subscribe-email pull-right">
                    <form>
                        <div class="input-group"><input type="text" placeholder="Email adress" class="form-control" /><span class="input-group-btn"><button type="submit" class="btn-email"><i class="icons fa fa-check"></i></button></span></div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</footer>